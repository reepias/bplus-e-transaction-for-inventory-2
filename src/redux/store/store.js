import { createStore, combineReducers, applyMiddleware } from 'redux'
import { persistReducer } from 'redux-persist'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import AsyncStorage from '@react-native-community/async-storage'

import { auth, ui } from '../reducers'

const authPersistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  blacklist: [],
  timeout: 0, // The code base checks for falsy, so 0 disables
}

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, auth),
})

const configureStore = () => {
  const store = createStore(rootReducer, applyMiddleware(logger, thunk))
  return store
}

export default configureStore
