import AsyncStorage from '@react-native-community/async-storage'
import { LOOKUP_ERP_URL } from '../../constants/config'
import { getErrorMessageTypeByError } from '../../helpers'
import { serviceId } from '../../constants/config'
import axios from 'axios'
import { CUSTOMER_TABLE, CREDITOR_TABLE } from '../../constants/config'

export const getCustomersService = async (searchTerm, mode) => {
  console.log('searchTerm: ', searchTerm)
  console.log('mode: ', mode)
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  console.log('bpAppUser: ', bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID
  let sqlFilter = null
  if (searchTerm) {
    sqlFilter = encodeURIComponent(
      `AND (${mode}_NAME LIKE '%${searchTerm}%') OR (${mode}_CODE LIKE '%${searchTerm}%')`
    )
  }
  const options = {
    timeout: 5000,
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
      'SQL-TABLE': mode === 'AP' ? CREDITOR_TABLE : CUSTOMER_TABLE,
    },
    url: LOOKUP_ERP_URL,
  }

  if (sqlFilter) {
    options.headers['SQL-FILTER'] = sqlFilter
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log('json: ', json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}

export const getCustomerService = async (searchTerm, mode) => {
  console.log('searchTerm: ', searchTerm)
  console.log('mode: ', mode)
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  console.log('bpAppUser: ', bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID
  let sqlFilter = null
  if (searchTerm) {
    sqlFilter = encodeURIComponent(`AND (${mode}_CODE = '${searchTerm}')`)
  }
  const options = {
    timeout: 5000,
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
      'SQL-TABLE': mode === 'AP' ? CREDITOR_TABLE : CUSTOMER_TABLE,
    },
    url: LOOKUP_ERP_URL,
  }

  if (sqlFilter) {
    options.headers['SQL-FILTER'] = sqlFilter
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log('json: ', json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}
