import AsyncStorage from '@react-native-community/async-storage'
import { LOOKUP_ERP_URL } from '../../constants/config'
import { getErrorMessageTypeByError } from '../../helpers'
import { serviceId } from '../../constants/config'
import axios from 'axios'

export const getDocumentService = async (sqlTableKey, sqlFilter) => {
  console.log('sqlTableKey: ', sqlTableKey)
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  console.log('bpAppUser: ', bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID
  // let sqlFilter = encodeURIComponent("AND (DI_REF = 'DB256101/00001')")
  const options = {
    timeout: 5000,
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
      'SQL-TABLE': sqlTableKey,
      'SQL-ORDER-BY': 'ORDER BY DI_DATE DESC',
    },
    url: LOOKUP_ERP_URL,
  }

  if (sqlFilter) {
    options.headers['SQL-FILTER'] = encodeURIComponent(sqlFilter)
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log('json: ', json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}
