import AsyncStorage from '@react-native-community/async-storage'
import {
  LOOKUP_ERP_URL,
  serviceId,
  CREDITOR_AGREEMENT_TABLE,
} from '../../constants/config'
import { getErrorMessageTypeByError } from '../../helpers'
import axios from 'axios'

export const getCreditorAgreementsService = async () => {
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID
  const options = {
    timeout: 5000,
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
      'SQL-TABLE': CREDITOR_AGREEMENT_TABLE,
    },
    url: LOOKUP_ERP_URL,
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log('json: ', json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}
