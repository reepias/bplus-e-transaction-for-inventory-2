import AsyncStorage from '@react-native-community/async-storage'
import {
  LOOKUP_ERP_URL,
  serviceId,
  PRODUCT_TABLE,
} from '../../constants/config'
import { getErrorMessageTypeByError } from '../../helpers'
import axios from 'axios'

export const getAllProductService = async searchTerm => {
  console.log('searchTerm: ', searchTerm)
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID

  let sqlFilter = null
  if (searchTerm) {
    sqlFilter = encodeURIComponent(
      `AND (SKU_NAME LIKE '%${searchTerm}%') OR (SKU_CODE LIKE '%${searchTerm}%')`
    )
  }

  const options = {
    timeout: 5000,
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
      'SQL-TABLE': PRODUCT_TABLE,
    },
    url: LOOKUP_ERP_URL,
  }

  if (sqlFilter) {
    options.headers['SQL-FILTER'] = sqlFilter
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      return json
    })
    .catch(error => {
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}

export const findProductService = async searchTerm => {
  console.log('searchTerm: ', searchTerm)
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID

  let sqlFilter = null
  if (searchTerm) {
    sqlFilter = encodeURIComponent(`AND (SKU_CODE = '${searchTerm}')`)
  }

  const options = {
    timeout: 5000,
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
      'SQL-TABLE': PRODUCT_TABLE,
    },
    url: LOOKUP_ERP_URL,
  }

  if (sqlFilter) {
    options.headers['SQL-FILTER'] = sqlFilter
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      return json
    })
    .catch(error => {
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}
