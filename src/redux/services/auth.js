import DeviceInfo from 'react-native-device-info'
import AsyncStorage from '@react-native-community/async-storage'
import { AUTH_URL } from '../../constants/auth'
import { getErrorMessageTypeByError } from '../../helpers'
import { SIGN_IN_SCREEN } from '../../navigations/Screens'
import { Navigation } from 'react-native-navigation'
import { serviceId } from '../../constants/config'
import axios from 'axios'

export const getAddress = () => {
  return new Promise(resolve => {
    DeviceInfo.getMACAddress()
      .then(mac => {
        getAddressSuccess(mac)
        resolve(mac)
      })
      .catch(err => {
        alert(err)
      })
  })
}

const getAddressSuccess = async mac => {
  await AsyncStorage.setItem('@app_macAddress', mac)
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: SIGN_IN_SCREEN,
            },
          },
        ],
      },
    },
  })
}

export const registerUserService = async request => {
  console.log('request: ', request)
  const macAddress = await AsyncStorage.getItem('@app_macAddress')
  console.log('macAddress: ', macAddress)
  const options = {
    timeout: 4000,
    method: 'POST',
    headers: { 'content-type': 'application/json' },
    data: JSON.stringify({
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-MACHINE': macAddress,
      'BPAPUS-CNTRY-CODE': request.countryCode,
      'BPAPUS-MOBILE': request.telephone,
    }),
    url: AUTH_URL,
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log('json: ', json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}

export const loginUserService = async request => {
  console.log('request: ', request)
  const macAddress = await AsyncStorage.getItem('@app_macAddress')
  console.log('macAddress: ', macAddress)
  const options = {
    timeout: 4000,
    method: 'GET',
    headers: {
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-MACHINE': macAddress,
      'BPAPUS-USERID': request.username,
      'BPAPUS-PASSWORD': request.password,
    },
    url: AUTH_URL,
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log(json)
      return json
    })
    .catch(error => {
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}

export const logoutUserService = async () => {
  let bpAppUser = await AsyncStorage.getItem('bpAppUser')
  bpAppUser = JSON.parse(bpAppUser)
  console.log('bpAppUser: ', bpAppUser)
  const BPAPUS_GUID = bpAppUser.BPAPUS_GUID
  const options = {
    timeout: 4000,
    method: 'PUT',
    data: JSON.stringify({
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-GUID': BPAPUS_GUID,
    }),
    url: AUTH_URL,
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log(json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}

export const unRegisterService = async () => {
  const macAddress = await AsyncStorage.getItem('@app_macAddress')
  console.log('macAddress: ', macAddress)
  const options = {
    timeout: 4000,
    method: 'DELETE',
    headers: {
      'content-type': 'application/json',
      'BPAPUS-BPAPSV': serviceId,
      'BPAPUS-MACHINE': macAddress,
    },
    url: AUTH_URL,
  }

  console.log('options: ', options)

  return axios(options)
    .then(json => {
      console.log('json: ', json)
      return json
    })
    .catch(error => {
      console.log('error: ', error)
      const message = getErrorMessageTypeByError(error)
      alert(`${message}`)
    })
}
