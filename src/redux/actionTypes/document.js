export const GET_DOCUMENT = `GET_DOCUMENT`
export const GET_DOCUMENT_FAILURE = 'GET_DOCUMENT_FAILURE'
export const RECEIVE_DOCUMENT = 'RECEIVE_DOCUMENT'

export const CHOOSE_DOCUMENT_MENU = 'CHOOSE_DOCUMENT_MENU'
export const RECEIVE_DOCUMENT_MENU = 'RECEIVE_DOCUMENT_MENU'

export const PREPARE_DOCUMENT = 'PREPARE_DOCUMENT'
export const RECEIVE_PREPARE_DOCUMENT = 'RECEIVE_PREPARE_DOCUMENT'

export const REMOVE_DOCUMENT = 'REMOVE_DOCUMENT'
