import * as types from '../actionTypes/index'
import { all, fork, put, takeEvery } from 'redux-saga/effects'
import * as services from '../services'
import * as actions from '../actions'
import _ from 'lodash'
import { Navigation } from 'react-native-navigation'

export function* getDocumentFunction({ payload, sqlFilter }) {
  const sqlTableKey = payload
  try {
    console.log('payload: ', payload)
    console.log('sqlTableKey: ', sqlTableKey)
    console.log('sqlFilter: ', sqlFilter)
    const response = yield services.getDocumentService(sqlTableKey, sqlFilter)
    console.log('getDocument: ', response)
    if (response.data) {
      const data = response.data
      if (data && Object.keys(data)[0]) {
        const getDocuments = data[Object.keys(data)[0]]
        const keys = _.keys(data)
        let documentKey = ''
        if (keys) {
          documentKey = keys[0]
        }
        console.log('getDocuments: ', getDocuments)
        yield put(actions.receiveDocument(getDocuments, documentKey))
      }
    }
  } catch (e) {
    console.log(e.message)
  }
}

export function* chooseDocumentMenuFunction({ payload }) {
  try {
    console.log('payload: ', payload)
    const { title, sqlTableKey, customer, componentId, componentName } = payload
    const chooseDocument = {
      ...payload,
    }
    yield put(actions.receiveDocumentMenu(chooseDocument))
    Navigation.push(componentId, {
      component: {
        name: componentName,
      },
    })
  } catch (e) {
    console.log(e.message)
  }
}

export function* setPrepareDocumentFunction({ payload }) {
  try {
    const object = {
      ...payload,
    }
    yield put(actions.receivePrepareDocument(object))
  } catch (e) {
    console.log(e.message)
  }
}

export function* setPrepareDocument() {
  yield takeEvery(types.PREPARE_DOCUMENT, setPrepareDocumentFunction)
}

export function* getDocument() {
  yield takeEvery(types.GET_DOCUMENT, getDocumentFunction)
}

export function* chooseDocumentMenu() {
  yield takeEvery(types.CHOOSE_DOCUMENT_MENU, chooseDocumentMenuFunction)
}

export function* rootSaga() {
  yield all([
    fork(getDocument),
    fork(chooseDocumentMenu),
    fork(setPrepareDocument),
  ])
}

export default rootSaga
