import { GET_MAC_ADDRESS } from '../actionTypes'
import * as types from '../actionTypes/index'
import { all, fork, put, takeEvery } from 'redux-saga/effects'
import {
  getAddress,
  loginUserService,
  registerUserService,
  logoutUserService,
  unRegisterService,
} from '../services'
import {
  receiveMacAddress,
  loginUserSuccess,
  registerUserSuccess,
  logoutUserSuccess,
  unRegisterSuccess,
  loadingStartAction,
  loadingEndAction,
} from '../actions'
import AsyncStorage from '@react-native-community/async-storage'
import React from 'react'
import { Alert } from 'react-native'
import { i18n } from 'react-native-i18n-localize'
import removeItemValue from '../../helpers/removeItemValue'

export function* getMacAddressFunction() {
  try {
    const macAddress = yield getAddress()
    console.log('getMacAddressFunction: ', macAddress)
    yield put(receiveMacAddress(macAddress))
  } catch (e) {
    console.log(e.message)
  }
}

export function* loginUserFunction(payload) {
  try {
    const { user } = payload
    yield put(loadingStartAction())
    const response = yield loginUserService(user)
    if (response && response.data) {
      const data = response.data
      const bpAppUser = data.BpAppUsers[0]
      console.log('bpAppUser: ', bpAppUser)
      yield AsyncStorage.setItem('bpAppUser', JSON.stringify(bpAppUser))
      yield put(loginUserSuccess(bpAppUser))
    }
    yield put(loadingEndAction())
  } catch (e) {
    yield put(loadingEndAction())
    console.log(e.message)
  }
}

export function* logoutUserFunction(payload) {
  console.log('logoutUserFunction :Start', payload)
  try {
    const response = yield logoutUserService()
    console.log('response: ', response)
    if (response.data) {
      const data = response.data
      console.log('data: ', data)
      const bpOldGuid = data[0]
      console.log('bpOldGuid: ', bpOldGuid)
      yield AsyncStorage.setItem('bpOldGuid', JSON.stringify(bpOldGuid))
      yield put(logoutUserSuccess(bpOldGuid))
    }
  } catch (e) {
    console.log(e.message)
  }
}

export function* registerUserFunction(payload) {
  try {
    const { user } = payload
    const response = yield registerUserService(user)
    console.log('registerUserFunction: ', response)
    const { status } = response
    if (status === 200) {
      yield put(registerUserSuccess())
      Alert.alert(
        `${i18n.t(`register.success.title`)}`,
        `${i18n.t(`register.success.message`)}`,
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false }
      )
    }
    // if (response.data) {
    //   const data = response.data
    //   const bpAppUser = data.BpAppUsers[0]
    //   console.log('bpAppUser: ', bpAppUser)
    //   yield AsyncStorage.setItem('bpAppUser', JSON.stringify(bpAppUser))
    //   yield put(loginUserSuccess(bpAppUser))
    // }
  } catch (e) {
    console.log(e.message)
  }
}

export function* unRegisterFunction() {
  try {
    const response = yield unRegisterService()
    console.log('unRegisterFunction: ', response)
    const { status } = response
    if (status === 200) {
      yield removeItemValue('bpOldGuid')
      yield removeItemValue('bpAppUser')
      yield put(unRegisterSuccess())
    }
  } catch (e) {
    console.log(e.message)
  }
}

export function* getMacAddress() {
  yield takeEvery(GET_MAC_ADDRESS, getMacAddressFunction)
}

export function* loginUser() {
  yield takeEvery(types.LOGIN_USER, loginUserFunction)
}

export function* logoutUser() {
  console.log('logoutUser')
  yield takeEvery(types.LOGOUT_USER, logoutUserFunction)
}

export function* registerUser() {
  yield takeEvery(types.REGISTER_USER, registerUserFunction)
}

export function* unRegister() {
  yield takeEvery(types.UN_REGISTER, unRegisterFunction)
}

export function* rootSaga() {
  yield all([
    fork(getMacAddress),
    fork(loginUser),
    fork(logoutUser),
    fork(registerUser),
    fork(unRegister),
  ])
}

export default rootSaga
