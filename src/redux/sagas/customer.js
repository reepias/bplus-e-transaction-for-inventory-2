import * as types from '../actionTypes/index'
import { all, fork, put, takeEvery, select } from 'redux-saga/effects'
import * as services from '../services'
import * as actions from '../actions'
import { Navigation } from 'react-native-navigation'
import { CUSTOMER_SCREEN } from '../../navigations/Screens'
import { getPrepareDocument } from '../../helpers'

export const getPrepareDocumentState = state => state.document.prepareDocument

export function* getCustomersFunction({ payload }) {
  const { searchTerm, mode } = payload
  try {
    console.log('payload: ', payload)
    console.log('searchTerm: ', searchTerm)
    console.log('mode: ', mode)
    const response = yield services.getCustomersService(searchTerm, mode)
    console.log('getCustomers: ', response)
    if (response.data) {
      const data = response.data
      let customers = []
      if (mode === 'AP') {
        customers = data.Ap000130
      } else if (mode === 'AR') {
        customers = data.Ar000130
      }
      console.log('customers: ', customers)
      yield put(actions.receiveCustomers(customers))
    }
  } catch (e) {
    yield put(actions.receiveCustomers())
    console.log(e.message)
  }
}

export function* getCustomerFunction({ payload }) {
  const { searchTerm, mode, componentId } = payload
  try {
    console.log('payload: ', payload)
    console.log('searchTerm: ', searchTerm)
    console.log('mode: ', mode)
    if (!searchTerm) {
      Navigation.push(componentId, {
        component: {
          name: CUSTOMER_SCREEN,
          passProps: {
            title: `เจ้าหนี้`,
            searchTerm: searchTerm,
            mode: mode,
          },
        },
      })
    } else {
      const response = yield services.getCustomerService(searchTerm, mode)
      console.log('getCustomer: ', response)
      if (response.data) {
        const data = response.data
        let customer = []
        if (mode === 'AP') {
          customer = data.Ap000130
        } else if (mode === 'AR') {
          customer = data.Ar000130
        }
        //find 1
        customer = customer[0]
        if (customer) {
          console.log('customer: ', customer)
          let prepareDocumentState = yield select(getPrepareDocumentState) // <-- get the document
          console.log('prepareDocumentState: ', prepareDocumentState)

          const { prepareDocument } = getPrepareDocument(prepareDocumentState)

          let object = {
            ...prepareDocument,
            ...customer,
          }

          yield put(actions.setPrepareDocument({ ...object }))
          yield put(actions.receiveCustomer(customer))
        } else {
          console.log('*** not customer ***')
          // alert('ไม่พบ >>> ต้องไปหน้าค้นหาเจ้าหนี้')
          Navigation.push(componentId, {
            component: {
              name: CUSTOMER_SCREEN,
              passProps: {
                title: `เจ้าหนี้`,
                searchTerm: searchTerm,
                mode: mode,
              },
            },
          })
        }
      }
    }
  } catch (e) {
    yield put(actions.receiveCustomer())
    console.log(e.message)
  }
}

export function* getCustomers() {
  yield takeEvery(types.GET_ALL_CUSTOMERS, getCustomersFunction)
}

export function* getCustomer() {
  yield takeEvery(types.GET_CUSTOMER, getCustomerFunction)
}

export function* rootSaga() {
  yield all([fork(getCustomers), fork(getCustomer)])
}

export default rootSaga
