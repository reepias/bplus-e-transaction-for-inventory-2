import * as types from '../actionTypes/index'
import { all, fork, put, takeEvery } from 'redux-saga/effects'
import * as services from '../services'
import * as actions from '../actions'

export function* getCreditorAgreementsFunction({ payload }) {
  const { searchTerm } = payload
  try {
    const response = yield services.getCreditorAgreementsService(searchTerm)
    if (response.data) {
      const data = response.data
      console.log('data: ', data)
      const result = data && data.Ap000142
      yield put(actions.receiveCreditorAgreements(result))
    }
  } catch (e) {
    yield put(actions.receiveCreditorAgreements())
    console.log(e.message)
  }
}

export function* getCreditorAgreements() {
  yield takeEvery(types.GET_CREDITOR_AGREEMENTS, getCreditorAgreementsFunction)
}

export function* rootSaga() {
  yield all([fork(getCreditorAgreements), fork(getCreditorAgreements)])
}

export default rootSaga
