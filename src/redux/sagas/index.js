import { all } from 'redux-saga/effects'
import authSagas from './auth'
import customerSagas from './customer'
import documentSagas from './document'
import creditorAgreementSagas from './creditorAgreement'
import productSagas from './product'
import storageLocationSagas from './storageLocation'
export default function* rootSaga() {
  yield all([
    authSagas(),
    customerSagas(),
    documentSagas(),
    creditorAgreementSagas(),
    productSagas(),
    storageLocationSagas(),
  ])
}
