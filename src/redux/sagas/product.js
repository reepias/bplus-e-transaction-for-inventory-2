import * as types from '../actionTypes/index'
import { all, fork, put, takeEvery } from 'redux-saga/effects'
import * as services from '../services'
import * as actions from '../actions'

import { Navigation } from 'react-native-navigation'
import { ITEM_LIST_SCREEN } from '../../navigations/Screens'

export function* getAllProductFunction({ payload }) {
  const { searchTerm } = payload
  console.log('searchTerm: ', searchTerm)
  try {
    const response = yield services.getAllProductService(searchTerm)
    if (response.data) {
      const data = response.data
      console.log('data: ', data)
      const result = data && data.Ic000221
      yield put(actions.receiveAllProduct(result))
    }
  } catch (e) {
    yield put(actions.receiveAllProduct())
    console.log(e.message)
  }
}

export function* findProductFunction({ payload }) {
  const { searchTerm, componentId, onSelectItem } = payload
  try {
    if (!searchTerm) {
      console.log('componentId: ', componentId)
      Navigation.push(componentId, {
        component: {
          name: ITEM_LIST_SCREEN,
          passProps: {
            searchTerm,
            onSelectItem,
          },
        },
      })
    } else {
      const response = yield services.findProductService(searchTerm)
      if (response.data) {
        const data = response.data
        // console.log('data: ', data)
        const result = data && data.Ic000221
        console.log('result: ', result)
        if (result.length < 1) {
          Navigation.push(componentId, {
            component: {
              name: ITEM_LIST_SCREEN,
              passProps: {
                searchTerm,
                onSelectItem,
              },
            },
          })
        }
        yield put(actions.receiveProduct(result))
      }
    }
  } catch (e) {
    yield put(actions.receiveProduct())
    console.log(e.message)
  }
}

export function* getAllProduct() {
  yield takeEvery(types.GET_ALL_PRODUCT, getAllProductFunction)
}

export function* findProduct() {
  yield takeEvery(types.FIND_PRODUCT, findProductFunction)
}

export function* rootSaga() {
  yield all([fork(getAllProduct), fork(findProduct)])
}

export default rootSaga
