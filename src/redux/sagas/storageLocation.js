import * as types from '../actionTypes/index'
import { all, fork, put, takeEvery } from 'redux-saga/effects'
import * as services from '../services'
import * as actions from '../actions'

import { Navigation } from 'react-native-navigation'
import { WARE_LOCATION_SEARCH_SCREEN } from '../../navigations/Screens'

export function* getAllStorageLocationFunction({ payload }) {
  const { searchTerm } = payload
  try {
    const response = yield services.getAllStorageLocationService(searchTerm)
    if (response.data) {
      const data = response.data
      const result = data && data.Wh000220
      yield put(actions.receiveAllStorageLocation(result))
    }
  } catch (e) {
    yield put(actions.receiveAllStorageLocation())
    console.log(e.message)
  }
}

export function* findStorageLocationFunction({ payload }) {
  const { searchTerm, componentId, onSelectItem } = payload
  try {
    if (!searchTerm) {
      Navigation.push(componentId, {
        component: {
          name: ITEM_LIST_SCREEN,
          passProps: {
            searchTerm,
            onSelectItem,
          },
        },
      })
    } else {
      const response = yield services.findStorageLocationService(searchTerm)
      if (response.data) {
        const data = response.data
        const result = data && data.Wh000220
        if (result.length < 1) {
          Navigation.push(componentId, {
            component: {
              name: WARE_LOCATION_SEARCH_SCREEN,
              passProps: {
                searchTerm,
                onSelectItem,
              },
            },
          })
        }
        yield put(actions.receiveStorageLocation(result))
      }
    }
  } catch (e) {
    yield put(actions.receiveStorageLocation())
    console.log(e.message)
  }
}

export function* getAllStorageLocation() {
  yield takeEvery(types.GET_ALL_STORAGE_LOCATION, getAllStorageLocationFunction)
}

export function* findStorageLocation() {
  yield takeEvery(types.FIND_STORAGE_LOCATION, findStorageLocationFunction)
}

export function* rootSaga() {
  yield all([fork(getAllStorageLocation), fork(findStorageLocation)])
}

export default rootSaga
