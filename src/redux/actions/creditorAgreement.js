import * as types from '../actionTypes/index'

export const getCreditorAgreements = searchTerm => {
  return {
    type: types.GET_CREDITOR_AGREEMENTS,
    payload: {
      searchTerm,
    },
  }
}

export const receiveCreditorAgreements = creditorAgreements => {
  return {
    type: types.RECEIVE_CREDITOR_AGREEMENTS,
    creditorAgreements,
  }
}
