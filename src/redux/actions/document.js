import * as types from '../actionTypes/index'

export const removePrepareDocument = () => {
  return {
    type: types.REMOVE_DOCUMENT,
  }
}

export const setPrepareDocument = object => {
  return {
    type: types.PREPARE_DOCUMENT,
    payload: object,
  }
}

export const receivePrepareDocument = object => {
  return {
    type: types.RECEIVE_PREPARE_DOCUMENT,
    prepareDocument: object,
  }
}

export const chooseDocumentMenu = menu => {
  return {
    type: types.CHOOSE_DOCUMENT_MENU,
    payload: menu,
  }
}

export const receiveDocumentMenu = chooseDocument => {
  return {
    type: types.RECEIVE_DOCUMENT_MENU,
    chooseDocument,
  }
}

export const getDocument = (sqlTableKey, sqlFilter) => {
  return {
    type: types.GET_DOCUMENT,
    payload: sqlTableKey,
    sqlFilter,
  }
}

export const receiveDocument = (documents, documentKey) => {
  return {
    type: types.RECEIVE_DOCUMENT,
    documents,
    documentKey: documentKey,
  }
}
