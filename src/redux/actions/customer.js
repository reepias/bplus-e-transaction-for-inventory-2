import * as types from '../actionTypes/index'

export const getCustomers = (searchTerm, mode) => {
  return {
    type: types.GET_ALL_CUSTOMERS,
    payload: {
      searchTerm,
      mode,
    },
  }
}

export const receiveCustomers = customers => {
  return {
    type: types.RECEIVE_CUSTOMERS,
    customers,
  }
}

export const getCustomer = (searchTerm, mode, componentId) => {
  return {
    type: types.GET_CUSTOMER,
    payload: {
      searchTerm,
      mode,
      componentId,
    },
  }
}

export const receiveCustomer = customer => {
  return {
    type: types.RECEIVE_CUSTOMER,
    customer,
  }
}
