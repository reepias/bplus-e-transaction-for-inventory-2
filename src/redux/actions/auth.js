import * as types from "../actionTypes/index";
import { Navigation } from "react-native-navigation";
import {
  MAIN_MENU_SCREEN,
  SIGN_IN_SCREEN,
  LOADING_SCREEN
} from "../../navigations/Screens";
import { i18n } from "react-native-i18n-localize";

export const loginUserAction = user => {
  return {
    type: types.LOGIN_USER,
    user
  };
};

export const loginUserSuccess = bpAppUser => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: MAIN_MENU_SCREEN
            }
          }
        ]
      }
    }
  });
  return {
    type: types.LOGIN_USER_SUCCESS,
    bpAppUser
  };
};

export const logoutUserAction = user => {
  console.log("logoutUserAction start: ", user);
  return {
    type: types.LOGOUT_USER,
    user
  };
};

export const logoutUserSuccess = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: LOADING_SCREEN,
              passProps: {
                text: `${i18n.t(`loggingOut`)}`
              }
            }
          }
        ]
      }
    }
  });
  return {
    type: types.LOGOUT_USER_SUCCESS
  };
};

export const registerUserSuccess = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: SIGN_IN_SCREEN
            }
          }
        ]
      }
    }
  });
  return {
    type: types.REGISTER_USER_SUCCESS
  };
};

export const registerUserAction = user => {
  return {
    type: types.REGISTER_USER,
    user
  };
};

export const getMacAddress = () => {
  return {
    type: types.GET_MAC_ADDRESS
  };
};

export const receiveMacAddress = macAddress => {
  return {
    type: types.RECEIVE_MAC_ADDRESS,
    macAddress
  };
};

export const unRegisterAction = () => {
  return {
    type: types.UN_REGISTER
  };
};

export const unRegisterSuccess = () => {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: LOADING_SCREEN,
              passProps: {
                text: `${i18n.t(`cancelingRegistration`)}`
              }
            }
          }
        ]
      }
    }
  });
  return {
    type: types.UN_REGISTER_SUCCESS
  };
};
