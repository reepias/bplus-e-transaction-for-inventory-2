import * as types from '../actionTypes/index'

export const loadingStartAction = () => {
  return {
    type: types.LOADING_START,
  }
}

export const loadingEndAction = () => {
  return {
    type: types.LOADING_END,
  }
}
