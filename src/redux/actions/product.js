import * as types from '../actionTypes/index'

export const getAllProduct = searchTerm => {
  return {
    type: types.GET_ALL_PRODUCT,
    payload: {
      searchTerm,
    },
  }
}

export const receiveAllProduct = products => {
  return {
    type: types.RECEIVE_ALL_PRODUCT,
    products,
  }
}

export const findProduct = payload => {
  return {
    type: types.FIND_PRODUCT,
    payload,
  }
}

export const receiveProduct = product => {
  return {
    type: types.RECEIVE_PRODUCT,
    product,
  }
}
