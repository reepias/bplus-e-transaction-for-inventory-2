import * as types from '../actionTypes/index'

export const findStorageLocation = payload => {
  return {
    type: types.FIND_STORAGE_LOCATION,
    payload,
  }
}

export const receiveStorageLocation = warehouse => {
  return {
    type: types.RECEIVE_STORAGE_LOCATION,
    warehouse,
  }
}

export const getAllStorageLocation = searchTerm => {
  return {
    type: types.GET_ALL_STORAGE_LOCATION,
    payload: {
      searchTerm,
    },
  }
}

export const receiveAllStorageLocation = storageLocations => {
  return {
    type: types.RECEIVE_ALL_STORAGE_LOCATION,
    storageLocations,
  }
}
