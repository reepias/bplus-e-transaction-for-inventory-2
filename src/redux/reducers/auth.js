import { AUTH_DO_LOGIN } from '../../constants/auth'

const auth = (
  state = {
    credential: undefined,
  },
  action
) => {
  switch (action.type) {
    case AUTH_DO_LOGIN:
      return {
        ...state,
        credential: action.payload,
      }
    case types.LOGOUT_USER_SUCCESS:
      console.log('LOGIN_USER_SUCCESS: ', action)
      return {
        ...state,
      }
    case types.UN_REGISTER_SUCCESS:
      console.log('UN_REGISTER_SUCCESS: ', action)
      return {
        ...state,
        macAddress: [],
        bpAppUser: null,
      }
    default:
      return state
  }
}

export default auth
