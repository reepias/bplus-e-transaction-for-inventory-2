import * as types from '../actionTypes/index'
import moment from 'moment'
const initialState = {
  documents: [],
  chooseDocument: {
    title: '',
    sqlTableKey: '',
    customer: '',
  },
  isFetching: false,
  prepareDocument: {
    DI_DATE: moment(),
    // DELIVERY_DATE: moment(),
    // DATE_OF_TAX_INVOICE: moment(),
    // EXPIRED_DATE: moment().add(30, 'days'),
    // DUE_DATE: moment().add(60, 'days'),
    // INVOICE_NUMBER: '<เลขถัดไป>',
    // TAX_INVOICE_NUMBER: '<เลขถัดไป>',
    // AGREEMENT: '5',
  },
}

const documentReducer = (state = initialState, action) => {
  const { type, documents, chooseDocument, prepareDocument } = action
  switch (type) {
    case types.GET_DOCUMENT:
      return {
        ...state,
        documents: [],
        isFetching: true,
      }
    case types.REMOVE_DOCUMENT:
      return {
        ...state,
        prepareDocument: initialState.prepareDocument,
      }
    case types.RECEIVE_PREPARE_DOCUMENT:
      console.log('PRECEIVE_PEPAIR_DOCUMENT: ', prepareDocument)
      return {
        ...state,
        prepareDocument: prepareDocument,
      }
    case types.RECEIVE_DOCUMENT_MENU:
      return {
        ...state,
        chooseDocument: chooseDocument,
      }
    case types.RECEIVE_DOCUMENT:
      const { documentKey } = action
      return {
        ...state,
        isFetching: false,
        documents: {
          documentKey: documentKey,
          data: documents,
        },
      }
    default:
      return state
  }
}

export default documentReducer
