import * as types from '../actionTypes/index'
import _ from 'lodash'

const initialState = {
  creditorAgreements: [],
  isFetching: false,
}

const customerReducer = (state = initialState, action) => {
  const { type, creditorAgreements } = action
  switch (type) {
    case types.GET_CREDITOR_AGREEMENTS:
      return {
        ...state,
        creditorAgreements: [],
        isFetching: true,
      }
    case types.RECEIVE_CREDITOR_AGREEMENTS:
      let creditorAgreementPicker = creditorAgreements
      creditorAgreementPicker =
        creditorAgreementPicker &&
        creditorAgreementPicker.map(object =>
          _.mapKeys(object, (value, key) => {
            // APDTAB_KEY to value
            // APDTAB_NAME to label
            // APDTAB_CODE to value
            if (key === 'APDTAB_KEY') {
              key = `value`
            } else if (key === 'APDTAB_NAME') {
              key = `label`
            } else if (key === 'APDTAB_CODE') {
              key = `key`
            }
            return key
          })
        )
      console.log('creditorAgreementPicker: ', creditorAgreementPicker)
      return {
        ...state,
        isFetching: false,
        creditorAgreements: creditorAgreementPicker,
      }
    default:
      return state
  }
}

export default customerReducer
