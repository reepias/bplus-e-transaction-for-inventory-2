import * as types from '../actionTypes'

const initialState = {
  storageLocations: [],
  isFetching: false,
}

const storageLocationReducer = (state = initialState, action) => {
  const { type, storageLocations } = action
  switch (type) {
    case types.GET_ALL_STORAGE_LOCATION:
      return {
        ...state,
        storageLocations: [],
        isFetching: true,
      }
    case types.RECEIVE_ALL_STORAGE_LOCATION:
      return {
        ...state,
        isFetching: false,
        storageLocations: storageLocations,
      }
    default:
      return state
  }
}

export default storageLocationReducer
