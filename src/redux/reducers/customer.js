import * as types from '../actionTypes/index'

const initialState = {
  customers: [],
  cus: {},
  isFetching: false,
}

const customerReducer = (state = initialState, action) => {
  const { type, customers, customer } = action
  switch (type) {
    case types.GET_ALL_CUSTOMERS:
      return {
        ...state,
        customers: [],
        isFetching: true,
      }
    case types.RECEIVE_CUSTOMERS:
      return {
        ...state,
        isFetching: false,
        customers: customers,
      }
    case types.GET_CUSTOMER:
      return {
        ...state,
        cus: {},
        isFetching: true,
      }
    case types.RECEIVE_CUSTOMER:
      return {
        ...state,
        isFetching: false,
        cus: customer,
      }
    default:
      return state
  }
}

export default customerReducer
