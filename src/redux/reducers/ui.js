import * as types from "../actionTypes/index";

const initialState = {
  isLoading: false
};

const uiReducer = (state = initialState, action) => {
  const { type } = action;
  console.log("action: ", action);
  switch (type) {
    case types.LOADING_START:
      return {
        ...state,
        isLoading: true
      };
    case types.LOADING_END:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};

export default uiReducer;
