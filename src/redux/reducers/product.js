import * as types from '../actionTypes'

const initialState = {
  products: [],
  productOne: [],
  isFetching: false,
}

const productReducer = (state = initialState, action) => {
  const { type, productOne } = action
  switch (type) {
    case types.GET_ALL_PRODUCT:
      return {
        ...state,
        products: [],
        isFetching: true,
      }
    case types.RECEIVE_ALL_PRODUCT:
      return {
        ...state,
        isFetching: false,
        products: action.products,
      }

    case types.FIND_PRODUCT:
      return {
        ...state,
        productOne: [],
        isFetching: true,
      }
    case types.RECEIVE_PRODUCT:
      return {
        ...state,
        isFetching: false,
        productOne: productOne,
      }
    default:
      return state
  }
}

export default productReducer
