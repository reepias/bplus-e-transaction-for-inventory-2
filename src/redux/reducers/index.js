import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-community/async-storage";
import auth from "./auth";
import ui from "./ui";
import customer from "./customer";
import document from "./document";
import creditorAgreement from "./creditorAgreement";
import product from "./product";
import storageLocation from "./storageLocation";

const authPersistConfig = {
  key: "auth",
  storage: AsyncStorage,
  timeout: 0 // The code base checks for falsy, so 0 disables
};

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, auth),
  ui: ui,
  customer: customer,
  document: document,
  creditorAgreement: creditorAgreement,
  product: product,
  storageLocation: storageLocation
});

export default rootReducer;
