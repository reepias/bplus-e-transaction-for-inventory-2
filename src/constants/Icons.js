import React from 'react'
import { Image, StyleSheet } from 'react-native'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

export const ArrowBackWhite = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-left-arrow-angle.png')}
  />
)

export const Setting = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-settings.png')}
  />
)

export const Worlwide = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-worlwide.png')}
  />
)

export const Hamburger = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-menu.png')}
  />
)

export const PlusCircleWhite = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-fab-plus-white.png')}
  />
)

export const SearchWhite = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-search.png')}
  />
)

export const Pencil = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-pencil-white.png')}
  />
)

export const Trash = props => (
  <Image
    style={[styles.icon, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/icons/ico-trash-white.png')}
  />
)

const styles = StyleSheet.create({
  icon: {
    width: wp('5%'),
    height: wp('5%'),
  },
})
