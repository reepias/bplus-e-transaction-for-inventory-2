export const API_ENDPOINT =
  'http://192.168.0.110:8905/BplusDvSvr/BplusErpDvSvrIIS.dll'
export const serviceId = '{167f0c96-86fd-488f-94d1-cc3169d60b1a}'

const LOOKUP_ERP_BASE_ENDPOINT = '/LookupErp'

// URL //
export const LOOKUP_ERP_URL = API_ENDPOINT + LOOKUP_ERP_BASE_ENDPOINT

//TABLE
export const CUSTOMER_TABLE = 'Ar000130'
export const CREDITOR_TABLE = 'Ap000130'
export const BUY_BELIEVE_TABLE = 'Oe000404'
export const SELL_BELIEVE_TABLE = 'Oe000304'
export const TRANSFER_TABLE = 'Oe001304'
export const CREDITOR_AGREEMENT_TABLE = 'Ap000142'

export const PRODUCT_TABLE = 'Ic000221'

export const STORAGE_LOCATION_TABLE = 'Wh000220'
