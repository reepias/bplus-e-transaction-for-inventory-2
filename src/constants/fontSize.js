import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

export const textSmall = hp('2%')
export const textMedium = hp('2.3%')
export const textBig = hp('4%')
