import React from 'react'
import { Image, StyleSheet } from 'react-native'

export const LogoHeader = props => (
  <Image
    style={[styles.images, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/images/logo-header.png')}
  />
)

export const LogoBplus = props => (
  <Image
    style={[styles.images, { ...props.style }]}
    resizeMode="stretch"
    source={require('../assets/images/logo-bplus.png')}
  />
)

const styles = StyleSheet.create({
  images: {},
})
