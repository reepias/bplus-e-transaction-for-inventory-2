import {
  BUY_BELIEVE_TABLE,
  SELL_BELIEVE_TABLE,
  TRANSFER_TABLE,
} from '../constants/config'
import { DOCUMENT_SCREEN } from '../navigations/Screens'
import { i18n } from 'react-native-i18n-localize'

const sourceImage = '../assets/images/'

export const documentMenu = [
  {
    key: 'buyBelieve',
    name: 'ซื้อเชื่อ',
    title: 'ซื้อเชื่อ',
    path: require(`${sourceImage}Blue1.png`),
    sqlTableKey: BUY_BELIEVE_TABLE,
    customer: 'AP',
    componentName: 'bplusTransaction.DocumentScreen',
    inputs: {
      documentDate: true,
      documentNumber: true,
      dateOfTaxInvoice: true,
      taxInvoiceNumber: true,
      agreement: true,
      dueDate: true,
      deliveryDate: true,
      expiredDate: false,
      invoiceNumber: true,
    },
    isActive: true,
  },
  {
    key: 'returnBelieve',
    name: 'ส่งคืนเชื่อ',
    title: 'ส่งคืนเชื่อ',
    path: require(`${sourceImage}Blue2.png`),
    sqlTableKey: BUY_BELIEVE_TABLE,
    customer: 'AP',
    componentName: 'bplusTransaction.DocumentScreen',
    inputs: {
      documentDate: false,
    },
    isActive: false,
  },
  {
    key: 'sellBelieve',
    name: 'ขายเชื่อ',
    title: 'ขายเชื่อ',
    path: require(`${sourceImage}Blue3.png`),
    sqlTableKey: SELL_BELIEVE_TABLE,
    customer: 'AR',
    componentName: 'bplusTransaction.DocumentScreen',
    inputs: {
      documentDate: true,
    },
    isActive: false,
  },
  {
    key: 'takeBackBelieve',
    name: 'รับคืนเชื่อ',
    title: 'รับคืนเชื่อ',
    path: require(`${sourceImage}Blue4.png`),
    sqlTableKey: SELL_BELIEVE_TABLE,
    customer: 'AR',
    componentName: 'bplusTransaction.DocumentScreen',
    inputs: {
      documentDate: true,
    },
    isActive: false,
  },
  {
    key: 'transfer',
    name: 'โอนย้าย',
    title: 'โอนย้าย',
    path: require(`${sourceImage}Blue5.png`),
    sqlTableKey: TRANSFER_TABLE,
    customer: '',
    componentName: 'bplusTransaction.DocumentScreen',
    inputs: {
      documentDate: true,
    },
    isActive: false,
  },
]
