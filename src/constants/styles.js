export const startGradient = '#1464ff'
export const middleGradient = '#1ca5ec'
export const endGradient = '#23cdee'

export const greenStartGardient = '#3fcaa5'
export const greenMiddleGradient = '#6bdd85'
export const greenEndGradient = '#89e970'
export const drawerHeader = '#2690ee'
export const colors = {
  primary: '#1464ec',
  gray: '#f0f0f0',
  grayBlack: '#727d83',
  grayWhite: '#f4f5f7',
  white: '#FFFFFF',
  green: '#89e970',
  greenStart: '#3fcaa5',
  orange: '#f6a845',
  red: '#fc4044',
  darkGray: '#A9A9A9',
}
export const Kanit = 'Kanit-Regular'
