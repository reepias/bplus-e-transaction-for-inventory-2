import { StyleSheet, Dimensions } from 'react-native'
const { height, width } = Dimensions.get('window')
export const widthWindow = width
export const heightWindow = height
export const ASPECT_RATIO = width / height
export const LATITUDE_DELTA = 0.0922
export const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
