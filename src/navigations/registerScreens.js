import { Navigation } from 'react-native-navigation'
import React, { PureComponent } from 'react'
import { ActivityIndicator } from 'react-native'
import configureStore from '../redux/store/store'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'
import { screens } from './Screens'

const stores = configureStore()
const persistor = persistStore(stores)

const createAppWithRedux = (Component, ...props) => {
  const AppComponent = class App extends PureComponent {
    render() {
      return (
        <Provider store={stores}>
          <PersistGate
            loading={<ActivityIndicator size="large" />}
            persistor={persistor}
          >
            <Component
              {...{
                ...this.props,
                ...props,
              }}
            />
          </PersistGate>
        </Provider>
      )
    }
  }
  AppComponent.options = Component.options
  return AppComponent
}

export default function() {
  screens.map(s => {
    Navigation.registerComponent(s.componentName, () =>
      createAppWithRedux(s.screen)
    )
  })
  // Navigation.registerComponent(
  //   MAIN_MENU_SCREEN,
  //   () => MainMenu,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   SIGN_UP_SCREEN,
  //   () => SignUp,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   DRAWER_SCREEN,
  //   () => Drawer,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   SERVICE_SCREEN,
  //   () => Service,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   DOCUMENT_SCREEN,
  //   () => Document,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   BILLING_SCREEN,
  //   () => Billing,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   MANAGE_BILLING_SCREEN,
  //   () => ManageBilling,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   BILLING_SUMMARY_SCREEN,
  //   () => BillingSummary,
  //   Provider,
  //   store
  // )
  // Navigation.registerComponent(
  //   CUSTOMER_SCREEN,
  //   () => Customer,
  //   Provider,
  //   store
  // )
  console.info('All screens have been registered...')
}
