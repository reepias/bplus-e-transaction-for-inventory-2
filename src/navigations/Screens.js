import {
  Loading,
  SignIn,
  SignUp,
  MainMenu,
  Drawer,
  Service,
  Document,
  Billing,
  ManageBilling,
  Customer,
  BillingSummary,
  ItemList,
  DocumentSearch,
  DatePicker,
  FilterPicker,
  WareLocationSearch,
} from '../screens'

const appName = 'bplusTransaction'
export const LOADING_SCREEN = `${appName}.LoadingScreen`
export const SIGN_IN_SCREEN = `${appName}.SignInScreen`
export const SIGN_UP_SCREEN = `${appName}.SignUpScreen`
export const MAIN_MENU_SCREEN = `${appName}.MainMenuScreen`
export const DRAWER_SCREEN = `${appName}.DrawerScreen`
export const SERVICE_SCREEN = `${appName}.ServiceScreen`
export const DOCUMENT_SCREEN = `${appName}.DocumentScreen`
export const BILLING_SCREEN = `${appName}.BillingScreen`
export const MANAGE_BILLING_SCREEN = `${appName}.ManageBillingScreen`
export const CUSTOMER_SCREEN = `${appName}.CustomerScreen`
export const BILLING_SUMMARY_SCREEN = `${appName}.BillingSummaryScreen`
export const ITEM_LIST_SCREEN = `${appName}.BillingItemListScreen`
export const DOCUMENT_SEARCH_SCREEN = `${appName}.DocumentSearchScreen`
export const DATE_PICKER_SCREEN = `${appName}.DatePickerScreen`
export const FILTER_PICKER_SCREEN = `${appName}.FilterPickerScreen`
export const WARE_LOCATION_SEARCH_SCREEN = `${appName}.WareLocationSearchScreen`

export const screens = [
  {
    key: '1',
    componentName: LOADING_SCREEN,
    screen: Loading,
  },
  {
    key: '2',
    componentName: SIGN_IN_SCREEN,
    screen: SignIn,
  },
  {
    key: '3',
    componentName: SIGN_UP_SCREEN,
    screen: SignUp,
  },
  {
    key: '4',
    componentName: MAIN_MENU_SCREEN,
    screen: MainMenu,
  },
  {
    key: '5',
    componentName: DRAWER_SCREEN,
    screen: Drawer,
  },
  {
    key: '6',
    componentName: SERVICE_SCREEN,
    screen: Service,
  },
  {
    key: '7',
    componentName: DOCUMENT_SCREEN,
    screen: Document,
  },
  {
    key: '8',
    componentName: BILLING_SCREEN,
    screen: Billing,
  },
  {
    key: '9',
    componentName: MANAGE_BILLING_SCREEN,
    screen: ManageBilling,
  },
  {
    key: '10',
    componentName: CUSTOMER_SCREEN,
    screen: Customer,
  },
  {
    key: '11',
    componentName: BILLING_SUMMARY_SCREEN,
    screen: BillingSummary,
  },
  {
    key: '12',
    componentName: ITEM_LIST_SCREEN,
    screen: ItemList,
  },
  {
    key: '13',
    componentName: DOCUMENT_SEARCH_SCREEN,
    screen: DocumentSearch,
  },
  {
    key: '14',
    componentName: DATE_PICKER_SCREEN,
    screen: DatePicker,
  },
  {
    key: '15',
    componentName: FILTER_PICKER_SCREEN,
    screen: FilterPicker,
  },
  {
    key: '16',
    componentName: WARE_LOCATION_SEARCH_SCREEN,
    screen: WareLocationSearch,
  },
]
