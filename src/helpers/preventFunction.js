const preventFunction = {
  isDoing: false,
  exec(fn) {
    const self = this
    return function() {
      if (!self.isDoing) {
        self.isDoing = true
        fn()
        setTimeout(function() {
          self.isDoing = false
        }, 10)
      }
    }
  },
}

export default preventFunction
