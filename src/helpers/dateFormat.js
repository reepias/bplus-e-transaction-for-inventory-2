export const dateThaiFormat = date => {
  var newDate = new Date(date) // M-D-YYYY

  var d = newDate.getDate()
  var m = newDate.getMonth() + 1
  var y = newDate.getFullYear()

  y = y + 543

  var dateString =
    (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m) + '/' + y
  return dateString
}
