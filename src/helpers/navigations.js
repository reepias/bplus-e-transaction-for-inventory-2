import { Navigation } from 'react-native-navigation'
import { DRAWER_SCREEN, LOADING_SCREEN } from '../navigations/Screens'

export const setStackRoot = (context, object) =>
  Navigation.setStackRoot(context.props.componentId, [
    {
      component: {
        name: object.componentName,
      },
    },
  ])

export const pop = componentId => Navigation.pop(componentId)

export const push = (componentId, object) =>
  Navigation.push(componentId, {
    component: {
      name: object.componentName,
      passProps: object.passProps,
    },
  })

export const drawer = componentId => {
  Navigation.push(componentId, {
    component: {
      name: DRAWER_SCREEN,
    },
  })
}

export const signOut = componentId => {
  Navigation.setStackRoot(componentId, [
    {
      component: {
        name: LOADING_SCREEN,
        passProps: {
          text: `กำลังออกจากระบบ...`,
        },
      },
    },
  ])
}
