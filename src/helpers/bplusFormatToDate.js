export const bplusFormatToDate = s => {
  const yyyy = s.substring(0, 4)
  const mm = s.substring(4, 6)
  const dd = s.substring(6, 8)
  const hh = s.substring(8, 10)
  const mn = s.substring(10, 12)
  const result = new Date(yyyy, mm - 1, dd, hh, mn, 0, 0)
  return result
}
