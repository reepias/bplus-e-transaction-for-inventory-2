const removeItemValue = async key => {
  try {
    await AsyncStorage.removeItem(key)
    console.log('removeItemValue: ', key)
    return true
  } catch (exception) {
    return false
  }
}

export default removeItemValue
