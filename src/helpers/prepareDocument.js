import moment from 'moment'

export function getPrepareDocument(prepareDocument) {
  let {
    DI_DATE,
    DELIVERY_DATE,
    DATE_OF_TAX_INVOICE,
    EXPIRED_DATE,
    DUE_DATE,
    INVOICE_NUMBER,
    TAX_INVOICE_NUMBER,
    AGREEMENT,
  } = prepareDocument

  if (!DELIVERY_DATE) {
    DELIVERY_DATE = moment(DI_DATE)
    prepareDocument = {
      ...prepareDocument,
      DELIVERY_DATE,
    }
  }

  if (!DATE_OF_TAX_INVOICE) {
    DATE_OF_TAX_INVOICE = moment(DI_DATE)
    prepareDocument = {
      ...prepareDocument,
      DATE_OF_TAX_INVOICE,
    }
  }

  if (!EXPIRED_DATE) {
    EXPIRED_DATE = moment(DI_DATE).add(60, 'days')
    prepareDocument = {
      ...prepareDocument,
      EXPIRED_DATE,
    }
  }

  if (!DUE_DATE) {
    DUE_DATE = moment(DI_DATE).add(60, 'days')
    prepareDocument = {
      ...prepareDocument,
      DUE_DATE,
    }
  }

  if (!INVOICE_NUMBER) {
    INVOICE_NUMBER = '<เลขเดียวกัน>'
    prepareDocument = {
      ...prepareDocument,
      INVOICE_NUMBER,
    }
  }

  if (!TAX_INVOICE_NUMBER) {
    TAX_INVOICE_NUMBER = '<เลขเดียวกัน>'
    prepareDocument = {
      ...prepareDocument,
      TAX_INVOICE_NUMBER,
    }
  }

  if (!AGREEMENT) {
    AGREEMENT = '5'
    prepareDocument = {
      ...prepareDocument,
      AGREEMENT,
    }
  }

  return { prepareDocument }
}
