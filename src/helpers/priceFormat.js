import numeral from 'numeral'
export const priceFormat = number => numeral(number).format('0,0[.]00')
