import React, { Component } from 'react'
import { I18nLocalize } from 'react-native-i18n-localize'

const customWithLanguage = WrappedComponent => {
  return class extends Component {
    constructor(props) {
      super(props)
      this._isComponentWillUnmount = false
      this._forceUpdate = false
    }

    componentDidMount = () => {
      this._isComponentWillUnmount = false
      I18nLocalize.on('changeLanguage', this.handleChange)
    }

    componentWillUnmount = () => {
      this._isComponentWillUnmount = true
      I18nLocalize.off('changeLanguage', this.handleChange)
    }

    handleChange = () => {
      if (this._isComponentWillUnmount == false) {
        this._forceUpdate = true
        this.forceUpdate(() => {
          this._forceUpdate = false
        })
      }
    }

    render() {
      return (
        <WrappedComponent {...this.props} isForceUpdate={this._forceUpdate} />
      )
    }
  }
}

export default customWithLanguage
