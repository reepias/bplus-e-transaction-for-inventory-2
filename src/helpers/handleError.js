import { i18n } from 'react-native-i18n-localize'
import store from '../redux/store'
import { logoutUserSuccess } from '../redux/actions'
export const BPLUS_ERROR_TYPE = {
  machineOverLimit: 607,
  unRegisterMachine: 608, // 608 UnRegister Machine
  userIdNotFoundOrWrong: 609,
  internalServerError: 500,
  serviceUnavailable: 503,
  notLogin: 610,
  tableNameNotFound: 614,
  noLicenseOrLicenseExpire: 617,
  //   iCannotDataBase: 601,
  //   iUpdateDateBaseError: 602,
  //   iInvalidBplusAppID: 603,
  //   iRequiredField: 604,
  //   iMobileNotRegister: 605,
  //   iPinInvalid: 606,
  //   iPinTopupNeed: 607,
  //   iTopUpCoins: 609,
  //   iTopUpPin: 610,
  //   iInvalidNtfuGuid: 611,
  //   iAlreadyConfirm: 612,
  //   iConfirmError: 613,
  //   iInvalidMobileNo: 614,
  //   iInvalidServiceID: 615,
  //   iUserNotRegister: 616,
  //   iWrongPassword: 617,
  //   iInvalidSenderMobileNo: 618,
  //   iInvalidToRecipient: 619,
  //   iUserNotLogin: 620,
  //   iUndefineAttachFileName: 621,
  //   iAttachFileNameNotFound: 622,
  //   iInvalidNotiKey: 623,
  //   iInvalidNotiStatus: 624,
  //   iNoCompanyRequestCheckIn: 627,
  //   iLogout: 628,
  //   iInvalidCompanyId: 629,
  //   iMachineOverLimit: 630,
  //   iUnRegisterMachine: 631,
}

export const BPLUS_ERROR_TYPE_MESSAGE = {
  userIdNotFoundOrWrong: `error.userIdNotFoundOrWrong`,
  unRegisterMachine: 'error.unRegisterMachine',
  internalServerError: 'error.internalServerError',
  machineOverLimit: 'error.machineOverLimit',
  notLogin: 'error.notLogin',
  tableNameNotFound: 'error.tableNameNotFound',
  serviceUnavailable: 'error.serviceUnavailable',
  noLicenseOrLicenseExpire: 'error.noLicenseOrLicenseExpire',
  //   iCannotDataBase: 'ไม่สามารถเชื่อต่อฐานข้อมูล',
  //   iUpdateDateBaseError: 'การอัพเดทข้อมูลไม่สำเร็จ',
  //   iInvalidBplusAppID: 'รหัส App ID เกิดข้อผิดพลาด',
  //   iRequiredField: 'ไม่พบข้อมูลที่ต้องการ',
  //   iMobileNotRegister: 'เบอร์โทรศัพท์ไม่ได้ลงทะเบียน',
  //   iPinInvalid: 'PinCode ไม่ถูกต้อง',
  //   iPinTopupNeed: 'PinCode นี้ หมดอายุ',
  //   iTopUpAmount: 'กรุณาเพิ่มครั้งในการส่งข้อความ',
  //   iTopUpCoins: 'กรุณาเพิ่มจำนวนในการส่งข้อความ ',
  //   iTopUpPin: 'กรุณาเพิ่มครั้งในการส่งข้อความ',
  //   iInvalidNtfuGuid: 'กรุณาออกจากระบบแล้ว Login ใหม่',
  //   iAlreadyConfirm: 'การยืนยันสำเร็จ:',
  //   iConfirmError: 'การยืนยันไม่สำเร็จ',
  //   iInvalidMobileNo: 'เบอร์โทรไม่ถูกต้อง',
  //   iInvalidServiceID: 'รหัสบริการไม่ถูกต้อง',
  //   iUserNotRegister: 'ผู้ใช้ยังไม่ถูกลงทะเบียน',
  //   iWrongPassword: 'รหัสผ่านไม่ถูกต้อง',
  //   iInvalidSenderMobileNo: 'เบอร์ผู้ส่งไม่ถูกต้อง',
  //   iInvalidToRecipient: 'ผู้รับไม่ถูกต้อง',
  //   iUserNotLogin: 'ไม่พบข้อมูล ล็อคอิน',
  //   iUndefineAttachFileName: 'ไม่ได้กำหนดชื่อเอกสารแนบ',
  //   iAttachFileNameNotFound: 'ไม่พบเอกสารแนบ ',
  //   iInvalidNotiKey: 'รหัสการแจ้งเตือนไม่ถูกต้อง',
  //   iInvalidNotiStatus: 'สถานะการแจ้งเตือนไม่ถูกต้อง',
  //   iNoCompanyRequestCheckIn: 'ไม่พบชื่อบริษัทที่ทำการร้องขอ',
  //   iLogout: 'ออกจากระบบ',
  //   iInvalidCompanyId: 'รหัสบริษัทไม่ถูกต้อง',
  //   iMachineOverLimit: 'จำนวนเครื่องเกินจากที่กำหนด',
  //   iUnRegisterMachine: 'เครื่องนี้ไม่ได้ลงทะเบียน',
}

export const getErrorTypeByError = err => {
  if (err && err.response && err.response.status) {
    if (err.response.status === 610) {
      // Not login
      setTimeout(() => {
        store.dispatch(logoutUserSuccess())
      }, 3000)
    }
    return getErrorType(err.response.status)
  } else {
    return null
  }
}

export const getErrorMessageTypeByError = err => {
  const result = getErrorTypeByError(err)
  if (result) {
    let message =
      i18n.t(BPLUS_ERROR_TYPE_MESSAGE[result]) +
      ` (${BPLUS_ERROR_TYPE[result]})`
    if (!BPLUS_ERROR_TYPE_MESSAGE[result]) {
      message = 'Unknow Response Error(-1)'
    }
    return message ? message : 'Unknow Response Error(-1)'
  } else {
    return 'Server Error! Please contact administrator'
  }
}

export const getErrorType = errorCode => {
  const result = Object.keys(BPLUS_ERROR_TYPE).filter(k => {
    return BPLUS_ERROR_TYPE[k] + '' == errorCode + ''
  })
  return result ? result : null
}

export function handleError(error) {
  // Error 😨
  if (error.response) {
    /*
     * The request was made and the server responded with a
     * status code that falls out of the range of 2xx
     */
    const { status, data, headers } = error.response
    // console.log(data)
    // console.log(status)
    // console.log(headers)
    if (status === 609) {
      //
    }
  } else if (error.request) {
    /*
     * The request was made but no response was received, `error.request`
     * is an instance of XMLHttpRequest in the browser and an instance
     * of http.ClientRequest in Node.js
     */
    // console.log(error.request)
  } else {
    // Something happened in setting up the request and triggered an Error
    // console.log('Error', error.message)
  }
  //   console.log(error.config)
  throw new Error(error)
}
