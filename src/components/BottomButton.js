import React, { PureComponent } from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'
import { View } from 'native-base'
import { colors, greenStartGardient } from '../constants/styles'
import TextComponent from './Text'

class BottomButton extends PureComponent {
  render() {
    let { okText, cancelText, nextNavigation, backNavigation } = this.props
    okText = okText || 'ตกลง'
    cancelText = cancelText || 'ยกเลิก'
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={nextNavigation}
          style={[styles.button, styles.okButton]}
        >
          <TextComponent style={styles.okText}>{okText}</TextComponent>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={backNavigation}
          style={[styles.button, styles.cancelButton]}
        >
          <TextComponent style={styles.cancelText}>{cancelText}</TextComponent>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  okButton: {
    backgroundColor: greenStartGardient,
  },
  cancelButton: {
    borderColor: greenStartGardient,
    borderWidth: 1,
    backgroundColor: colors.white,
  },
  okText: {
    color: colors.white,
    fontSize: 16,
  },
  cancelText: {
    color: greenStartGardient,
    fontSize: 16,
  },
})

export default BottomButton
