import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { colors } from '../constants/styles'
import LinearGradient from 'react-native-linear-gradient'
import { TextComponent } from '../components'

const ButtonComponent = props => {
  let { text, color1, color2, color3, textColor } = props
  color1 = color1 || colors.gray
  color2 = color2 || colors.gray
  color3 = color3 || colors.gray
  text = text || '-'
  return (
    <TouchableOpacity onPress={props.onPressed} style={styles.touchable}>
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={[color1, color2, color3]}
        style={styles.linearGradient}
        onPressed={() => console.log('object')}
      >
        <TextComponent style={[styles.text, { color: textColor }]}>
          {text}
        </TextComponent>
      </LinearGradient>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  touchable: {
    width: '80%',
    alignSelf: 'center',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    width: '100%',
    height: 45,
  },
  text: {
    marginLeft: 10,
    fontSize: 18,
    color: colors.grayBlack,
    fontFamily: 'Kanit-Regular',
  },
})

export default ButtonComponent
