import React from 'react'
import { StyleSheet } from 'react-native'
import { Text } from 'native-base'
import { colors } from '../constants/styles'

import { widthPercentageToDP as wp } from 'react-native-responsive-screen'

const TextComponent = props => {
  return (
    <Text
      {...props}
      maxFontSizeMultiplier={1}
      allowFontScaling={false}
      style={[styles.defaultStyle, props.style]}
    >
      {props.children}
    </Text>
  )
}

const styles = StyleSheet.create({
  defaultStyle: {
    fontSize: wp('2.1%'),
    color: colors.white,
    fontFamily: 'Kanit-Regular',
  },
})

export default TextComponent
