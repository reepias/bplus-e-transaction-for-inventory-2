import React from 'react'
import { StyleSheet, TextInput } from 'react-native'
import {
  greenStartGardient,
  greenMiddleGradient,
  greenEndGradient,
  colors,
} from '../constants/styles'
import LinearGradient from 'react-native-linear-gradient'
import { textMedium, textBig } from '../constants/fontSize'

const InputComponent = props => {
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
      colors={[greenStartGardient, greenMiddleGradient, greenEndGradient]}
      style={[styles.linearGradient, props.style]}
    >
      <TextInput allowFontScaling={false} {...props} style={styles.text} />
    </LinearGradient>
  )
}

const styles = StyleSheet.create({
  linearGradient: {
    borderRadius: 30,
    width: '80%',
  },
  text: {
    marginLeft: 10,
    fontSize: textMedium,
    fontFamily: 'Kanit-Regular',
    color: colors.white,
    height: 50,
    justifyContent: 'center',
  },
})

export default InputComponent
