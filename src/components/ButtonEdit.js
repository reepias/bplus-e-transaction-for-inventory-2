import React from 'react'
import { StyleSheet, TouchableOpacity, Image } from 'react-native'
import { colors } from '../constants/styles'

const ButtonEdit = props => {
  const { heightValue, onPressed } = props
  return (
    <TouchableOpacity
      onPress={onPressed}
      style={[styles.button, { height: heightValue }]}
    >
      <Image
        style={styles.icon}
        source={require('../assets/icons/ico-info-white.png')}
      />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 50,
    borderRadius: 15,
    backgroundColor: colors.orange,
  },
  icon: {
    width: 25,
    height: 25,
  },
})

export default ButtonEdit
