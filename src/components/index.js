import InputComponent from './Input'
import ButtonComponent from './Button'
import TextComponent from './Text'
import HeaderComponent from './Header'
import FullHeaderComponent from './FullHeader'
import ModalSearchComponent from './ModalSearch'
import BottomButton from './BottomButton'
import ButtonEdit from './ButtonEdit'
import Loader from './Loader'

export {
  InputComponent,
  ButtonComponent,
  TextComponent,
  HeaderComponent,
  FullHeaderComponent,
  ModalSearchComponent,
  BottomButton,
  ButtonEdit,
  Loader,
}
