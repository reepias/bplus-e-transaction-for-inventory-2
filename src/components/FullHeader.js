import React from 'react'
import { StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { startGradient, middleGradient, endGradient } from '../constants/styles'
import { heightWindow } from '../constants/resolutions'
import { Hamburger } from '../constants/Icons'

const FullHeaderComponent = props => {
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0, y: 1 }}
      colors={[endGradient, middleGradient, startGradient]}
      style={[styles.linearGradient, props.style]}
      {...props}
    >
      {props.children}
    </LinearGradient>
  )
}

const styles = StyleSheet.create({
  linearGradient: {
    width: '100%',
    flex: 0.3,
    height: heightWindow / 4.5,
    // position: 'absolute',
    // top: 0,
    // flexDirection: 'row',
    // padding: 10,
  },
  title: {
    fontSize: 20,
    fontFamily: 'Kanit-Regular',
  },
  leftPanel: {
    width: '20%',
    justifyContent: 'flex-start',
  },
  centerPanel: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightPanel: {
    width: '20%',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  backButton: {},
})

export default FullHeaderComponent
