import React from 'react'
import { StyleSheet, View, TextInput } from 'react-native'
import Modal from 'react-native-modal'
import ButtonComponent from './Button'
import { colors, greenStartGardient } from '../constants/styles'
import { heightWindow } from '../constants/resolutions'
import { Label } from 'native-base'
import { textSmall, textMedium } from '../constants/fontSize'

TextInput.defaultProps.allowFontScaling = false

const ModalSearchComponent = props => {
  const { visibleModal, closeModal } = props

  renderModalContent = () => (
    <View style={styles.content}>
      <View style={styles.section}>
        <View style={styles.textView}>
          <Label style={styles.labelPrice}>{`กลุ่มเอกสาร`}</Label>
          <TextInput
            style={styles.textInput}
            bordered
            placeholder={`กลุ่มเอกสาร`}
          />
        </View>
        <View style={styles.textView}>
          <Label style={styles.labelPrice}>{`เลขที่เอกสาร`}</Label>
          <TextInput
            style={styles.textInput}
            bordered
            placeholder={`เลขที่เอกสาร`}
          />
        </View>
        <View style={styles.textView}>
          <Label style={styles.labelPrice}>{`วันที่`}</Label>
          <View style={styles.row}>
            <TextInput
              style={styles.dateInput}
              bordered
              placeholder={`วันที่`}
            />
            <TextInput
              style={styles.dateInput}
              bordered
              placeholder={`ถึงวันที่`}
            />
          </View>
        </View>
      </View>
      <ButtonComponent
        color1={greenStartGardient}
        color2={greenStartGardient}
        color3={greenStartGardient}
        text={`ค้นหา`}
        textColor={colors.white}
        onPressed={closeModal}
      />
    </View>
  )

  return (
    <Modal
      isVisible={visibleModal}
      onSwipeComplete={closeModal}
      swipeDirection={['up', 'left', 'right', 'down']}
      style={styles.bottomModal}
      animationOut="slideOutDown"
      onBackButtonPress={closeModal}
      onBackdropPress={closeModal}
    >
      {renderModalContent()}
    </Modal>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },

  //search
  content: {
    backgroundColor: 'white',
    paddingBottom: 25,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    height: heightWindow / 1.75,
  },
  row: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  section: {
    flex: 1,
    paddingVertical: 30,
  },
  textView: {
    marginHorizontal: 20,
    marginBottom: 10,
  },
  labelPrice: {
    fontFamily: 'Kanit-Regular',
    fontSize: textMedium,
    color: '#333',
    marginBottom: 10,
    paddingHorizontal: 5,
  },
  textInput: {
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    color: '#333',
    width: '100%',
    backgroundColor: '#f0f0f0',
    borderRadius: 0,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  dateInput: {
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    color: '#333',
    width: '45%',
    backgroundColor: '#f0f0f0',
    borderRadius: 0,
    marginHorizontal: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
})

export default ModalSearchComponent
