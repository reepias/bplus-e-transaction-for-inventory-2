import React, { PureComponent, Component } from 'react'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import {
  Setting,
  ArrowBackWhite,
  SearchWhite,
  Worlwide,
} from '../constants/Icons'
import TextComponent from './Text'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen'

import { customWithLanguage } from '../helpers'

import { i18n, I18nLocalize } from 'react-native-i18n-localize'
import AsyncStorage from '@react-native-community/async-storage'

class HeaderComponent extends Component {
  constructor(props) {
    super(props)
  }

  changeLang = async () => {
    const testLange = I18nLocalize.getLocale()
    if (testLange === 'th') {
      I18nLocalize.setLanguage('en')
    } else {
      I18nLocalize.setLanguage('th')
    }
    try {
      await AsyncStorage.setItem('@app_locale', I18nLocalize.getLocale())
    } catch (error) {
      console.log(error)
      alert('swich lang error')
    }
  }

  render() {
    const styles = StyleSheet.create({
      header: {
        flexDirection: 'row',
        height: hp('6%'),
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
      },
      title: {
        fontSize: wp('4.5%'),
        fontFamily: 'Kanit-Regular',
      },
      leftPanel: {
        width: '20%',
        justifyContent: 'flex-start',
      },
      centerPanel: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
      },
      rightPanel: {
        width: '20%',
        justifyContent: 'center',
        alignItems: 'flex-end',
      },
      rightButton: {},
    })
    const { onPressedSearch, onChangeLang } = this.props
    return (
      <View style={[styles.header, this.props.style]}>
        <View style={styles.leftPanel}>
          {this.props.haveBack && (
            <TouchableOpacity onPress={this.props.onPressedBack}>
              <ArrowBackWhite />
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.centerPanel}>
          {this.props.title && (
            <TextComponent style={styles.title}>
              {this.props.title}
            </TextComponent>
          )}
          {this.props.children}
        </View>
        <View style={styles.rightPanel}>
          {this.props.haveSetting && (
            <TouchableOpacity style={styles.rightButton}>
              <Setting />
            </TouchableOpacity>
          )}
          {this.props.haveSearch && (
            <TouchableOpacity
              onPress={onPressedSearch}
              style={styles.rightButton}
            >
              <SearchWhite />
            </TouchableOpacity>
          )}
          {this.props.haveChangeLang && (
            <TouchableOpacity
              onPress={this.changeLang}
              style={styles.rightButton}
            >
              <Worlwide style={{ height: 20, width: 20 }} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    )
  }
}

export default customWithLanguage(HeaderComponent)
