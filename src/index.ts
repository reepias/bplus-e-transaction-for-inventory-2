import { Navigation } from 'react-native-navigation'
import {
  YellowBox,
  NetInfo,
  AsyncStorage,
  UIManager,
  Platform,
  Dimensions,
} from 'react-native'
import { pushTutorialScreen } from './navigations'
import 'moment/locale/th'

// Ignore yellow box
YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated',
  "Module RCTImageLoader requires main queue setup since it overrides `init` but doesn't implement `requiresMainQueueSetup`. In a future release React Native will default to initializing all native modules on a background thread unless explicitly opted-out of.",
  'Could not find image',
  'RCTBridge required dispatch_sync',
  'Required dispatch_sync to load constants',
  'Remote debugger is in a background tab',
  'ViewPagerAndroid has been',
  'Deprecation warning',
  'Warning: ListView is deprecated and will be removed'
])

Navigation.events().registerAppLaunchedListener(() => pushTutorialScreen())
