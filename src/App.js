import React, { PureComponent } from 'react'
import { StyleSheet, Text } from 'react-native'
import { ContainerComponent } from './src/containers'

const theme = {
  Button: {
    raised: true,
    titleStyle: {
      color: 'red',
    },
  },
}

export default class App extends PureComponent {
  render() {
    return (
      <ContainerComponent>
        <Text style={styles.text}>Heading 1</Text>
        <Text style={styles.text}>ทดสอบภาษาไทย</Text>
      </ContainerComponent>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    fontFamily: 'Kanit-Regular',
  },
})
