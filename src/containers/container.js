import React from 'react'
import { StyleSheet, ScrollView, View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { startGradient, middleGradient, endGradient } from '../constants/styles'

const Container = props => {
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0, y: 1 }}
      colors={[startGradient, middleGradient, endGradient]}
      style={[styles.linearGradient, props.style]}
      {...props}
    >
      {props.disabledScrollView ? (
        <View style={{ flex: 1 }}>{props.children}</View>
      ) : (
        <ScrollView style={{ flex: 1 }} bounces={false}>
          {props.children}
        </ScrollView>
      )}
    </LinearGradient>
  )
}

var styles = StyleSheet.create({
  linearGradient: {
    backgroundColor: startGradient,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
})

export default Container
