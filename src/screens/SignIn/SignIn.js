import React, { PureComponent } from 'react'
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
} from 'react-native'
import { ContainerComponent } from '../../containers'
import {
  InputComponent,
  ButtonComponent,
  TextComponent,
  Loader,
} from '../../components'
import { widthWindow, heightWindow } from '../../constants/resolutions'
import { Setting, Worlwide } from '../../constants/Icons'
import { push } from '../../helpers/navigations'
import { SIGN_UP_SCREEN, SERVICE_SCREEN } from '../../navigations/Screens'
import { connect } from 'react-redux'
import { loginUserAction } from '../../redux/actions'
import validate from '../../rules/LoginFormValidationRules'
import isEmpty from '../../helpers/isEmpty'
import { preventFunction, customWithLanguage } from '../../helpers'
import { i18n, I18nLocalize } from 'react-native-i18n-localize'
import AsyncStorage from '@react-native-community/async-storage'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

import { textSmall, textMedium, textBig } from '../../constants/fontSize'

class SignIn extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      username: 'business',
      password: 'system',
      error: [],
    }
  }

  componentDidMount() {}

  onPressedSignIn = async () => {
    const { username, password } = this.state
    const user = {
      username,
      password,
    }
    const error = await validate(user)
    if (!isEmpty(error)) {
      let message = Object.values(error)
        .map((item, key) => item)
        .join('\n')
      Alert.alert(
        `${i18n.t(`login.error.title`)}`,
        `${message}`,
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        { cancelable: false }
      )
    } else {
      this.props.loginUserAction(user)
    }
  }
  onPressedSignUp = () => {
    const object = {
      componentName: SIGN_UP_SCREEN,
    }
    push(this.props.componentId, object)
  }
  onPressedService = () => {
    const object = {
      componentName: SERVICE_SCREEN,
    }
    push(this.props.componentId, object)
  }
  render() {
    const { isLoading } = this.props
    return (
      <ContainerComponent style={styles.container}>
        {isLoading && <Loader loading={isLoading} />}
        <View style={styles.header}>
          <TouchableOpacity style={styles.touchIcon}>
            <Worlwide />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.onPressedService}
            style={styles.touchIcon}
          >
            <Setting />
          </TouchableOpacity>
        </View>
        <KeyboardAvoidingView behavior="position" style={styles.content}>
          <View style={styles.logoPanel}>
            <Image
              style={styles.logo}
              source={require('../../assets/images/logo-blue.png')}
            />
            <View style={styles.column}>
              <TextComponent
                style={styles.textBig}
              >{`e-Transactions`}</TextComponent>
              <TouchableOpacity>
                <TextComponent
                  style={styles.textMiddle}
                >{`for Inventory`}</TextComponent>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.inputPanel}>
            <InputComponent
              style={styles.input}
              placeholder={i18n.t(`username.text`)}
              onChangeText={value => this.setState({ username: value })}
              value={this.state.username}
            />
          </View>
          <View style={styles.inputPanel}>
            <InputComponent
              style={styles.input}
              placeholder={`รหัสผ่าน`}
              secureTextEntry={true}
              onChangeText={value => this.setState({ password: value })}
              value={this.state.password}
            />
          </View>
          <View style={styles.buttonPanel}>
            <ButtonComponent
              onPressed={preventFunction.exec(this.onPressedSignIn)}
              text={i18n.t(`login.loginButton`)}
            />
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.text}>
              {i18n.t(`login.dotNotHaveAccount`)}
            </TextComponent>
            <TouchableOpacity
              disabled={isLoading}
              onPress={preventFunction.exec(this.onPressedSignUp)}
            >
              <TextComponent
                style={[styles.text, { textDecorationLine: 'underline' }]}
              >{`ลงทะเบียน`}</TextComponent>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </ContainerComponent>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    height: heightWindow,
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center',
    // height: heightWindow - 100,
    height: hp('80%'),
  },
  header: {
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  logo: {
    width: widthWindow / 2,
    height: widthWindow / 2,
    alignSelf: 'center',
  },
  logoPanel: {
    paddingBottom: 60,
  },
  touchIcon: {
    paddingHorizontal: 6,
  },
  inputPanel: {
    width: widthWindow - 30,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    height: 50,
    justifyContent: 'center',
  },
  buttonPanel: {
    width: widthWindow - 30,
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBig: {
    color: '#FFF',
    fontFamily: 'Kanit-Bold',
    fontSize: textBig,
  },
  textMiddle: {
    color: '#FFF',
    fontFamily: 'Kanit-Regular',
    fontSize: textMedium,
  },
  text: {
    color: '#FFF',
    fontSize: textSmall,
  },
  row: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  column: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

function mapStateToProps({ ui }) {
  const { isLoading } = ui
  return {
    isLoading,
  }
}

export default connect(
  mapStateToProps,
  {
    loginUserAction,
  }
)(customWithLanguage(SignIn))
