import { StyleSheet, Platform } from 'react-native'
import {
  colors,
  startGradient,
  middleGradient,
  greenStartGardient,
} from '../../constants/styles'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen'

import { textSmall, textMedium, textBig } from '../../constants/fontSize'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    paddingVertical: 10,
    justifyContent: 'flex-start',
  },
  topBar: {
    backgroundColor: startGradient,
  },
  footer: {
    backgroundColor: 'transparent',
    height: hp('6%'),
  },
  panelList: {
    flexGrow: 1,
    backgroundColor: 'transparent',
  },
  descPanel: {
    height: hp('4%'),
    backgroundColor: colors.grayWhite,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    flexDirection: 'row',
    marginBottom: 6,
  },
  buttonPrimary: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: greenStartGardient,
    height: hp('4%'),
    paddingHorizontal: 5,
    borderRadius: 5,
    borderColor: colors.white,
    borderWidth: 0.5,
  },
  text: {
    fontSize: 16,
    color: colors.primary,
  },
  groupDetailPanel: {
    // height: heightWindow <= 568 ? hp('40%') : hp('35%'),
    flex: 1,
  },
  customerPanel: {
    // height: hp('9%'),
    // flex: 0.18,
    paddingBottom: 15,
  },

  buttonAdd: {
    backgroundColor: colors.greenStart,
    justifyContent: 'center',
    marginHorizontal: 20,
    margin: 10,
  },
  buttonAddDisabled: {
    backgroundColor: colors.gray,
    justifyContent: 'center',
    marginHorizontal: 20,
    margin: 10,
  },

  // ** Tab ** //
  tabStyle: {
    backgroundColor: colors.primary,
  },
  activeTabStyle: {
    backgroundColor: colors.primary,
    borderColor: colors.white,
    color: colors.white,
  },
  activeTextStyle: {
    // fontSize: 14,
    fontSize: textMedium,
    color: colors.white,
    fontFamily: 'Kanit-Regular',
  },
  textStyle: {
    color: colors.white,
    // fontSize: 14,
    fontSize: textMedium,
    fontFamily: 'Kanit-Regular',
  },
  textSearch: {
    color: colors.white,
    fontSize: textMedium,
    fontFamily: 'Kanit-Regular',
  },
})
