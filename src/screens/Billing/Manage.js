import React, { Component } from 'react'
import { SafeAreaView, View, TextInput, Keyboard } from 'react-native'
import { Footer, Label, Content, Icon, Button } from 'native-base'
import { TextComponent, HeaderComponent, BottomButton } from '../../components'
import { pop } from '../../helpers/navigations'
import {
  ITEM_LIST_SCREEN,
  WARE_LOCATION_SEARCH_SCREEN,
} from '../../navigations/Screens'
import styles from './styles'
import Style from '../../styles/Theme/Style'
import Styles from '../../styles/Manage/Style'
import { colors } from '../../constants/styles'

import { connect } from 'react-redux'

import { findProduct, findStorageLocation } from '../../redux/actions'

import { Navigation } from 'react-native-navigation'

import { preventFunction, customWithLanguage } from '../../helpers'

import { i18n } from 'react-native-i18n-localize'

class Manage extends Component {
  constructor(props) {
    super(props)
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    )
    this.state = {
      item: {
        GOODS_CODE: '', // รหัสซื้อขาย
        SKU_NAME: '', // รายการ
        UTQ_NAME: '', // หน่วยนับ
        QUANTITY: '', // จำนวน
        PER_UNIT: '', // ต่อหน่วย
        DISCOUNT: '', // ส่วนลด
        AMOUNT: '', // จำนวนเงิน
        FREE: '', // แถม
        WL_CODE: '', // รหัสตำแหน่งเก็บ
      },
      openKeyboard: false,
    }
  }

  _keyboardDidShow = () => {
    this.setState({ openKeyboard: true })
  }

  _keyboardDidHide = () => {
    this.setState({ openKeyboard: false })
  }

  _backNavigation = () => {
    pop(this.props.componentId)
  }

  onItemList = () => {
    // const { GOODS_CODE } = this.state.item
    Navigation.push(this.props.componentId, {
      component: {
        name: ITEM_LIST_SCREEN,
        passProps: {
          onSelectItem: this.onSelectItem,
          // searchTerm: GOODS_CODE,
        },
      },
    })
  }

  onWareHouseSearch = () => {
    // const { GOODS_CODE } = this.state.item
    Navigation.push(this.props.componentId, {
      component: {
        name: WARE_LOCATION_SEARCH_SCREEN,
        passProps: {
          onSelectItem: this.onSelectWareHouse,
          // searchTerm: GOODS_CODE,
        },
      },
    })
  }

  onSelectWareHouse = warehouse => {
    const { WL_CODE } = warehouse
    console.log('warehouse: ', warehouse)
    this.setState({
      item: {
        ...this.state.item,
        WL_CODE,
      },
    })
    pop(this.props.componentId)
  }

  onSelectItem = item => {
    console.log('item: ', item)
    const { SKU_CODE, SKU_NAME, UTQ_NAME } = item

    this.setState({
      item: {
        GOODS_CODE: SKU_CODE,
        SKU_NAME,
        UTQ_NAME,
        QUANTITY: '1',
        PER_UNIT: `${Math.floor(Math.random() * 1000) + 1}.00`,
        DISCOUNT: '',
        AMOUNT: '1.00',
        FREE: '0.00',
        WL_CODE: '',
      },
    })
    pop(this.props.componentId)
  }

  onChangeGoodsCode = value => {
    this.setState({
      item: {
        ...this.state.item,
        GOODS_CODE: value,
      },
    })
    console.log('value: ', value)
  }

  onChangeWareHouse = value => {
    this.setState({
      item: {
        ...this.state.item,
        WL_CODE: value,
      },
    })
  }

  onFindWareHouse = () => {
    const { WL_CODE } = this.state.item
    const payload = {
      searchTerm: WL_CODE,
      componentId: this.props.componentId,
      onSelectItem: this.onSelectWareHouse,
    }
    this.props.findStorageLocation(payload)
  }

  searchGoodsCode = () => {
    const { GOODS_CODE } = this.state.item
    console.log('GOODS_CODE: ', GOODS_CODE)
    const payload = {
      searchTerm: GOODS_CODE,
      componentId: this.props.componentId,
      onSelectItem: this.onSelectItem,
    }
    this.props.findProduct(payload)
  }

  render() {
    const {
      GOODS_CODE,
      SKU_NAME,
      UTQ_NAME,
      QUANTITY,
      PER_UNIT,
      DISCOUNT,
      AMOUNT,
      FREE,
      WL_CODE,
    } = this.state.item
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <HeaderComponent
            onPressedBack={this._backNavigation}
            haveBack={true}
            // haveChangeLang={true}
            title={`${this.props.title}`}
            style={styles.topBar}
          />

          <Content
            style={Style.layoutInner}
            contentContainerStyle={Style.layoutContent}
          >
            <View style={Styles.section}>
              <View style={Styles.step}>
                <TextComponent style={Styles.stepText}>{`${i18n.t(
                  `description`
                )}`}</TextComponent>
              </View>

              {/* <Button
                style={Styles.searchButton}
                onPress={preventFunction.exec(this.onItemList)}
              >
                <TextComponent style={styles.textSearch}>{`${i18n.t(
                  `search`
                )}`}</TextComponent>
                <Icon
                  type="FontAwesome"
                  name="search"
                  style={{
                    fontSize: 20,
                    color: colors.grayWhite,
                    textAlign: 'right',
                  }}
                />
              </Button> */}

              <View style={Styles.row}>
                <View style={Styles.rowInlineBorder}>
                  <View style={Styles.colLeft}>
                    <Label style={Styles.label}>{`${i18n.t(
                      `tradingCode`
                    )}`}</Label>

                    <TextInput
                      returnKeyType="search"
                      style={Styles.textInputLong}
                      placeholder={''}
                      clearButtonMode="while-editing"
                      onSubmitEditing={this.searchGoodsCode}
                      underlineColorAndroid="transparent"
                      onChangeText={this.onChangeGoodsCode}
                      value={GOODS_CODE}
                    />
                  </View>
                  <View style={Styles.colRight}>
                    <Button
                      onPress={preventFunction.exec(this.onItemList)}
                      style={Styles.searchButton}
                    >
                      <Icon
                        type="FontAwesome"
                        name="search"
                        style={{
                          fontSize: 20,
                          color: colors.grayWhite,
                          textAlign: 'right',
                        }}
                      />
                    </Button>
                  </View>
                </View>
              </View>

              <View style={Styles.row}>
                <Label style={Styles.label}>{`รายการ`}</Label>
                <TextInput
                  editable={false}
                  style={Styles.textInput}
                  placeholder={''}
                  value={SKU_NAME}
                />
              </View>

              <View style={Styles.row}>
                <View style={Styles.rowInline}>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`หน่วยนับ`}</Label>
                    <TextInput
                      editable={false}
                      style={Styles.textInput}
                      placeholder={''}
                      value={UTQ_NAME}
                    />
                  </View>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`จำนวน`}</Label>
                    <TextInput
                      style={Styles.textInput}
                      placeholder={''}
                      value={QUANTITY}
                    />
                  </View>
                </View>
              </View>

              <View style={Styles.row}>
                <View style={Styles.rowInline}>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`ต่อหน่วย`}</Label>
                    <TextInput
                      style={Styles.textInput}
                      placeholder={''}
                      value={PER_UNIT}
                    />
                  </View>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`ส่วนลด`}</Label>
                    <TextInput
                      style={Styles.textInput}
                      placeholder={''}
                      value={DISCOUNT}
                    />
                  </View>
                </View>
              </View>

              <View style={Styles.row}>
                <View style={Styles.rowInline}>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`จำนวนเงิน`}</Label>
                    <TextInput
                      style={Styles.textInput}
                      placeholder={''}
                      value={AMOUNT}
                    />
                  </View>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`แถม`}</Label>
                    <TextInput
                      style={Styles.textInput}
                      placeholder={''}
                      value={FREE}
                    />
                  </View>
                </View>
              </View>

              <View style={Styles.row}>
                <View style={Styles.rowInlineBorder}>
                  <View style={Styles.colLeft}>
                    <Label style={Styles.label}>{`ตน.เก็บ`}</Label>
                    <TextInput
                      returnKeyType="search"
                      style={Styles.textInputLong}
                      placeholder={''}
                      onChangeText={this.onChangeWareHouse}
                      onSubmitEditing={this.onFindWareHouse}
                      value={WL_CODE}
                    />
                  </View>
                  <View style={Styles.colRight}>
                    <Button
                      onPress={preventFunction.exec(this.onWareHouseSearch)}
                      style={Styles.searchButton}
                    >
                      <Icon
                        type="FontAwesome"
                        name="search"
                        style={{
                          fontSize: 20,
                          color: colors.grayWhite,
                          textAlign: 'right',
                        }}
                      />
                    </Button>
                  </View>
                </View>
              </View>

              <View style={Styles.row}>
                <Label style={Styles.label}>{`คำอธิบายสินค้า`}</Label>
                <TextInput
                  style={Styles.textInputMulti}
                  multiline={true}
                  numberOfLines={5}
                  placeholder={''}
                  value={''}
                />
              </View>

              {/* <View style={Styles.step}>
                <TextComponent
                  style={Styles.stepText}
                >{`รายละเอียดเพิ่มเติม`}</TextComponent>
              </View>

              <View style={Styles.row}>
                <View style={Styles.rowInline}>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`เลขล็อต`}</Label>
                    <TextInput style={Styles.textInput} placeholder={''} />
                  </View>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`เลขกำกับ`}</Label>
                    <TextInput style={Styles.textInput} placeholder={''} />
                  </View>
                </View>
              </View>

              <View style={Styles.row}>
                <View style={Styles.rowInline}>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`วันที่ผลิต`}</Label>
                    <TextInput style={Styles.textInput} placeholder={''} />
                  </View>
                  <View style={Styles.col}>
                    <Label style={Styles.label}>{`วันที่หมดอายุ`}</Label>
                    <TextInput style={Styles.textInput} placeholder={''} />
                  </View>
                </View>
              </View> */}
            </View>
          </Content>
          <Footer
            style={[
              styles.footer,
              { display: this.state.openKeyboard ? 'none' : 'flex' },
            ]}
          >
            <BottomButton
              nextNavigation={preventFunction.exec(this._backNavigation)}
              backNavigation={preventFunction.exec(this._backNavigation)}
            />
          </Footer>
        </View>
      </SafeAreaView>
    )
  }
}

function mapStateToProps({ document }) {
  const { prepareDocument } = document
  return {
    prepareDocument,
  }
}

export default connect(
  mapStateToProps,
  {
    findProduct,
    findStorageLocation,
  }
)(customWithLanguage(Manage))
