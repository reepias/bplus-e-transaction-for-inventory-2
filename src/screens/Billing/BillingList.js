import React, { PureComponent } from 'react'
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native'
import { TextComponent } from '../../components'
import { colors } from '../../constants/styles'
import { data } from '../../mock/itemData'
import { MANAGE_BILLING_SCREEN } from '../../navigations/Screens'
import { Navigation } from 'react-native-navigation'
import { Pencil, Trash } from '../../constants/Icons'

const numColumns = 1

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns)

  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns
  while (
    numberOfElementsLastRow !== numColumns &&
    numberOfElementsLastRow !== 0
  ) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true })
    numberOfElementsLastRow++
  }

  return data
}

class BillingList extends PureComponent {
  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return <View style={[styles.item, styles.itemInvisible]} />
    }
    return (
      <View style={styles.item}>
        <View style={styles.panelOne}>
          <View>
            <TextComponent style={styles.textMediumPrimary}>
              รหัสซื้อขาย
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>
              8852099021532
            </TextComponent>
          </View>
          <View>
            <TextComponent style={styles.textMediumPrimary}>
              รายการ
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>
              กาแฟหอมหวน ขวดฝาแดง 200 กรัม
            </TextComponent>
          </View>
        </View>
        <View
          style={{
            borderColor: colors.gray,
            borderRightWidth: 1,
            marginVertical: 10,
            backgroundColor: colors.grayWhite,
          }}
        />
        <View style={styles.panelTwo}>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>
              หน่วยนับ
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>
              หีบ * 12
            </TextComponent>
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>
              จำนวน
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>1.00</TextComponent>
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>
              ต่อหน่วย
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>325.000</TextComponent>
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>
              ส่วนลด
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>10+5 บ</TextComponent>
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>
              จำนวนเงิน
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>287.50</TextComponent>
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>แถม</TextComponent>
            <TextComponent style={styles.textMediumGray}>0.00</TextComponent>
          </View>
          <View style={styles.row}>
            <TextComponent style={styles.textMediumPrimary}>
              ตน.เก็บ
            </TextComponent>
            <TextComponent style={styles.textMediumGray}>HO.NA</TextComponent>
          </View>
        </View>
        <View style={styles.panelThree}>
          <TouchableOpacity
            onPress={this.onPressNavigation}
            style={styles.editButton}
          >
            <Pencil style={styles.icon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.deleteButton}>
            <Trash style={styles.icon} />
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  onPressNavigation = () => {
    Navigation.push(this.props.componentId, {
      component: {
        name: MANAGE_BILLING_SCREEN,
        passProps: {
          mode: `edit`,
          title: `แก้ไขรายการ`,
        },
        options: {
          topBar: {
            drawBehind: true,
            visible: false,
            animate: false,
          },
        },
      },
    })
  }

  _keyExtractor = (item, index) => `${index}`

  render() {
    return (
      <FlatList
        data={formatData(data, numColumns)}
        style={styles.container}
        renderItem={this.renderItem}
        numColumns={numColumns}
        keyExtractor={this._keyExtractor}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    flexDirection: 'row',
    width: '100%',
    marginBottom: 6,
    flex: 1,
    backgroundColor: colors.grayWhite,
    // padding: 10,
  },
  panelOne: {
    padding: 10,
    width: '40%',
    backgroundColor: colors.grayWhite,
    flexDirection: 'column',
  },
  panelTwo: {
    padding: 10,
    width: '50%',
    backgroundColor: colors.grayWhite,
    flexDirection: 'column',
  },
  panelThree: {
    width: '10%',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  editButton: {
    backgroundColor: '#F8A938',
    height: '48%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
  },
  deleteButton: {
    backgroundColor: '#FF3E3D',
    height: '48%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 3,
  },
  icon: {
    width: 20,
    height: 20,
  },
  textMediumPrimary: {
    fontSize: 14,
    color: colors.primary,
  },
  textMediumGray: {
    fontSize: 14,
    color: colors.grayBlack,
  },
})

export default BillingList
