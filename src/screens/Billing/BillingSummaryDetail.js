import React, { PureComponent } from 'react'
import { StyleSheet, View, TextInput, Platform } from 'react-native'
import { Input } from 'native-base'
import { TextComponent } from '../../components'
import { colors } from '../../constants/styles'

class BillingSummaryDetail extends PureComponent {
  render() {
    return (
      <View style={styles.item}>
        <View style={styles.row}>
          <View style={styles.panelOne}>
            <TextComponent style={styles.textMediumPrimary}>
              ส่วนลด
            </TextComponent>
          </View>
          <Input
            defaultValue="10"
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
          <Input
            defaultValue="28.75"
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.panelOne}>
            <TextComponent style={styles.textMediumPrimaryBorderBottom}>
              หักมัดจำ
            </TextComponent>
          </View>
          <View style={styles.blank} />
          <Input
            defaultValue="0.00"
            keyboardType="decimal-pad"
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.panelOne}>
            <TextComponent style={styles.textMediumPrimary}>
              มูลค่าสินค้า
            </TextComponent>
          </View>
          <View style={styles.blank} />
          <Input
            defaultValue="258.75"
            keyboardType="decimal-pad"
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.panelOne}>
            <TextComponent numberOfLines={1} style={styles.textMediumPrimary}>
              ภาษีมูลค่าเพิ่ม
            </TextComponent>
          </View>
          <View style={styles.blank} />
          <Input
            defaultValue="18.11"
            keyboardType="decimal-pad"
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.panelOne}>
            <TextComponent numberOfLines={1} style={styles.textMediumPrimary}>
              มูลค่ายกเว้น
            </TextComponent>
          </View>
          <View style={styles.blank} />
          <Input
            defaultValue="0.00"
            keyboardType="decimal-pad"
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
        </View>

        <View style={styles.row}>
          <View style={styles.panelOne}>
            <TextComponent
              numberOfLines={1}
              style={[styles.textMediumPrimary, { fontFamily: 'Kanit-Bold' }]}
            >
              รวมทั้งสิ้น
            </TextComponent>
          </View>
          <View style={styles.blank} />
          <Input
            defaultValue="276.86"
            keyboardType="decimal-pad"
            underlineColorAndroid="transparent"
            style={[styles.textInput, { fontFamily: 'Kanit-Bold' }]}
          />
        </View>
        <View style={styles.row}>
          <TextInput
            style={styles.textInputMulti}
            multiline={true}
            numberOfLines={5}
            placeholder={'ช่องใส่หมายเหตุ'}
            value={''}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  blank: {
    width: 120,
    marginHorizontal: 5,
  },
  textInputMulti: {
    fontFamily: 'Kanit-Regular',
    borderColor: '#cecece',
    borderWidth: 0.5,
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: '#fff',
    fontSize: 14,
    width: '100%',
    marginBottom: 10,
    ...Platform.select({
      ios: {
        height: 100,
        paddingTop: 20,
      },
      android: {
        textAlignVertical: 'top',
      },
    }),
  },
  textInput: {
    fontFamily: 'Kanit-Regular',
    fontSize: 14,
    color: '#333',
    height: 25,
    padding: 0,
    textAlign: 'right',
    backgroundColor: '#fff',
    borderRadius: 0,
    marginHorizontal: 5,
    paddingHorizontal: 10,
    borderColor: '#cecece',
    borderWidth: 0.5,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  item: {
    flexDirection: 'column',
  },
  panelOne: {
    width: '35%',
    flexDirection: 'row',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 5,
  },
  icon: {
    width: 20,
    height: 20,
  },
  textMediumPrimary: {
    fontSize: 14,
    color: colors.primary,
  },
  textMediumPrimaryBorderBottom: {
    fontSize: 14,
    color: colors.primary,
    textDecorationLine: 'underline',
  },
  textMediumGray: {
    fontSize: 14,
    color: colors.grayBlack,
  },
})

export default BillingSummaryDetail
