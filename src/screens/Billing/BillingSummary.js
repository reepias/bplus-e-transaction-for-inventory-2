import React, { PureComponent } from 'react'
import { SafeAreaView, View, ScrollView, TouchableOpacity } from 'react-native'
import { Footer, Icon, Title, Body, Content, Item, Input } from 'native-base'
import { TextComponent, HeaderComponent, BottomButton } from '../../components'
import { pop, push } from '../../helpers/navigations'
import styles from './styles'
import CustomerInput from './CustomerInput'
import GroupDetail from './GroupDetail'
import BillingSummaryDetail from './BillingSummaryDetail'
class BillingSummary extends PureComponent {
  constructor(props) {
    super(props)
  }
  _backNavigation = () => {
    pop(this.props.componentId)
  }
  _nextNavigation = () => {
    pop(this.props.componentId)
  }
  render() {
    const { title } = this.props
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <HeaderComponent
            onPressedBack={() => pop(this.props.componentId)}
            haveBack={true}
            title={`สรุปรายการ - ${title}`}
            style={styles.topBar}
          />
          <View style={styles.content}>
            <View style={styles.customerPanel}>
              <View>
                <CustomerInput
                  show={false}
                  componentId={this.props.componentId}
                />
              </View>
            </View>
            <View style={styles.groupDetailPanel}>
              <GroupDetail edit={false} />
            </View>
            <View style={styles.descPanel}>
              <TextComponent style={styles.text}>รายละเอียด</TextComponent>
            </View>
            <View style={styles.panelList}>
              <ScrollView scrollEnabled={true}>
                <BillingSummaryDetail />
              </ScrollView>
            </View>
          </View>
        </View>
        <Footer style={styles.footer}>
          <BottomButton
            okText={`บันทึก`}
            cancelText={`ยกเลิก`}
            backNavigation={this._backNavigation}
            nextNavigation={this._nextNavigation}
          />
        </Footer>
      </SafeAreaView>
    )
  }
}

export default BillingSummary
