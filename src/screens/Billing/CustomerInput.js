import React, { PureComponent } from 'react'
import { StyleSheet } from 'react-native'
import { View, Input, Label } from 'native-base'
import { TextComponent, ButtonEdit } from '../../components'
import { colors } from '../../constants/styles'
import { CUSTOMER_SCREEN } from '../../navigations/Screens'
import { push } from '../../helpers/navigations'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

import { connect } from 'react-redux'
import { i18n, withLanguage } from 'react-native-i18n-localize'

import { preventFunction } from '../../helpers'

import { textSmall, textMedium } from '../../constants/fontSize'

//customer
import { getCustomer } from '../../redux/actions/index'

class CustomerInput extends PureComponent {
  constructor(props) {
    super(props)
    const customerType = this.props.chooseDocument.customer
    this.state = {
      width: null,
      height: hp('4%'),
      x: null,
      y: null,
      show: this.props.show,
      customer: '',
      customerType:
        customerType === 'AP'
          ? 'เจ้าหนี้'
          : customerType === 'AR'
          ? `ลูกหนี้`
          : '',
    }
  }

  onLayout = e => {
    this.setState({
      width: e.nativeEvent.layout.width,
      height: e.nativeEvent.layout.height - 20,
      x: e.nativeEvent.layout.x,
      y: e.nativeEvent.layout.y,
    })
  }
  onPressed = () => {
    const object = {
      componentName: CUSTOMER_SCREEN,
      passProps: {
        title: this.props.title,
        notifyChange: c => {
          if (c) {
            this.setState({ customer: c })
          }
        },
      },
    }
    push(this.props.componentId, object)
  }

  searchCustomer = () => {
    const searchTerm = this.state.customer
    console.log('searchTerm: ', searchTerm)
    //dispatch to search customer
    const mode = this.props.chooseDocument.customer
    this.props.getCustomer(searchTerm, mode, this.props.componentId)
  }

  render() {
    const styles = StyleSheet.create({
      container: {
        padding: 2,
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
      content: {
        width: '80%',
        flexDirection: 'column',
      },
      form: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
      },
      buttonPanel: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
      },
      descPanel: {
        flexDirection: 'row',
        padding: 1,
      },
      desc: {
        flex: 1,
        color: '#666666',
        // fontSize: 14,
        fontSize: textSmall,
      },
      inputBox: {
        margin: 5,
        width: wp('15%'),
        // height: hp('4%'),
        height: 40,
        padding: 0,
        paddingLeft: 10,
        justifyContent: 'center',
        backgroundColor: '#fff',
        borderColor: '#cecece',
        borderWidth: 0.5,
        // fontSize: 14,
        fontSize: textSmall,
        fontFamily: 'Kanit-Regular',
      },
      customerText: {
        color: colors.primary,
        // fontSize: 16,
        fontSize: textMedium,
      },
    })
    const { height, show, customerType } = this.state
    const { prepareDocument } = this.props
    console.log('customer input: ', prepareDocument)
    return (
      <View style={styles.container}>
        <View onLayout={this.onLayout} style={styles.content}>
          <View style={styles.form}>
            <Label>
              <TextComponent style={styles.customerText}>
                {`${customerType}`}
              </TextComponent>
            </Label>
            {show != false && (
              <Input
                underlineColorAndroid="transparent"
                style={styles.inputBox}
                returnKeyType="search"
                // autoFocus={true}
                selectionColor={colors.orange}
                onSubmitEditing={this.searchCustomer}
                clearButtonMode="while-editing"
                placeholderTextColor="#bbb"
                placeholder={`รหัส${customerType}`}
                onChangeText={val => this.setState({ customer: val })}
                value={this.state.customer}
              />
            )}
          </View>
          <View style={styles.descPanel}>
            <TextComponent numberOfLines={2} style={styles.desc}>
              {prepareDocument &&
                (prepareDocument.AP_NAME || prepareDocument.AR_NAME)}
            </TextComponent>
          </View>
        </View>
        {show != false && (
          <View style={styles.buttonPanel}>
            <ButtonEdit
              onPressed={preventFunction.exec(this.onPressed)}
              heightValue={height}
            />
          </View>
        )}
      </View>
    )
  }
}

function mapStateToProps({ customer, document }) {
  const { isFetching } = customer
  const { chooseDocument, prepareDocument } = document
  return {
    isFetching,
    chooseDocument,
    prepareDocument,
  }
}

export default connect(
  mapStateToProps,
  {
    getCustomer,
  }
)(withLanguage(CustomerInput))
