import React, { PureComponent } from 'react'
import { StyleSheet, TextInput, TouchableOpacity } from 'react-native'
import { Container, Content, Label } from 'native-base'
import { colors } from '../../constants/styles'
import { Column as Col, Row } from 'react-native-responsive-grid'

import { Navigation } from 'react-native-navigation'
import { DATE_PICKER_SCREEN } from '../../navigations/Screens'

import { TextComponent } from '../../components'

import RNPickerSelect from 'react-native-picker-select'

import moment from 'moment'

import { preventFunction } from '../../helpers'

import { connect } from 'react-redux'
import { i18n, withLanguage } from 'react-native-i18n-localize'
import { setPrepareDocument, getCreditorAgreements } from '../../redux/actions'

import { dateThaiFormat } from '../../helpers'

import { textSmall, textMedium } from '../../constants/fontSize'

Label.defaultProps.allowFontScaling = false
TextInput.defaultProps.allowFontScaling = false

class GroupDetail extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      edit: this.props.edit,
      creditorAgreement: undefined,
    }
  }
  componentDidMount() {
    this.props.getCreditorAgreements()
  }

  onDatePicker = (key, value) => () => {
    console.log('onDatePicker key: ', key)
    console.log('onDatePicker value: ', value)
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: DATE_PICKER_SCREEN,
              passProps: {
                inputKey: key,
                onOK: this.onOK,
                value: value,
              },
            },
          },
        ],
      },
    })
  }

  onOK = (key, value) => {
    console.log('key: ', key)
    console.log('value: ', value)
    let object = {
      ...this.props.prepareDocument,
      [key]: value,
    }

    if (key === 'DI_DATE') {
      object = {
        ...object,
        DELIVERY_DATE: value,
        EXPIRED_DATE: moment(value)
          .add(60, 'days')
          .format('YYYY-MM-DD'),
        DUE_DATE: moment(value)
          .add(60, 'days')
          .format('YYYY-MM-DD'),
      }
    }

    console.log('object: ', object)
    this.props.setPrepareDocument(object)
  }

  onChangeCreditorAgreement = value => {
    console.log('onChangeCreditorAgreement: ', value)
    this.setState({
      creditorAgreement: value,
    })
  }

  render() {
    const { edit } = this.state
    const { chooseDocument, prepareDocument, disabled } = this.props
    console.log('disabled: ', disabled)
    const {
      DI_DATE,
      DELIVERY_DATE,
      EXPIRED_DATE,
      DUE_DATE,
      TAX_INVOICE_NUMBER,
      INVOICE_NUMBER,
      DATE_OF_TAX_INVOICE,
      AGREEMENT,
    } = prepareDocument
    const { inputs } = chooseDocument
    const {
      documentDate,
      documentNumber,
      dateOfTaxInvoice,
      taxInvoiceNumber,
      agreement,
      dueDate,
      deliveryDate,
      invoiceNumber,
      expiredDate,
    } = inputs

    const pickerSelectStyles = StyleSheet.create({
      inputIOS: {
        fontFamily: 'Kanit-Regular',
        fontSize: textSmall,
        height: 40,
        borderRadius: 0,
        paddingHorizontal: 10,
        borderColor: '#cecece',
        borderWidth: 0.5,
        color: colors.grayBlack,
        paddingRight: 30, // to ensure the text is never behind the icon
        backgroundColor: disabled ? colors.gray : 'transparent',
      },
      inputAndroid: {
        fontFamily: 'Kanit-Regular',
        fontSize: textSmall,
        height: 40,
        borderRadius: 0,
        paddingHorizontal: 10,
        borderColor: '#cecece',
        borderWidth: 0.5,
        color: colors.grayBlack,
        paddingRight: 30, // to ensure the text is never behind the icon
        backgroundColor: disabled ? colors.gray : 'transparent',
      },
    })

    return (
      <Container style={styles.container}>
        <Content scrollEnabled={false} style={styles.content}>
          <Row>
            {/* {Document Date} */}
            {documentDate === true && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`วันที่เอกสาร`}</Label>
                <TouchableOpacity
                  onPress={preventFunction.exec(
                    this.onDatePicker('DI_DATE', DI_DATE)
                  )}
                  style={styles.touchInput}
                >
                  <TextComponent style={styles.textPlaceholder}>
                    {DI_DATE ? dateThaiFormat(DI_DATE) : 'วันที่เอกสาร'}
                  </TextComponent>
                </TouchableOpacity>
              </Col>
            )}

            {/* {Document Number} */}
            {documentNumber && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`เลขที่เอกสาร`}</Label>
                <TextInput
                  editable={false}
                  style={disabled ? styles.textInputDisabled : styles.textInput}
                  bordered
                  placeholder={`<เลขถัดไป>`}
                />
              </Col>
            )}

            {/* dateOfTaxInvoice */}
            {dateOfTaxInvoice && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`วันที่ใบกำกับ`}</Label>
                <TouchableOpacity
                  onPress={preventFunction.exec(
                    this.onDatePicker(
                      'DATE_OF_TAX_INVOICE',
                      DATE_OF_TAX_INVOICE
                    )
                  )}
                  style={
                    disabled ? styles.touchInputDisabled : styles.touchInput
                  }
                >
                  <TextComponent style={styles.textPlaceholder}>
                    {DATE_OF_TAX_INVOICE
                      ? dateThaiFormat(DATE_OF_TAX_INVOICE)
                      : 'วันที่ใบกำกับ'}
                  </TextComponent>
                </TouchableOpacity>
              </Col>
            )}

            {/* taxInvoiceNumber */}
            {taxInvoiceNumber && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`เลขที่ใบกำกับภาษี`}</Label>
                <TextInput
                  style={disabled ? styles.textInputDisabled : styles.textInput}
                  bordered
                  editable={edit}
                  placeholder={`เลขที่ใบกำกับภาษี`}
                  value={TAX_INVOICE_NUMBER}
                />
              </Col>
            )}

            {/* agreement */}
            {agreement && (
              <Col
                style={styles.textView}
                smSize={100}
                mdSize={100}
                lgSize={50}
              >
                <Label style={styles.labelPrice}>{`ข้อตกลง`}</Label>
                <RNPickerSelect
                  disabled={disabled}
                  placeholder={{
                    label: 'เลือกข้อตกลง',
                    value: null,
                    color: '#9EA0A4',
                  }}
                  useNativeAndroidPickerStyle={false}
                  style={{
                    ...pickerSelectStyles,
                    iconContainer: {
                      top: 10,
                      right: 12,
                    },
                  }}
                  onValueChange={this.onChangeCreditorAgreement}
                  items={this.props.creditorAgreements}
                  value={AGREEMENT}
                />
              </Col>
            )}

            {/* dueDate */}
            {dueDate && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`ครบกำหนด`}</Label>
                <TouchableOpacity
                  onPress={preventFunction.exec(
                    this.onDatePicker('DUE_DATE', DUE_DATE)
                  )}
                  style={
                    disabled ? styles.touchInputDisabled : styles.touchInput
                  }
                >
                  <TextComponent style={styles.textPlaceholder}>
                    {DUE_DATE ? dateThaiFormat(DUE_DATE) : 'ครบกำหนด'}
                  </TextComponent>
                </TouchableOpacity>
              </Col>
            )}

            {/* deliveryDate */}
            {deliveryDate && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`วันที่ใบส่งของ`}</Label>
                <TouchableOpacity
                  onPress={preventFunction.exec(
                    this.onDatePicker('DELIVERY_DATE', DELIVERY_DATE)
                  )}
                  style={
                    disabled ? styles.touchInputDisabled : styles.touchInput
                  }
                >
                  <TextComponent style={styles.textPlaceholder}>
                    {DELIVERY_DATE
                      ? dateThaiFormat(DELIVERY_DATE)
                      : 'วันที่ใบส่งของ'}
                  </TextComponent>
                </TouchableOpacity>
              </Col>
            )}

            {/* invoiceNumber */}
            {invoiceNumber && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`เลขที่ใบส่งของ`}</Label>
                <TextInput
                  style={disabled ? styles.textInputDisabled : styles.textInput}
                  bordered
                  editable={edit}
                  placeholder={`เลขที่ใบส่งของ`}
                  value={INVOICE_NUMBER}
                />
              </Col>
            )}

            {/* expiredDate */}
            {expiredDate && (
              <Col style={styles.textView} smSize={50} mdSize={50} lgSize={25}>
                <Label style={styles.labelPrice}>{`วันหมดอายุ`}</Label>
                <TouchableOpacity
                  onPress={preventFunction.exec(
                    this.onDatePicker('EXPIRED_DATE', EXPIRED_DATE)
                  )}
                  style={
                    disabled ? styles.touchInputDisabled : styles.touchInput
                  }
                >
                  <TextComponent style={styles.textPlaceholder}>
                    {EXPIRED_DATE ? dateThaiFormat(EXPIRED_DATE) : 'วันหมดอายุ'}
                  </TextComponent>
                </TouchableOpacity>
              </Col>
            )}
          </Row>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  content: {
    flex: 1,
  },
  textView: {
    paddingHorizontal: 5,
    paddingBottom: 5,
  },
  labelPrice: {
    fontFamily: 'Kanit-Regular',
    fontSize: textMedium,
    color: colors.primary,
  },
  textPlaceholder: {
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    color: colors.grayBlack,
  },
  textInput: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    color: colors.grayBlack,
    width: '100%',
    height: 40,
    backgroundColor: colors.white,
    // paddingTop: 10,
    borderRadius: 0,
    paddingHorizontal: 10,
    borderColor: '#cecece',
    borderWidth: 0.5,
  },
  textInputDisabled: {
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    color: colors.grayBlack,
    width: '100%',
    height: 40,
    // paddingTop: 10,
    borderRadius: 0,
    paddingHorizontal: 10,
    borderColor: '#cecece',
    borderWidth: 0.5,
    backgroundColor: colors.gray,
  },
  touchInputDisabled: {
    color: colors.grayBlack,
    width: '100%',
    height: 40,
    backgroundColor: colors.gray,
    borderRadius: 0,
    paddingHorizontal: 10,
    borderColor: '#cecece',
    borderWidth: 0.5,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  touchInput: {
    color: colors.grayBlack,
    width: '100%',
    height: 40,
    backgroundColor: colors.white,
    borderRadius: 0,
    paddingHorizontal: 10,
    borderColor: '#cecece',
    borderWidth: 0.5,
    alignSelf: 'center',
    justifyContent: 'center',
  },
})

function mapStateToProps({ document, creditorAgreement }) {
  const { chooseDocument, prepareDocument } = document
  const { creditorAgreements } = creditorAgreement
  return {
    chooseDocument,
    prepareDocument,
    creditorAgreements,
  }
}

export default connect(
  mapStateToProps,
  {
    setPrepareDocument,
    getCreditorAgreements,
  }
)(withLanguage(GroupDetail))
