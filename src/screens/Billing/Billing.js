import React, { PureComponent } from 'react'
import {
  SafeAreaView,
  View,
  TouchableOpacity,
  Keyboard,
  Dimensions,
  PanResponder,
  Animated,
  Alert,
} from 'react-native'
import { Footer, Icon, Tab, Tabs, Button, TabHeading } from 'native-base'
import { TextComponent, HeaderComponent, BottomButton } from '../../components'
import { pop } from '../../helpers/navigations'
import styles from './styles'
import BillingList from './BillingList'
import GroupDetail from './GroupDetail'
import BillingSummaryDetail from './BillingSummaryDetail'
import { MANAGE_BILLING_SCREEN } from '../../navigations/Screens'
import CustomerInput from './CustomerInput'
import { preventFunction, customWithLanguage } from '../../helpers'

import { connect } from 'react-redux'
import { i18n } from 'react-native-i18n-localize'
import { removePrepareDocument } from '../../redux/actions'
import { colors } from '../../constants/styles'

import { Navigation } from 'react-native-navigation'

class Billing extends PureComponent {
  constructor(props) {
    super(props)
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    )
    this.state = {
      openKeyboard: false,
      disabled: true,
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener.remove()
  }

  componentWillReceiveProps(nextProps) {
    //unlock disabled all when choose AP or AR
    const { AP_CODE, AR_CODE } = nextProps.prepareDocument
    console.log('nextProps.prepareDocument: ', nextProps.prepareDocument)
    if (AP_CODE || AR_CODE) {
      console.log('disabled: false')
      this.setState({ disabled: false })
    }
  }

  _keyboardDidShow = () => {
    this.setState({ openKeyboard: true })
  }

  _keyboardDidHide = () => {
    this.setState({ openKeyboard: false })
  }

  _backNavigation = () => {
    Alert.alert(
      'Alert Title',
      'ต้องการบันทึกฉบับร่างหรือไม่?',
      [
        {
          text: 'Cancel',
          onPress: () => {
            this.props.removePrepareDocument()
            pop(this.props.componentId)
          },
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            this.props.removePrepareDocument()
            pop(this.props.componentId)
          },
        },
      ],
      { cancelable: false }
    )
  }

  _nextNavigation = () => {
    Navigation.popToRoot(this.props.componentId)
  }

  _onManageScreen = mode => {
    let title = `เพิ่มรายการ`
    const object = {
      componentName: MANAGE_BILLING_SCREEN,
      passProps: {
        title: title,
        mode: mode,
      },
    }
    Navigation.push(this.props.componentId, {
      component: {
        name: object.componentName,
        passProps: object.passProps,
      },
    })
  }

  render() {
    const { openKeyboard, disabled } = this.state
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
          }}
        >
          <HeaderComponent
            onPressedBack={this._backNavigation}
            haveBack={true}
            title={`${this.props.chooseDocument.title}`}
            style={styles.topBar}
          />

          {/* Body */}
          <View style={styles.customerPanel}>
            <CustomerInput componentId={this.props.componentId} />
          </View>
          <Tabs
            initialPage={0}
            locked
            tabBarUnderlineStyle={{ backgroundColor: colors.white }}
            scrollWithoutAnimation={false}
          >
            <Tab
              activeTabStyle={styles.activeTabStyle}
              heading={
                <TabHeading style={styles.tabStyle}>
                  <TextComponent
                    style={styles.textStyle}
                  >{`ส่วนหัว`}</TextComponent>
                </TabHeading>
              }
            >
              <View style={styles.groupDetailPanel}>
                <GroupDetail disabled={disabled} />
              </View>
            </Tab>
            <Tab
              activeTabStyle={styles.activeTabStyle}
              heading={
                <TabHeading style={styles.tabStyle}>
                  <TextComponent style={styles.textStyle}>{`${i18n.t(
                    `description`
                  )}`}</TextComponent>
                </TabHeading>
              }
            >
              <View style={{ flex: 1, paddingTop: 10 }}>
                <TouchableOpacity
                  disabled={disabled}
                  onPress={this._onManageScreen}
                >
                  <Button
                    disabled={disabled}
                    primary
                    rounded
                    style={
                      disabled ? styles.buttonAddDisabled : styles.buttonAdd
                    }
                    onPress={this._onManageScreen}
                  >
                    <Icon type="FontAwesome5" name="plus-circle" />
                  </Button>
                </TouchableOpacity>
                <BillingList componentId={this.props.componentId} />
              </View>
            </Tab>
            <Tab
              activeTabStyle={styles.activeTabStyle}
              heading={
                <TabHeading style={styles.tabStyle}>
                  <TextComponent style={styles.textStyle}>{`${i18n.t(
                    `totalNet`
                  )}`}</TextComponent>
                </TabHeading>
              }
            >
              <View style={{ flex: 1, paddingTop: 10 }}>
                <BillingSummaryDetail />
              </View>
            </Tab>
          </Tabs>
          {/* Body */}
        </View>
        <Footer
          style={[styles.footer, { display: openKeyboard ? 'none' : 'flex' }]}
        >
          <BottomButton
            okText={`${i18n.t(`save`)}`}
            cancelText={`${i18n.t(`cancel`)}`}
            backNavigation={preventFunction.exec(this._backNavigation)}
            nextNavigation={preventFunction.exec(this._nextNavigation)}
          />
        </Footer>
      </SafeAreaView>
    )
  }
}

function mapStateToProps({ document }) {
  const { chooseDocument, prepareDocument } = document
  return {
    chooseDocument,
    prepareDocument,
  }
}

export default connect(
  mapStateToProps,
  {
    removePrepareDocument,
  }
)(customWithLanguage(Billing))
