import { StyleSheet } from 'react-native'

export default (styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'flex-start',
    // alignItems: 'center',
  },
  panel: {
    marginTop: 20,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputPanel: {
    width: '100%',
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonPanel: {
    width: '100%',
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
}))
