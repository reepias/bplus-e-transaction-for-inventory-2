import React, { PureComponent } from 'react'
import {
  TextComponent,
  HeaderComponent,
  InputComponent,
  ButtonComponent,
} from '../../components'
import { View } from 'react-native'
import styles from './styles'
import { ContainerComponent } from '../../containers'
import { pop } from '../../helpers/navigations'
import { i18n } from 'react-native-i18n-localize'
import validate from '../../rules/SignUpFormValidationRules'
import isEmpty from '../../helpers/isEmpty'
import { customWithLanguage, preventFunction } from '../../helpers'
import { connect } from 'react-redux'
import { registerUserAction } from '../../redux/actions'

class SignUp extends PureComponent {
  constructor(props) {
    super(props)
  }
  onPressedSignUp = () => {
    pop(this.props.componentId)
  }
  render() {
    return (
      <ContainerComponent style={styles.container}>
        <HeaderComponent
          onPressedBack={() => pop(this.props.componentId)}
          haveBack={true}
          title={`ลงทะเบียน`}
        />
        <View style={styles.panel}>
          {/* Phone input with native-base */}
          <Item rounded style={styles.itemStyle}>
            <Icon active name="call" style={styles.iconStyle} />
            <Image
              source={require('../../assets/flags/th.png')}
              style={[styles.flag]}
              onPress={this.onPressFlag}
            />
            {/* <TextComponent style={styles.flagText}>+66</TextComponent> */}
            <Icon
              active
              name="md-arrow-dropdown"
              style={[styles.iconStyle, { marginLeft: 0 }]}
            />
            <InputComponent
              placeholder={`${i18n.t(`phoneNumber.text`)}`}
              keyboardType={'phone-pad'}
              returnKeyType="done"
              autoCapitalize="none"
              autoCorrect={false}
              secureTextEntry={false}
              style={styles.inputStyle}
              onChangeText={value => this.setState({ telephone: value })}
              maxLength={10}
            />
          </Item>
          <View style={styles.buttonPanel}>
            <ButtonComponent
              onPressed={preventFunction.exec(this.onPressedSignUp)}
              text={`${i18n.t(`signUp`)}`}
            />
          </View>
        </View>
      </ContainerComponent>
    )
  }
}

export default connect(
  null,
  {
    registerUserAction,
  }
)(customWithLanguage(SignUp))
