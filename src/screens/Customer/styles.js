import { StyleSheet } from 'react-native'
import { heightWindow } from '../../constants/resolutions'
import {
  colors,
  startGradient,
  middleGradient,
  greenStartGardient,
  Kanit,
} from '../../constants/styles'

import { textSmall, textMedium, textBig } from '../../constants/fontSize'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  containerNoContent: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },
  topBar: {
    backgroundColor: startGradient,
  },
  content: {
    flex: 1,
    backgroundColor: 'transparent',
    padding: 10,
  },
  title: {
    fontFamily: Kanit,
    fontSize: textMedium,
    color: colors.grayBlack,
  },
  body: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  boxInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderColor: colors.gray,
    paddingBottom: 10,
  },
  text: {
    fontSize: textMedium,
    fontFamily: Kanit,
    color: colors.grayBlack,
  },
  textCode: {
    fontSize: textMedium,
    fontFamily: Kanit,
    color: colors.darkGray,
  },
  textPrimary: {
    fontSize: textMedium,
    fontFamily: Kanit,
    color: middleGradient,
  },
  buttonIcon: {
    fontSize: textBig,
    color: middleGradient,
  },
  notFound: {
    fontSize: textMedium,
    fontFamily: Kanit,
    color: colors.grayBlack,
    textAlign: 'center',
  },
})
