import React, { PureComponent } from 'react'
import { SafeAreaView, View, TouchableOpacity, FlatList } from 'react-native'
import { Icon, Body, Content, Item, Input, ListItem, Right } from 'native-base'
import { TextComponent, HeaderComponent, Loader } from '../../components'
import { pop } from '../../helpers/navigations'
import styles from './styles'
import { connect } from 'react-redux'
import {
  getCustomers,
  receivePrepareDocument,
  setPrepareDocument,
} from '../../redux/actions/index'
import { i18n, withLanguage } from 'react-native-i18n-localize'
import { colors } from 'react-native-elements'
import moment from 'moment'
import { getPrepareDocument } from '../../helpers'

class Customer extends PureComponent {
  constructor(props) {
    super(props)
    const customer = this.props.chooseDocument.customer
    this.state = {
      searchTerm: this.props.searchTerm || '',
      oldSearchTerm: '',
      title:
        customer === 'AP' ? 'เจ้าหนี้' : customer === 'AR' ? 'ลูกหนี้' : '-',
    }
  }

  componentDidMount() {
    this._getCustomers(
      this.state.searchTerm,
      this.props.chooseDocument.customer
    )
  }

  _getCustomers = () => {
    const searchTerm = ''
    const oldSearchTerm = ''
    this.setState({ searchTerm, oldSearchTerm })
    this.props.getCustomers(searchTerm, this.props.chooseDocument.customer)
  }

  _filterCustomer = () => {
    const { searchTerm } = this.state
    this.setState({ oldSearchTerm: searchTerm })
    this.props.getCustomers(searchTerm, this.props.chooseDocument.customer)
  }

  _keyExtractor = (item, index) => `${index}`

  _onPress = () => {
    alert('comming soon')
  }

  chooseCustomer = customer => {
    let prepareDocumentState = this.props.prepareDocument

    const { prepareDocument } = getPrepareDocument(prepareDocumentState)
    let object = {
      ...prepareDocument,
      ...customer,
    }

    console.log('chooseCustomer: ', object)

    this.props.setPrepareDocument(object)
    pop(this.props.componentId)
    if (this.props.notifyChange) {
      this.props.notifyChange(
        customer.AP_CODE ? customer.AP_CODE : customer.AR_CODE
      )
    }
  }

  _renderItem = item => {
    let name = ''
    let code = ''
    if (this.props.chooseDocument.customer === 'AP') {
      name = item.item.AP_NAME
      code = item.item.AP_CODE
    } else if (this.props.chooseDocument.customer === 'AR') {
      name = item.item.AR_NAME
      code = item.item.AR_CODE
    }
    return (
      <ListItem>
        <Body>
          <TouchableOpacity onPress={() => this.chooseCustomer(item.item)}>
            <TextComponent style={styles.text}>{name}</TextComponent>
            <TextComponent style={styles.textCode} note numberOfLines={1}>
              {code}
            </TextComponent>
          </TouchableOpacity>
        </Body>
        <Right>
          <TouchableOpacity transparent onPress={this._onPress}>
            <TextComponent style={styles.textPrimary}>View</TextComponent>
          </TouchableOpacity>
        </Right>
      </ListItem>
    )
  }

  render() {
    const { customers, isFetching } = this.props
    const { searchTerm, oldSearchTerm } = this.state
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <HeaderComponent
            onPressedBack={() => pop(this.props.componentId)}
            haveBack={true}
            title={`ค้นหา${this.state.title}`}
            style={styles.topBar}
          />
          <Content style={styles.content}>
            <View style={styles.boxInput}>
              <Item rounded style={{ width: '80%' }}>
                <Icon
                  name="search"
                  style={{ fontSize: 20, color: colors.darkGray }}
                />
                <Input
                  style={styles.text}
                  placeholder={`รหัส ชื่อบริษัท`}
                  placeholderTextColor={colors.darkGray}
                  onChangeText={value => this.setState({ searchTerm: value })}
                  value={searchTerm}
                />
              </Item>
              <TouchableOpacity onPress={this._filterCustomer}>
                <Icon name="search" style={styles.buttonIcon} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this._getCustomers}>
                <Icon name="refresh" style={styles.buttonIcon} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              {customers.length > 0 ? (
                <FlatList
                  data={customers}
                  keyExtractor={this._keyExtractor}
                  renderItem={this._renderItem}
                  numColumns={1}
                />
              ) : isFetching === true ? (
                <View style={styles.container}>
                  <Loader loading={true} />
                </View>
              ) : (
                <View style={styles.containerNoContent}>
                  <TextComponent style={styles.notFound}>
                    {oldSearchTerm
                      ? `${i18n.t('yourSearch')} - ${oldSearchTerm} - ${i18n.t(
                          'doesNotMatch'
                        )} `
                      : `${i18n.t('noData')}`}
                  </TextComponent>
                </View>
              )}
            </View>
          </Content>
        </View>
      </SafeAreaView>
    )
  }
}

function mapStateToProps({ customer, document }) {
  const { customers, isFetching } = customer
  const { chooseDocument, prepareDocument } = document
  return {
    customers,
    isFetching,
    chooseDocument,
    prepareDocument,
  }
}

export default connect(
  mapStateToProps,
  {
    getCustomers,
    receivePrepareDocument,
    setPrepareDocument,
  }
)(withLanguage(Customer))
