import React, { PureComponent, Fragment } from 'react'
import { SafeAreaView, TextInput, TouchableOpacity } from 'react-native'
import { Content, Button, Icon, View, Label, Footer, Input } from 'native-base'
import { TextComponent, HeaderComponent } from '../../components'
import { i18n, withLanguage } from 'react-native-i18n-localize'

import { pop, push } from '../../helpers/navigations'

import Style from '../../styles/Theme/Style'
import Styles from './Style'

import { connect } from 'react-redux'

import DateTimePicker from 'react-native-modal-datetime-picker'

import moment from 'moment'

import {
  BUY_BELIEVE_TABLE,
  SELL_BELIEVE_TABLE,
  TRANSFER_TABLE,
} from '../../constants/config'

import { getDocument } from '../../redux/actions'

class DocumentSearch extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      diRef: '',
      name: '',
      isDateStartVisible: false,
      dateStart: '',
      dateEnd: '',
    }
  }

  _getDocument = async () => {
    const { sqlTableKey } = this.props.chooseDocument
    let { diRef, name, dateStart, dateEnd } = this.state
    console.log('sqlTableKey: ', sqlTableKey)
    let sqlFilter = ''
    if (diRef) {
      sqlFilter = `AND (DI_REF LIKE '%${diRef}%')`
    }

    if (name) {
      let fieldName = ''
      var nameString = name.replace(/\s/g, '%')
      if (sqlTableKey === BUY_BELIEVE_TABLE) {
        fieldName = 'AP_NAME'
      } else if (sqlTableKey === SELL_BELIEVE_TABLE) {
        fieldName = 'AR_NAME'
      }
      sqlFilter += `AND (${fieldName} LIKE '%${nameString}%')`
    }

    if (dateStart && dateEnd) {
      // mm/dd/year 12/23/2018
      //AND (DI_DATE BETWEEN '01/01/2018' AND '01/12/2018')
      dateStart = moment(dateStart).format('MM/DD/YYYY')
      dateEnd = moment(dateEnd).format('MM/DD/YYYY')
      let fieldName = 'DI_DATE'
      sqlFilter += `AND (${fieldName} BETWEEN '${dateStart}' AND '${dateEnd}')`
    }

    console.log('sqlFilter: ', sqlFilter)

    const result = await this.props.getDocument(sqlTableKey, sqlFilter)
    console.log('result: ', result)
    setTimeout(() => {
      pop(this.props.componentId)
    }, 1000)
  }

  showDateStartPicker = () => {
    this.setState({ isDateStartVisible: true })
  }

  hideDateStartPicker = () => {
    this.setState({ isDateStartVisible: false })
  }

  handleDateStartPicked = date => {
    const dateEnd = date
    this.setState({ dateStart: date, dateEnd })
    this.hideDateStartPicker()
  }

  showDateEndPicker = () => {
    this.setState({ isDateEndVisible: true })
  }

  hideDateEndPicker = () => {
    this.setState({ isDateEndVisible: false })
  }

  handleDateEndPicked = date => {
    const dateEnd = date
    this.setState({ dateEnd })
    this.hideDateEndPicker()
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={styles.container}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}
          >
            <HeaderComponent
              onPressedBack={() => pop(this.props.componentId)}
              onPressedSearch={this.showModal}
              haveBack={true}
              title={`ค้นหาเอกสาร - ${this.props.chooseDocument.title}`}
              style={Style.topBar}
            />
            <Content
              style={Style.layoutInner}
              contentContainerStyle={Style.listView}
            >
              <View style={Styles.section}>
                <View style={Styles.textView}>
                  <Label style={Styles.labelPrice}>
                    <TextComponent style={Styles.label}>เลขที่</TextComponent>
                  </Label>
                  <TextInput
                    style={Styles.textInput}
                    bordered
                    placeholder="เลขที่"
                    onChangeText={val => this.setState({ diRef: val })}
                  />
                </View>
                <View style={Styles.textView}>
                  <Label style={Styles.labelPrice}>
                    <TextComponent style={Styles.label}>ชื่อ</TextComponent>
                  </Label>
                  <TextInput
                    style={Styles.textInput}
                    bordered
                    placeholder="ชื่อ"
                    onChangeText={val => this.setState({ name: val })}
                  />
                </View>

                <View style={Styles.price}>
                  <Label style={Styles.labelPrice}>วันที่</Label>
                  <View style={Styles.col}>
                    <TouchableOpacity
                      onPress={this.showDateStartPicker}
                      style={Styles.datePicker}
                    >
                      <TextComponent
                        style={
                          this.state.dateStart
                            ? Styles.textInput
                            : Styles.dateInput
                        }
                      >
                        {this.state.dateStart
                          ? moment(this.state.dateStart).format('LL')
                          : `วันที่`}
                      </TextComponent>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={this.showDateEndPicker}
                      style={Styles.datePicker}
                    >
                      <TextComponent
                        style={
                          this.state.dateEnd
                            ? Styles.textInput
                            : Styles.dateInput
                        }
                      >
                        {this.state.dateEnd
                          ? moment(this.state.dateEnd).format('LL')
                          : `ถึงวันที่`}
                      </TextComponent>
                    </TouchableOpacity>
                  </View>
                </View>
                <DateTimePicker
                  isVisible={this.state.isDateStartVisible}
                  onConfirm={this.handleDateStartPicked}
                  onCancel={this.hideDateStartPicker}
                />
                <DateTimePicker
                  isVisible={this.state.isDateEndVisible}
                  onConfirm={this.handleDateEndPicked}
                  onCancel={this.hideDateEndPicker}
                  minimumDate={this.state.dateStart}
                />
              </View>
            </Content>
          </View>
        </SafeAreaView>
        <Footer style={Styles.footer}>
          <Button style={Styles.btn} onPress={this._getDocument}>
            <TextComponent style={Styles.btnText}>
              {'ค้นหา'.toUpperCase()}
            </TextComponent>
            <Icon
              active
              name="search"
              type="FontAwesome"
              style={Styles.btnIcon}
            />
          </Button>
        </Footer>
      </Fragment>
    )
  }
}

function mapStateToProps({ document }) {
  const { chooseDocument } = document
  return {
    chooseDocument,
  }
}

export default connect(
  mapStateToProps,
  {
    getDocument,
  }
)(withLanguage(DocumentSearch))
