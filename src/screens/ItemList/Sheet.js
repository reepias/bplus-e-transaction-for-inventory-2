import React, { PureComponent } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { TextComponent } from '../../components'

import { customWithLanguage } from '../../helpers'

import { i18n } from 'react-native-i18n-localize'
import Styles from '../../styles/ItemList/Style'

class Sheet extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      onScreen: true,
      items: this.props.data ? this.props.data.slice(0, 1) : null,
    }
    this.recursiveFunction = null
  }

  componentDidMount() {
    this.recursive()
  }

  componentWillUnmount() {
    console.log('componentWillUnmoun :', this.recursiveFunction)
    this.setState({ onScreen: false })
    clearTimeout(this.recursiveFunction)
  }

  recursive = () => {
    if (this.state.onScreen === true && this.state.items) {
      this.recursiveFunction = setTimeout(() => {
        let hasMore = this.state.items.length + 1 < this.props.data.length
        this.setState((prev, props) => ({
          items: props.data.slice(0, prev.items.length + 1),
        }))
        if (hasMore) this.recursive()
      }, 0)
    }
  }

  _listEmptyComponent = () => {
    return (
      <View style={Styles.layoutNoContent}>
        <TextComponent style={Styles.itemPrice}>{`${i18n.t(
          'productNotFound'
        )}`}</TextComponent>
      </View>
    )
  }

  _keyExtractor = (item, index) => `${index}`

  render() {
    const { items } = this.state
    return (
      <View style={Styles.section}>
        <FlatList
          data={items}
          showsHorizontalScrollIndicator={false}
          keyExtractor={this._keyExtractor}
          ListEmptyComponent={this._listEmptyComponent}
          renderItem={({ item, separators }) => (
            <TouchableOpacity
              style={Styles.item}
              underlayColor="transparent"
              onPress={() => this.props.onPress(item)}
            >
              <View style={Styles.itemLeft}>
                <TextComponent style={Styles.textTitle}>
                  {`${i18n.t('tradingCode')}`}
                </TextComponent>
                <TextComponent style={Styles.textDes}>
                  {item.SKU_CODE}
                </TextComponent>
                <TextComponent style={Styles.textTitle}>
                  {`${i18n.t('productName')}`}
                </TextComponent>
                <TextComponent style={Styles.textDes}>
                  {item.SKU_NAME}
                </TextComponent>
              </View>
              <View style={Styles.itemRight}>
                <View style={Styles.rowBetween}>
                  <TextComponent style={Styles.textTitle}>
                    {`${i18n.t('purchasePrice')}`}
                  </TextComponent>
                  <TextComponent style={Styles.textDes}>
                    ยังไม่ได้ข้อมูล
                  </TextComponent>
                </View>
                <View style={Styles.rowBetween}>
                  <TextComponent style={Styles.textTitle}>
                    {`${i18n.t('brand')}`}
                  </TextComponent>
                  <TextComponent style={Styles.textDes}>
                    {item.BRN_NAME}
                  </TextComponent>
                </View>
                <View style={Styles.rowBetween}>
                  <TextComponent style={Styles.textTitle}>
                    {`${i18n.t('category')}`}
                  </TextComponent>
                  <TextComponent style={Styles.textDes}>
                    {item.ICCAT_NAME}
                  </TextComponent>
                </View>
                <View style={Styles.rowBetween}>
                  <TextComponent style={Styles.textTitle}>
                    {`${i18n.t('unitOfMeasure')}`}
                  </TextComponent>
                  <TextComponent style={Styles.textDes}>
                    {item.UTQ_NAME}
                  </TextComponent>
                </View>
                <View style={Styles.rowBetween}>
                  <TextComponent style={Styles.textTitle}>
                    {`${i18n.t('productCode')}`}
                  </TextComponent>
                  <TextComponent style={Styles.textDes}>
                    {item.SKU_CODE}
                  </TextComponent>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    )
  }
}

export default customWithLanguage(Sheet)
