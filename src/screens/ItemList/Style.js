import { StyleSheet } from 'react-native'
import { heightWindow } from '../../constants/resolutions'
import {
  colors,
  startGradient,
  middleGradient,
  greenStartGardient,
  Kanit,
} from '../../constants/styles'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  layoutNoContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('60%'),
  },

  topBar: {
    backgroundColor: startGradient,
  },
  content: {
    flex: 1,
    backgroundColor: 'transparent',
    padding: 10,
  },
  title: {
    fontFamily: Kanit,
    fontSize: 18,
    color: colors.grayBlack,
  },
  body: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  boxInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderColor: colors.gray,
    paddingBottom: 10,
  },
  text: {
    fontFamily: Kanit,
    fontSize: 14,
  },
  buttonIcon: {
    fontSize: 30,
    color: middleGradient,
  },
  itemPrice: {
    color: '#333',
    fontSize: 14,
    fontFamily: 'Kanit-SemiBold',
    paddingHorizontal: 3,
  },
  textItem: {
    fontFamily: Kanit,
    fontSize: 14,
    color: '#333',
  },
})
