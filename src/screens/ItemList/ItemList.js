import React, { PureComponent } from 'react'
import { SafeAreaView, View, TouchableOpacity } from 'react-native'
import { Icon, Content, Item, Input } from 'native-base'
import { TextComponent, HeaderComponent, Loader } from '../../components'
import { pop } from '../../helpers/navigations'

import { connect } from 'react-redux'

import { getAllProduct } from '../../redux/actions'

import { customWithLanguage } from '../../helpers'

import { i18n } from 'react-native-i18n-localize'

import Styles from '../../styles/ItemList/Style'

import Sheet from './Sheet'

import { colors } from '../../constants/styles'

class ItemList extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm: this.props.searchTerm || '',
      oldSearchTerm: '',
    }
  }

  componentDidMount() {
    if (!this.props.searchTerm) {
      this._getAllProduct()
    }
  }

  _getAllProduct = () => {
    const searchTerm = ''
    this.setState({ searchTerm, oldSearchTerm: '' })
    this.props.getAllProduct(searchTerm)
  }

  _filterProduct = () => {
    this.props.getAllProduct(this.state.searchTerm)
  }

  onPressed = item => {
    this.props.onSelectItem(item)
  }

  render() {
    const { isFetching, products } = this.props
    return (
      <SafeAreaView style={Styles.container}>
        <Loader loading={isFetching} />
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <HeaderComponent
            onPressedBack={() => pop(this.props.componentId)}
            haveBack={true}
            // haveChangeLang={true}
            title={`${i18n.t(`searchForItems`)}`}
            style={Styles.topBar}
          />
          <Content>
            <View style={Styles.boxInput}>
              <Item rounded style={{ width: '80%' }}>
                <Icon
                  name="search"
                  style={{ fontSize: 20, color: colors.darkGray }}
                />
                <Input
                  returnKeyType="search"
                  style={Styles.text}
                  onSubmitEditing={this._filterProduct}
                  placeholder={`${i18n.t('tradingCode')}, ${i18n.t(
                    'productCode'
                  )}, ${i18n.t('productName')}`}
                  placeholderTextColor={colors.darkGray}
                  onChangeText={value => this.setState({ searchTerm: value })}
                  value={this.state.searchTerm}
                />
              </Item>
              <TouchableOpacity onPress={this._filterProduct}>
                <Icon name="search" style={Styles.buttonIcon} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this._getAllProduct}>
                <Icon name="refresh" style={Styles.buttonIcon} />
              </TouchableOpacity>
            </View>
            <View style={Styles.itemsPanel}>
              <TextComponent style={Styles.textDes}>
                {`${this.props.products.length} ${i18n.t('items')}`}
              </TextComponent>
            </View>
            {products && !this.props.isFetching && (
              <Sheet
                data={products}
                componentId={this.props.componentId}
                onPress={this.onPressed}
              />
            )}
          </Content>
        </View>
      </SafeAreaView>
    )
  }
}

function mapStateToProps({ product }) {
  const { products, isFetching } = product
  return {
    products,
    isFetching,
  }
}

export default connect(
  mapStateToProps,
  {
    getAllProduct,
  }
)(customWithLanguage(ItemList))
