import React, { PureComponent } from 'react'
import { TextComponent, HeaderComponent } from '../../components'
import { View, TouchableOpacity } from 'react-native'
import styles from './styles'
import { ContainerComponent } from '../../containers'
import { pop } from '../../helpers/navigations'
import { PlusCircleWhite } from '../../constants/Icons'
import { Badge, Button } from 'react-native-elements'
import ServiceList from './ServiceList'

class Service extends PureComponent {
  constructor(props) {
    super(props)
  }
  onPressedService = () => {
    pop(this.props.componentId)
  }
  render() {
    return (
      <ContainerComponent disabledScrollView={true} style={styles.container}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}
        >
          <HeaderComponent
            onPressedBack={() => pop(this.props.componentId)}
            haveBack={true}
            title={`เลือก Service`}
            style={styles.header}
          />
          <View style={styles.content}>
            <View style={styles.main}>
              <View>
                <TextComponent
                  style={styles.textPrimary}
                >{`Name`}</TextComponent>
              </View>
              <View style={styles.row}>
                <TextComponent
                  style={styles.textLink}
                  numberOfLines={1}
                >{`https://facebook.github.io/react-native`}</TextComponent>
                <View style={styles.statusPanel}>
                  <Badge status="success" />
                  <TextComponent
                    style={styles.textStatusSuccess}
                  >{`เชื่อมต่อสำเร็จ`}</TextComponent>
                </View>
              </View>
              <View style={styles.buttonPanel}>
                <Button
                  buttonStyle={[styles.button, styles.buttonConnect]}
                  titleStyle={styles.textButton}
                  title={`เชื่อมต่อ`}
                />
                <Button
                  buttonStyle={[styles.button, styles.buttonEdit]}
                  titleStyle={styles.textButton}
                  title={`แก้ไข`}
                />
                <Button
                  buttonStyle={[styles.button, styles.buttonDelete]}
                  titleStyle={styles.textButton}
                  title={`ลบ`}
                />
              </View>
            </View>
            <View>
              <ServiceList />
            </View>
          </View>
          <View style={styles.bottomPanel}>
            <TouchableOpacity>
              <PlusCircleWhite style={styles.icon} />
            </TouchableOpacity>
          </View>
        </View>
      </ContainerComponent>
    )
  }
}

export default Service
