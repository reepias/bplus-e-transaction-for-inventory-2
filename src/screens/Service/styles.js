import { StyleSheet } from 'react-native'
import { heightWindow } from '../../constants/resolutions'
import { colors } from '../../constants/styles'

export default (styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  bottomPanel: {
    flex: 0.1,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'flex-end',
    padding: 20,
  },
  icon: {
    width: 50,
    height: 50,
  },
  main: {
    padding: 15,
    paddingHorizontal: 25,
    height: 125,
    backgroundColor: colors.gray,
    borderBottomWidth: 1,
    borderColor: '#e5e5e5',
    justifyContent: 'space-between',
  },
  list: {
    padding: 15,
    paddingHorizontal: 25,
    height: 80,
    backgroundColor: colors.gray,
    borderBottomWidth: 1,
    borderColor: '#e5e5e5',
    justifyContent: 'space-between',
  },
  textPrimary: {
    color: colors.primary,
  },
  textLink: {
    color: '#a3a3a3',
  },
  textStatusSuccess: {
    paddingLeft: 10,
    color: '#28a368',
  },
  statusPanel: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonPanel: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  textButton: {
    color: colors.white,
    fontFamily: 'Kanit-Regular',
    fontSize: 14,
    alignSelf: 'center',
  },
  textChoose: {
    color: colors.primary,
    fontFamily: 'Kanit-Regular',
    fontSize: 14,
  },
  button: {
    width: 80,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonConnect: {
    backgroundColor: colors.primary,
  },
  buttonEdit: {
    backgroundColor: colors.orange,
  },
  buttonDelete: {
    backgroundColor: colors.red,
  },
  buttonChoose: {
    backgroundColor: colors.white,
  },
}))
