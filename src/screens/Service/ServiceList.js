import React, { PureComponent } from 'react'
import { View, FlatList } from 'react-native'
import { data } from './data'
import styles from './styles'
import { TextComponent } from '../../components'
import { Button } from 'react-native-elements'

const numColumns = 1

class ServiceList extends PureComponent {
  renderItem = ({ item, index }) => {
    if (item.empty === true) {
      return (
        <View style={[styles.item, styles.itemInvisible]}>
          {' '}
          <CustomText style={styles.itemName}>ไม่พบข้อมูล</CustomText>
        </View>
      )
    }
    return (
      <View style={styles.list}>
        <View>
          <TextComponent style={styles.textPrimary}>{`Name`}</TextComponent>
        </View>
        <View style={styles.row}>
          <TextComponent
            style={styles.textLink}
            numberOfLines={1}
          >{`https://facebook.github.io/react-native`}</TextComponent>
          <Button
            containerStyle={{ borderRadius: 10 }}
            buttonStyle={[styles.button, styles.buttonChoose]}
            titleStyle={styles.textChoose}
            title={`เลือก`}
          />
        </View>
      </View>
    )
  }

  _keyExtractor = (item, index) => `${item.id}-${index}`

  render() {
    return (
      <FlatList
        data={data}
        renderItem={this.renderItem}
        keyExtractor={this._keyExtractor}
        numColumns={numColumns}
      />
    )
  }
}

export default ServiceList
