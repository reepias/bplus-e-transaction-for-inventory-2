import { StyleSheet, Platform } from 'react-native'
import { heightWindow } from '../../constants/resolutions'
import {
  colors,
  startGradient,
  middleGradient,
  greenStartGardient,
  Kanit,
} from '../../constants/styles'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  layoutNoContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('60%'),
  },

  topBar: {
    backgroundColor: startGradient,
  },
  content: {
    flex: 1,
    backgroundColor: 'transparent',
    padding: 10,
  },
  title: {
    fontFamily: Kanit,
    fontSize: 18,
    color: colors.grayBlack,
  },
  body: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  boxInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderColor: colors.gray,
    paddingBottom: 10,
  },
  text: {
    fontFamily: Kanit,
    fontSize: 14,
  },
  buttonIcon: {
    fontSize: 30,
    color: middleGradient,
  },
  itemPrice: {
    color: '#333',
    fontSize: 14,
    fontFamily: 'Kanit-SemiBold',
    paddingHorizontal: 3,
  },
  textItem: {
    fontFamily: Kanit,
    fontSize: 14,
    color: '#333',
  },

  section: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 10,
    backgroundColor: colors.white,
  },
  itemList: {
    flexDirection: 'row',
    width: '100%',
    marginBottom: 10,
  },
  itemBg: {
    ...Platform.select({
      ios: {},
    }),
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  item: {
    flex: 1,
    borderRadius: 0,
    borderBottomWidth: 1,
    borderColor: '#f0f0f0',
    paddingVertical: 10,
  },
  itemImgBg: {
    flex: 1,
  },
  itemImg: {
    width: '100%',
    height: 100,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    ...Platform.select({
      ios: {
        borderRadius: 0,
      },
    }),
  },
  itemFavorite: {
    position: 'absolute',
    alignSelf: 'flex-end',
    color: '#ED5D02',
    paddingRight: 10,
    fontSize: 18,
  },
  itemTitle: {
    color: '#39405B',
    fontSize: 13,
    fontFamily: 'Kanit-SemiBold',
    paddingHorizontal: 1,
  },
  itemText: {
    color: '#999',
    fontSize: 12,
    fontFamily: 'Kanit-Regular',
    paddingHorizontal: 3,
  },
  itemPrice: {
    color: '#333',
    fontSize: 14,
    fontFamily: 'Kanit-SemiBold',
    paddingHorizontal: 3,
  },
  itemDescription: {
    color: '#999',
    fontSize: 12,
    fontFamily: 'Kanit-Regular',
    marginBottom: 10,
    paddingHorizontal: 3,
    paddingLeft: 3,
  },
  itemLocation: {
    color: '#999',
    fontSize: 12,
    fontFamily: 'Kanit-Regular',
    marginBottom: 10,
    paddingHorizontal: 3,
  },
  crv: {
    borderRadius: 0,
  },
  itemRow: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingBottom: 15,
  },
  itemOverview: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  itemIcon: {
    color: '#CCC',
    marginRight: 5,
    fontSize: 14,
  },
  itemDate: {
    color: '#999',
    fontFamily: 'Kanit-Regular',
    fontSize: 12,
    marginRight: 20,
  },
  itemPosted: {
    marginTop: 10,
    color: '#999',
    fontFamily: 'Kanit-Regular',
    fontSize: 11,
    paddingHorizontal: 3,
    flexDirection: 'row',
  },
  itemLeft: {
    flexWrap: 'wrap',
    width: '40%',
  },
  itemContent: {
    flexWrap: 'wrap',
    width: '60%',
  },
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  //Text
  textTitle: {
    color: colors.primary,
    fontSize: 14,
    fontFamily: 'Kanit-Regular',
    paddingHorizontal: 1,
  },
  textDes: {
    color: '#333',
    fontSize: 14,
    fontFamily: 'Kanit-Regular',
    paddingHorizontal: 3,
  },

  bgFilter: {
    backgroundColor: '#FFF',
    borderTopWidth: 0.5,
    borderColor: '#DDD',
    flexDirection: 'row',
  },
  chevron: {
    fontSize: 18,
    color: '#999',
  },
  trash: {
    fontSize: 18,
    color: '#ED5D02',
  },

  //search
  boxInput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  text: {
    fontSize: 14,
    fontFamily: 'Kanit-Regular',
    color: colors.grayBlack,
  },
  buttonIcon: {
    fontSize: 30,
    color: middleGradient,
  },

  itemsPanel: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    borderColor: '#f0f0f0',
  },
})
