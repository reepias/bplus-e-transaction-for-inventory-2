import React, { PureComponent } from 'react'
import { View, TouchableOpacity, FlatList } from 'react-native'
import { TextComponent } from '../../components'

import { customWithLanguage } from '../../helpers'

import { i18n } from 'react-native-i18n-localize'
import Styles from './Style'

class Sheet extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      onScreen: true,
      items: this.props.data ? this.props.data.slice(0, 1) : null,
    }
    this.recursiveFunction = null
  }

  componentDidMount() {
    this.recursive()
  }

  componentWillUnmount() {
    console.log('componentWillUnmoun :', this.recursiveFunction)
    this.setState({ onScreen: false })
    clearTimeout(this.recursiveFunction)
  }

  recursive = () => {
    if (this.state.onScreen === true && this.state.items) {
      this.recursiveFunction = setTimeout(() => {
        let hasMore = this.state.items.length + 1 < this.props.data.length
        this.setState((prev, props) => ({
          items: props.data.slice(0, prev.items.length + 1),
        }))
        if (hasMore) this.recursive()
      }, 0)
    }
  }

  _listEmptyComponent = () => {
    return (
      <View style={Styles.layoutNoContent}>
        <TextComponent style={Styles.itemPrice}>ไม่พบตำแหน่งเก็บ</TextComponent>
        {/* <TextComponent style={Styles.itemPrice}>{`${i18n.t(
          'productNotFound'
        )}`}</TextComponent> */}
      </View>
    )
  }

  _keyExtractor = (item, index) => `${index}-${item.WH_NAME}`

  render() {
    const { items } = this.state
    return (
      <View style={Styles.section}>
        <FlatList
          data={items}
          showsHorizontalScrollIndicator={false}
          keyExtractor={this._keyExtractor}
          ListEmptyComponent={this._listEmptyComponent}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={Styles.item}
              underlayColor="transparent"
              onPress={() => this.props.onPress(item)}
            >
              <View style={Styles.rowBetween}>
                <View style={Styles.itemLeft}>
                  <TextComponent style={Styles.textTitle}>
                    {/* {`${i18n.t('purchasePrice')}`} */}
                    {`รหัส`}
                  </TextComponent>
                </View>
                <View style={Styles.itemContent}>
                  <TextComponent style={Styles.textDes}>
                    {item.WL_CODE}
                  </TextComponent>
                </View>
              </View>
              <View style={Styles.rowBetween}>
                <View style={Styles.itemLeft}>
                  <TextComponent style={Styles.textTitle}>
                    {/* {`${i18n.t('purchasePrice')}`} */}
                    {`ชื่อตำแหน่งเก็บ`}
                  </TextComponent>
                </View>
                <View style={Styles.itemContent}>
                  <TextComponent style={Styles.textDes}>
                    {item.WL_NAME}
                  </TextComponent>
                </View>
              </View>
              <View style={Styles.rowBetween}>
                <View style={Styles.itemLeft}>
                  <TextComponent style={Styles.textTitle}>
                    {/* {`${i18n.t('purchasePrice')}`} */}
                    {`ชื่อคลังสินค้า`}
                  </TextComponent>
                </View>
                <View style={Styles.itemContent}>
                  <TextComponent style={Styles.textDes}>
                    {item.WH_NAME}
                  </TextComponent>
                </View>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    )
  }
}

export default customWithLanguage(Sheet)
