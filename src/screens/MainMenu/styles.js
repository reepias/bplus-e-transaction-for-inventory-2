import { StyleSheet } from 'react-native'
import { heightWindow } from '../../constants/resolutions'

export default (styles = StyleSheet.create({
  container: {
    height: heightWindow,
  },
  iconPanel: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  logoPanel: {
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textPanel: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menuPanel: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingTop: 15,
    padding: 5,
  },
}))
