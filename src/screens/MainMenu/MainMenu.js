import React, { PureComponent } from 'react'
import { StyleSheet, ScrollView, View, TouchableOpacity } from 'react-native'
import { TextComponent } from '../../components'
import styles from './styles'
import { FullHeaderComponent } from '../../components'
import { documentMenu } from '../../constants'
import { Hamburger, Setting } from '../../constants/Icons'
import { LogoHeader, LogoBplus } from '../../constants/images'
import MenuComponent from './Menu'
import { drawer, push } from '../../helpers/navigations'
import { DOCUMENT_SCREEN } from '../../navigations/Screens'
import { customWithLanguage, preventFunction } from '../../helpers'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen'
import { i18n } from 'react-native-i18n-localize'
import {
  BUY_BELIEVE_TABLE,
  SELL_BELIEVE_TABLE,
  TRANSFER_TABLE,
} from '../../constants/config'

import { chooseDocumentMenu } from '../../redux/actions'
import { connect } from 'react-redux'

class MainMenu extends PureComponent {
  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    }
  }
  constructor(props) {
    super(props)
    this.onNavigating = false
  }

  componentDidMount() {
    loc(this)
  }

  componentWillUnMount() {
    rol()
  }

  _onPress = menu => () => {
    menu = {
      ...menu,
      componentId: this.props.componentId,
    }
    this.props.chooseDocumentMenu(menu)
  }

  render() {
    const newStyles = StyleSheet.create({
      header: {
        width: '100%',
        // height: heightWindow < 800 ? heightWindow / 4.5 : heightWindow / 5,
        height: hp('20%'),
        padding: 15,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
      },
      logoHeader: {
        width: hp('10%'),
        height: hp('10%'),
      },
      logoBplus: {
        height: hp('5%'),
        width: hp('10%'),
      },
      textHeader: {
        fontSize: wp('4%'),
        marginLeft: 10,
      },
    })
    return (
      <ScrollView bounces={false} style={styles.container}>
        <FullHeaderComponent style={newStyles.header}>
          <View style={styles.iconPanel}>
            <TouchableOpacity onPress={() => drawer(this.props.componentId)}>
              <Hamburger />
            </TouchableOpacity>
            <LogoHeader style={newStyles.logoHeader} />
            <TouchableOpacity>
              <Setting />
            </TouchableOpacity>
          </View>
          <View style={styles.logoPanel}>
            <View style={styles.textPanel}>
              <LogoBplus style={newStyles.logoBplus} />
              <TextComponent style={newStyles.textHeader}>
                e-Tranactions for Inventory
              </TextComponent>
            </View>
          </View>
        </FullHeaderComponent>
        <View style={styles.menuPanel}>
          {documentMenu &&
            documentMenu.map((menu, index) => {
              return (
                <MenuComponent
                  key={`${index}`}
                  text={`${menu.name}`}
                  source={menu.path}
                  isActive={menu.isActive}
                  onPress={preventFunction.exec(this._onPress(menu))}
                />
              )
            })}
        </View>
      </ScrollView>
    )
  }
}

export default connect(
  null,
  {
    chooseDocumentMenu,
  }
)(customWithLanguage(MainMenu))
