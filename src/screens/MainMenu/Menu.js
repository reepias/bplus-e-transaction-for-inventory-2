import React, { PureComponent } from 'react'
import { StyleSheet, TouchableOpacity, Image } from 'react-native'
import {
  startGradient,
  middleGradient,
  endGradient,
  colors,
} from '../../constants/styles'
import LinearGradient from 'react-native-linear-gradient'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  listenOrientationChange as loc,
  removeOrientationListener as rol,
} from 'react-native-responsive-screen'
import { TextComponent } from '../../components'

import { textBig } from '../../constants/fontSize'

class MenuComponent extends PureComponent {
  componentDidMount() {
    loc(this)
  }

  componentWillUnMount() {
    rol()
  }
  render() {
    let { text, source } = this.props
    text = text || '-'
    const styles = StyleSheet.create({
      linearGradient: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 15,
        width: wp('45%'),
        height: hp('23%'),
      },
      touchable: {
        width: '50%',
        paddingHorizontal: 8,
        paddingVertical: 5,
      },
      text: {
        marginLeft: 10,
        fontSize: textBig,
        color: colors.white,
        fontFamily: 'Kanit-Regular',
      },
      img: {
        width: hp('20%'),
        height: hp('15%'),
      },
    })
    return (
      <TouchableOpacity
        disabled={!this.props.isActive}
        onPress={this.props.onPress}
        style={styles.touchable}
      >
        {this.props.isActive ? (
          <LinearGradient
            start={{ x: 1, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={[endGradient, middleGradient, startGradient]}
            style={styles.linearGradient}
          >
            <Image style={styles.img} resizeMode="center" source={source} />
            <TextComponent style={styles.text}>{text}</TextComponent>
          </LinearGradient>
        ) : (
          <LinearGradient
            start={{ x: 1, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={[colors.grayBlack, colors.darkGray, colors.grayBlack]}
            style={styles.linearGradient}
          >
            <Image style={styles.img} resizeMode="center" source={source} />
            <TextComponent style={styles.text}>{text}</TextComponent>
          </LinearGradient>
        )}
      </TouchableOpacity>
    )
  }
}

export default MenuComponent
