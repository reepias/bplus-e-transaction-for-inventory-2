import { StyleSheet } from 'react-native'
import { widthWindow, heightWindow } from '../../constants/resolutions'

export default (styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    width: widthWindow / 2,
    height: widthWindow / 2,
    alignSelf: 'center',
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: heightWindow,
  },
  text: {
    fontSize: 16,
    paddingTop: 10,
  },
}))
