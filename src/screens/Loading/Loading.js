import React, { PureComponent } from 'react'
import { Image, View } from 'react-native'
import { connect } from 'react-redux'
import { TextComponent } from '../../components'
import { ContainerComponent } from '../../containers'
import styles from './styles'
import AnimatedEllipsis from 'react-native-animated-ellipsis'
import { connect } from 'react-redux'
import { getMacAddress } from '../../redux/actions'

class Loading extends PureComponent {
  static get options() {
    return {
      topBar: {
        visible: false,
        drawBehind: true,
      },
    }
  }
  constructor(props) {
    super(props)
    this.text = props.text || 'กำลังโหลด...'
  }

  componentDidMount() {
    this.timer = setInterval(
      () => this.setState(prevState => ({ test: prevState.test + 1 })),
      100
    )
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.test === 4) {
      this.props.getMacAddress()
      console.log(this.state.test)
    }
    DeviceInfo.getMACAddress()
      .then(macAddress => {
        console.log(macAddress)
      })
      .catch(err => {
        console.log(err)
      })
    setTimeout(() => {
      setStackRoot(this, object)
    }, 3000)
  }

  render() {
    return (
      <ContainerComponent style={styles.container}>
        <View style={styles.content}>
          <Image
            style={styles.logo}
            source={require('../../assets/images/logo-blue.png')}
          />
          <AnimatedEllipsis
            numberOfDots={6}
            minOpacity={0.5}
            animationDelay={200}
            style={{
              color: '#FFF',
              fontSize: 20,
            }}
          />
          <TextComponent style={styles.text}>{this.text}</TextComponent>
        </View>
      </ContainerComponent>
    )
  }
}

// const mapStateToProps = (state, ownProps) => ({
//   authStore: state.auth
// });

// const mapDispatchToProps = dispatch => ({
//   login: () => {
//     dispatch({
//       type: AUTH_DO_LOGIN,
//       payload: {
//         userId: 20
//       }
//     });
//   }
// });

export default connect(
  null,
  null
)(Loading)
