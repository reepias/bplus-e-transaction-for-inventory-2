import React, { Component } from 'react'
import { View } from 'react-native'
import { Calendar } from 'react-native-toggle-calendar'
import { Button, Text, Row } from 'native-base'
import moment from 'moment'

import CalendarHeaderComponent from '../components/CalendarHeaderComponent'

import { colors } from '../constants/styles'

import { Navigation } from 'react-native-navigation'

import { customWithLanguage } from '../helpers'

import { i18n } from 'react-native-i18n-localize'

let currentDate = moment().format('YYYY-MM-DD')

class DatePickerComponent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      calendarDate:
        moment(this.props.value).format('YYYY-MM-DD') || currentDate,
      horizontal: false,
      selectedDate: {
        [moment(this.props.value).format('YYYY-MM-DD') || currentDate]: {
          selected: true,
        },
      },
    }
  }

  onPressArrowLeft = () => {
    const calendarDate = this.state.calendarDate.add(-1, 'month')
    this.updateCalendarDate(calendarDate)
  }

  onPressArrowRight = () => {
    const calendarDate = this.state.calendarDate.add(1, 'month')
    this.updateCalendarDate(calendarDate)
  }

  onPressListView = () => {
    this.setState({ horizontal: true })
  }

  onPressGridView = () => {
    this.setState({ horizontal: false })
  }

  onDayPress = date => {
    selectDate = moment(date.dateString)
    this.setState({
      selectedDate: {
        [selectDate.format('YYYY-MM-DD')]: { selected: true },
      },
      calendarDate: selectDate.format('YYYY-MM-DD'),
    })
  }

  updateCalendarDate = calendarDate => {
    this.setState({
      calendarDate: calendarDate.format('YYYY-MM-DD'),
    })
  }

  onCancel = () => {
    Navigation.dismissModal(this.props.componentId)
  }

  _onOK = () => {
    this.props.onOK(this.props.inputKey, this.state.calendarDate)
    Navigation.dismissModal(this.props.componentId)
  }

  render() {
    const { selectedDate } = this.state
    return (
      <View style={{ flex: 1 }}>
        <Calendar
          current={this.state.calendarDate}
          //   dayComponent={CalendarDayComponent}
          calendarHeaderComponent={CalendarHeaderComponent}
          // headerData={{
          //   calendarDate: calendarDate.format('DD MMM, YYYY'),
          // }}
          style={{
            paddingLeft: 0,
            paddingRight: 0,
          }}
          onPressArrowLeft={this.onPressArrowLeft}
          onPressArrowRight={this.onPressArrowRight}
          onPressListView={this.onPressListView}
          onPressGridView={this.onPressGridView}
          horizontalEndReachedThreshold={50}
          horizontalStartReachedThreshold={0}
          horizontal={this.state.horizontal}
          onDayPress={this.onDayPress}
          markedDates={selectedDate}
        />
        <Row style={{ flex: 1, justifyContent: 'center', padding: 20 }}>
          <Button
            onPress={this._onOK}
            primary
            style={{
              flex: 0.3,
              justifyContent: 'center',
              marginHorizontal: 10,
              backgroundColor: colors.greenStart,
            }}
          >
            <Text>{`${i18n.t('ok')}`}</Text>
          </Button>
          <Button
            onPress={this.onCancel}
            light
            style={{
              flex: 0.3,
              justifyContent: 'center',
              marginHorizontal: 10,
            }}
          >
            <Text style={{ color: 'gray' }}>{`${i18n.t('cancel')}`}</Text>
          </Button>
        </Row>
      </View>
    )
  }
}

export default customWithLanguage(DatePickerComponent)
