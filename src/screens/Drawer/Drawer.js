import React, { PureComponent } from 'react'
import { ScrollView, View, TouchableOpacity } from 'react-native'
import { ContainerComponent } from '../../containers'
import DrawerHeader from './DrawerHeader'
import { Setting, ArrowBackWhite } from '../../constants/Icons'
import { pop, signOut } from '../../helpers/navigations'
import styles from './styles'
import { TextComponent } from '../../components'
import { connect } from 'react-redux'
import { logoutUserAction, unRegisterAction } from '../../redux/actions'
import { i18n, withLanguage } from 'react-native-i18n-localize'
class Drawer extends PureComponent {
  constructor(props) {
    super(props)
  }

  _signOut = () => {
    this.props.logoutUserAction()
  }

  _unRegisterAction = () => {
    this.props.unRegisterAction()
  }
  render() {
    return (
      <ContainerComponent disabledScrollView={true} style={styles.container}>
        <DrawerHeader style={styles.header}>
          <TouchableOpacity onPress={() => pop(this.props.componentId)}>
            <ArrowBackWhite />
          </TouchableOpacity>
        </DrawerHeader>
        <ScrollView bounces={true} style={styles.scrollView}>
          <View style={styles.content}>
            <TouchableOpacity
              onPress={this._unRegisterAction}
              style={styles.itemList}
            >
              <TextComponent style={styles.text}>
                {i18n.t(`unRegister`)}
              </TextComponent>
            </TouchableOpacity>
            <TouchableOpacity onPress={this._signOut} style={styles.itemList}>
              <TextComponent style={styles.text}>
                {i18n.t(`signOut`)}
              </TextComponent>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </ContainerComponent>
    )
  }
}

export default connect(
  null,
  {
    logoutUserAction,
    unRegisterAction,
  }
)(withLanguage(Drawer))
