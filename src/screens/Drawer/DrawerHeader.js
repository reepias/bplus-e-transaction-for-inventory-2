import React from 'react'
import { StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { drawerHeader } from '../../constants/styles'
import { heightWindow } from '../../constants/resolutions'

const DrawerHeaderComponent = props => {
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0, y: 1 }}
      colors={[drawerHeader, drawerHeader, drawerHeader]}
      style={[styles.linearGradient, props.style]}
      {...props}
    >
      {props.children}
    </LinearGradient>
  )
}

const styles = StyleSheet.create({
  linearGradient: {
    width: '100%',
    flex: 0.3,
    height: heightWindow / 4.5,
  },
})

export default DrawerHeaderComponent
