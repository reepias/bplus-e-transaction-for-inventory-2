import { StyleSheet } from 'react-native'
import { heightWindow } from '../../constants/resolutions'
import { colors } from '../../constants/styles'
export default (styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    height: heightWindow < 800 ? heightWindow / 4.5 : heightWindow / 5,
    padding: 15,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  itemList: {
    backgroundColor: colors.white,
    height: 40,
    justifyContent: 'center',
    paddingLeft: 20,
  },
  text: {
    fontFamily: 'Kanit-Regular',
    color: colors.grayBlack,
    fontSize: 16,
  },
  scrollView: {
    paddingVertical: 10,
    flex: 1,
  },
}))
