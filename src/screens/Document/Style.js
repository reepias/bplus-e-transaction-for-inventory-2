const React = require('react-native')
const { Platform } = React
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

import { textSmall, textMedium } from '../../constants/fontSize'

export default {
  layoutContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  layoutNoContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('60%'),
  },

  section: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 10,
    backgroundColor: '#FFF',
  },
  itemList: {
    flexDirection: 'row',
    width: '100%',
    marginBottom: 10,
  },
  itemBg: {
    ...Platform.select({
      ios: {},
    }),
    flexDirection: 'column',
    justifyContent: 'space-between',
  },

  item: {
    flex: 1,
    borderRadius: 0,
    borderBottomWidth: 1,
    borderColor: '#f0f0f0',
    flexDirection: 'row',
    paddingVertical: 10,
  },
  itemImgBg: {
    flex: 1,
  },
  itemImg: {
    width: '100%',
    height: 100,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
    ...Platform.select({
      ios: {
        borderRadius: 0,
      },
    }),
  },
  itemFavorite: {
    position: 'absolute',
    alignSelf: 'flex-end',
    color: '#ED5D02',
    paddingRight: 10,
    fontSize: textSmall,
  },
  itemTitle: {
    color: '#39405B',
    fontSize: textSmall,
    fontFamily: 'Kanit-SemiBold',
    paddingHorizontal: 1,
  },
  itemText: {
    color: '#999',
    fontSize: textSmall,
    fontFamily: 'Kanit-Regular',
    paddingHorizontal: 3,
  },
  itemPrice: {
    color: '#333',
    fontSize: textSmall,
    fontFamily: 'Kanit-SemiBold',
    paddingHorizontal: 3,
  },
  itemDescription: {
    color: '#999',
    fontSize: textSmall,
    fontFamily: 'Kanit-Regular',
    marginBottom: 10,
    paddingHorizontal: 3,
    paddingLeft: 3,
  },
  itemLocation: {
    color: '#999',
    fontSize: textSmall,
    fontFamily: 'Kanit-Regular',
    marginBottom: 10,
    paddingHorizontal: 3,
  },
  crv: {
    borderRadius: 0,
  },
  itemRow: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingBottom: 15,
  },
  itemOverview: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  itemIcon: {
    color: '#CCC',
    marginRight: 5,
    fontSize: textSmall,
  },
  itemDate: {
    color: '#999',
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    marginRight: 20,
  },
  itemPosted: {
    marginTop: 10,
    color: '#999',
    fontFamily: 'Kanit-Regular',
    fontSize: textSmall,
    paddingHorizontal: 3,
    flexDirection: 'row',
  },
  itemLeft: {
    flexWrap: 'wrap',
    width: '50%',
  },
  itemContent: {
    flexWrap: 'wrap',
    width: '50%',
  },
  itemRight: {
    flexWrap: 'wrap',
    width: '10%',
  },

  bgFilter: {
    backgroundColor: '#FFF',
    borderTopWidth: 0.5,
    borderColor: '#DDD',
    flexDirection: 'row',
  },
  chevron: {
    fontSize: textMedium,
    color: '#999',
  },
  trash: {
    fontSize: textMedium,
    color: '#ED5D02',
  },
}
