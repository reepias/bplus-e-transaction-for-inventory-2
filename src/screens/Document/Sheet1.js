import React, { PureComponent } from 'react'
import { StyleSheet, View, ScrollView } from 'react-native'
import { Table, TableWrapper, Row, Cols } from 'react-native-table-component'

export default class Sheet extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      items: this.props.data ? this.props.data.slice(0, 1) : null,
      onScreen: true,
      tableHead: [
        'DI_REF',
        'DI_DATE',
        'VAT_REF',
        'AP_CODE',
        'AP_NAME',
        'APD_B_AMT',
      ],
      widthArr: [150, 150, 150, 150, 200, 150],
    }
    this.recursiveFunction = null
  }

  componentDidMount() {
    this.recursive()
  }

  componentWillUnmount() {
    console.log('componentWillUnmoun :', this.recursiveFunction)
    this.setState({ onScreen: false })
    clearTimeout(this.recursiveFunction)
  }

  recursive = () => {
    console.log('recursive')
    if (this.state.onScreen === true && this.state.items) {
      this.recursiveFunction = setTimeout(() => {
        let hasMore = this.state.items.length + 1 < this.props.data.length
        console.log('hasMore: ', hasMore)
        this.setState((prev, props) => ({
          items: props.data.slice(0, prev.items.length + 1),
        }))
        if (hasMore) this.recursive()
      }, 0)
    }
  }

  render() {
    const state = this.state
    const tableData = []
    // for (let i = 0; i < 30; i += 1) {
    //   const rowData = []
    //   for (let j = 0; j < 9; j += 1) {
    //     rowData.push(`${i}${j}`)
    //   }
    //   tableData.push(rowData)
    // }
    // console.log('tableData: ', tableData)

    state.items &&
      state.items.map(row => {
        tableData.push([
          // row['DI_KEY'],
          row['DI_REF'],
          row['DI_DATE'],
          row['VAT_REF'],
          row['AP_CODE'],
          row['AP_NAME'],
          row['APD_B_AMT'],
        ])
      })

    // console.log('d: ', d)
    // if (d) {
    //   tableData.push(d)
    // }

    console.log('new Data: ', state.items)

    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{ borderColor: '#C1C0B9' }}>
              <Row
                data={state.tableHead}
                widthArr={state.widthArr}
                style={styles.header}
                textStyle={styles.text}
              />
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                <TableWrapper style={{ flex: 1 }}>
                  {tableData.map((rowData, index) => (
                    <Row
                      key={index}
                      data={rowData}
                      widthArr={state.widthArr}
                      style={[
                        styles.row,
                        index % 2 && { backgroundColor: '#F7F6E7' },
                      ]}
                      textStyle={styles.text}
                      numberOfLines={1}
                    />
                  ))}
                </TableWrapper>
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // padding: 16,
    // paddingTop: 30,
    backgroundColor: '#fff',
  },
  header: {
    height: 50,
    backgroundColor: '#537791',
  },
  text: {
    textAlign: 'right',
    fontFamily: 'Kanit-Regular',
    paddingHorizontal: 5,
  },
  dataWrapper: {
    // marginTop: -1,
  },
  row: {
    height: 40,
    backgroundColor: '#E7E6E1',
    justifyContent: 'flex-end',
    // alignItems: 'flex-end',
    alignSelf: 'flex-end',
  },
})
