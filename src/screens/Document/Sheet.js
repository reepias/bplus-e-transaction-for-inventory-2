import React, { PureComponent } from 'react'
import { TouchableOpacity, View, FlatList } from 'react-native'
import { Container, Content, Icon } from 'native-base'
import Text from '../../components/Text'

import {
  bplusFormatToDate,
  priceFormat,
  customWithLanguage,
} from '../../helpers/index'

import Style from '../../styles/Theme/Style'
import Styles from './Style'

import moment from 'moment'

import { i18n } from 'react-native-i18n-localize'

class Sheet extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      onScreen: true,
      items: this.props.data ? this.props.data.slice(0, 1) : null,
    }
    this.recursiveFunction = null
  }

  componentDidMount() {
    this.recursive()
  }

  componentWillUnmount() {
    console.log('componentWillUnmoun :', this.recursiveFunction)
    this.setState({ onScreen: false })
    clearTimeout(this.recursiveFunction)
  }

  recursive = () => {
    if (this.state.onScreen === true && this.state.items) {
      this.recursiveFunction = setTimeout(() => {
        let hasMore = this.state.items.length + 1 < this.props.data.length
        this.setState((prev, props) => ({
          items: props.data.slice(0, prev.items.length + 1),
        }))
        if (hasMore) this.recursive()
      }, 0)
    }
  }

  _listEmptyComponent = () => {
    return (
      <View style={Styles.layoutNoContent}>
        <Text style={Styles.itemPrice}>{`${i18n.t('documentNotFound')}`}</Text>
      </View>
    )
  }

  _keyExtractor = (item, index) => `${index}`

  render() {
    const { items } = this.state
    return (
      <Container style={Style.bgMain}>
        <Content
          style={Style.layoutInner}
          contentContainerStyle={Style.layoutContent}
        >
          <View style={Styles.section}>
            <FlatList
              data={items}
              showsHorizontalScrollIndicator={false}
              keyExtractor={this._keyExtractor}
              ListEmptyComponent={this._listEmptyComponent}
              renderItem={({ item, separators }) => (
                <TouchableOpacity
                  style={Styles.item}
                  underlayColor="transparent"
                  onPress={this.props.onPress}
                >
                  <View style={Styles.itemLeft}>
                    <Text style={Styles.itemTitle}>
                      {item.AP_CODE || item.AR_CODE}
                    </Text>
                    <Text style={Styles.itemDescription}>
                      {item.AP_NAME || item.AR_NAME}
                    </Text>
                  </View>
                  <View style={Styles.itemContent}>
                    {/* <Text style={Styles.itemTitle}>{item.DI_KEY}</Text> */}
                    <Text style={Styles.itemTitle}>{`${i18n.t('no')}: ${
                      item.DI_REF
                    }`}</Text>
                    {item.VAT_REF && (
                      <Text style={Styles.itemText}>{`${i18n.t(
                        'taxInvoice'
                      )}: ${item.VAT_REF}`}</Text>
                    )}
                    {/* {item.DI_ITEMS && (
                      <Text style={Styles.itemText}>{`${i18n.t('DI_ITEMS')}: ${
                        item.DI_ITEMS
                      }`}</Text>
                    )} */}
                    <Text style={Styles.itemText}>
                      {`${i18n.t('balance')}: ฿${priceFormat(
                        item.APD_B_AMT || item.ARD_B_AMT || item.DI_AMOUNT
                      )}`}
                    </Text>
                    <View style={Styles.itemPosted}>
                      <Icon
                        name="calendar"
                        type="FontAwesome"
                        style={Styles.itemIcon}
                      />
                      <Text style={Styles.itemDate}>
                        {moment(bplusFormatToDate(item.DI_DATE)).format('LL')}
                      </Text>
                    </View>
                  </View>
                  {/* <View style={Styles.itemRight}>

                  </View> */}
                </TouchableOpacity>
              )}
            />
          </View>
        </Content>
      </Container>
    )
  }
}

export default customWithLanguage(Sheet)
