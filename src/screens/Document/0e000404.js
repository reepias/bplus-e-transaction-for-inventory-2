const data = [
        {
            "DI_KEY": "758",
            "DI_DATE": "201801120000",
            "DI_REF": "CB256101/00001",
            "VAT_REF": "CB256101/00001",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "149850"
        },
        {
            "DI_KEY": "762",
            "DI_DATE": "201801150000",
            "DI_REF": "CB256101/00002",
            "VAT_REF": "CB256101/00002",
            "AP_CODE": "FAP002",
            "AP_NAME": "SHIJIAZHUANG GOLDEN IMPORT AND   EXPORT TRADE CO., LTD.",
            "APD_B_AMT": "88457"
        },
        {
            "DI_KEY": "769",
            "DI_DATE": "201801220000",
            "DI_REF": "CB256101/00003",
            "VAT_REF": "CB256101/00003",
            "AP_CODE": "FAP004",
            "AP_NAME": "ZIBO JINGXIN E&M MANUFACTURING CO.,LTD.",
            "APD_B_AMT": "126834.5"
        },
        {
            "DI_KEY": "781",
            "DI_DATE": "201802080000",
            "DI_REF": "CB256102/00001",
            "VAT_REF": "CB256102/00001",
            "AP_CODE": "AP003",
            "AP_NAME": "บริษัท กรีนเพท จำกัด",
            "APD_B_AMT": "25538.76"
        },
        {
            "DI_KEY": "783",
            "DI_DATE": "201802130000",
            "DI_REF": "CB256102/00002",
            "VAT_REF": "CB256102/00002",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "200839"
        },
        {
            "DI_KEY": "810",
            "DI_DATE": "201810050000",
            "DI_REF": "CB256110/00001",
            "VAT_REF": "CB256110/00001",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "8260.4"
        },
        {
            "DI_KEY": "816",
            "DI_DATE": "201803100000",
            "DI_REF": "CB256103/00001",
            "VAT_REF": "CB256103/00001",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "11556"
        },
        {
            "DI_KEY": "860",
            "DI_DATE": "201801310000",
            "DI_REF": "PP256101/00001",
            "VAT_REF": "PP256101/00001",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "3025"
        },
        {
            "DI_KEY": "1038",
            "DI_DATE": "201804110000",
            "DI_REF": "CB256104/00001",
            "VAT_REF": "CB256104/00001",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "45427.8"
        },
        {
            "DI_KEY": "1192",
            "DI_DATE": "201802180000",
            "DI_REF": "CB256102/00003",
            "VAT_REF": "CB256102/00003",
            "AP_CODE": "AP012",
            "AP_NAME": "บริษัท คราม จำกัด",
            "APD_B_AMT": "142855.7"
        },
        {
            "DI_KEY": "1196",
            "DI_DATE": "201802280000",
            "DI_REF": "PP256102/00001",
            "VAT_REF": "PP256102/00001",
            "AP_CODE": "AP002",
            "AP_NAME": "บริษัท เอ.บี.ฟาส จำกัด",
            "APD_B_AMT": "5030"
        },
        {
            "DI_KEY": "1200",
            "DI_DATE": "201803150000",
            "DI_REF": "CB256103/00002",
            "VAT_REF": "CB256103/00002",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "45427.8"
        },
        {
            "DI_KEY": "1212",
            "DI_DATE": "201803130000",
            "DI_REF": "CB256103/00003",
            "VAT_REF": "CB256103/00003",
            "AP_CODE": "AP018",
            "AP_NAME": "บริษัท ศุภกรี จำกัด",
            "APD_B_AMT": "110691.5"
        },
        {
            "DI_KEY": "1228",
            "DI_DATE": "201810170000",
            "DI_REF": "CB256110/00002",
            "VAT_REF": "CB256110/00002",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "190797.05"
        },
        {
            "DI_KEY": "1229",
            "DI_DATE": "201804220000",
            "DI_REF": "CB256104/00002",
            "VAT_REF": "CB256104/00002",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "163164.3"
        },
        {
            "DI_KEY": "1238",
            "DI_DATE": "201809090000",
            "DI_REF": "CB256109/00001",
            "VAT_REF": "CB256109/00001",
            "AP_CODE": "AP010",
            "AP_NAME": "บริษัท อินจัน จำกัด",
            "APD_B_AMT": "270356.34"
        },
        {
            "DI_KEY": "1246",
            "DI_DATE": "201809120000",
            "DI_REF": "CB256109/00002",
            "VAT_REF": "CB256109/00002",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "77040"
        },
        {
            "DI_KEY": "1252",
            "DI_DATE": "201805070000",
            "DI_REF": "CB256105/00001",
            "VAT_REF": "CB256105/00001",
            "AP_CODE": "AP014",
            "AP_NAME": "บริษัท สยามไท จำกัด",
            "APD_B_AMT": "156763.68"
        },
        {
            "DI_KEY": "1254",
            "DI_DATE": "201805110000",
            "DI_REF": "CB256105/00002",
            "VAT_REF": "CB256105/00002",
            "AP_CODE": "AP017",
            "AP_NAME": "บริษัท อานันตาพุฒ จำกัด",
            "APD_B_AMT": "14091.9"
        },
        {
            "DI_KEY": "1257",
            "DI_DATE": "201806150000",
            "DI_REF": "CB256106/00001",
            "VAT_REF": "CB256106/00001",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "65400"
        },
        {
            "DI_KEY": "1261",
            "DI_DATE": "201810200000",
            "DI_REF": "CB256110/00003",
            "VAT_REF": "CB256110/00003",
            "AP_CODE": "AP004",
            "AP_NAME": "บริษัท เอ.เอ็ม.แอล จำกัด",
            "APD_B_AMT": "27674.21"
        },
        {
            "DI_KEY": "1317",
            "DI_DATE": "201809150000",
            "DI_REF": "CB256109/00003",
            "VAT_REF": "CB256109/00003",
            "AP_CODE": "AP011",
            "AP_NAME": "บริษัท ควิ้กรี่ จำกัด",
            "APD_B_AMT": "45416.43"
        },
        {
            "DI_KEY": "1321",
            "DI_DATE": "201810050000",
            "DI_REF": "CB256110/00004",
            "VAT_REF": "CB256110/00004",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "9405.3"
        },
        {
            "DI_KEY": "1328",
            "DI_DATE": "201811050000",
            "DI_REF": "CB256111/00001",
            "VAT_REF": "CB256111/00001",
            "AP_CODE": "AP005",
            "AP_NAME": "บริษัท รุ่งการค้า จำกัด",
            "APD_B_AMT": "49938"
        },
        {
            "DI_KEY": "1331",
            "DI_DATE": "201811110000",
            "DI_REF": "CB256111/00002",
            "VAT_REF": "CB256111/00002",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "102399"
        },
        {
            "DI_KEY": "1343",
            "DI_DATE": "201812310000",
            "DI_REF": "CB256112/00001",
            "VAT_REF": "CB256112/00001",
            "AP_CODE": "AP002",
            "AP_NAME": "บริษัท เอ.บี.ฟาส จำกัด",
            "APD_B_AMT": "55500.9"
        },
        {
            "DI_KEY": "1351",
            "DI_DATE": "201808120000",
            "DI_REF": "CB256108/00001",
            "VAT_REF": "CB256108/00001",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "132883.3"
        },
        {
            "DI_KEY": "1366",
            "DI_DATE": "201809120000",
            "DI_REF": "CB256109/00004",
            "VAT_REF": "CB256109/00004",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "34533"
        },
        {
            "DI_KEY": "1368",
            "DI_DATE": "201807130000",
            "DI_REF": "CB256107/00001",
            "VAT_REF": "CB256107/00001",
            "AP_CODE": "AP011",
            "AP_NAME": "บริษัท ควิ้กรี่ จำกัด",
            "APD_B_AMT": "132883.3"
        },
        {
            "DI_KEY": "1374",
            "DI_DATE": "201801060000",
            "DI_REF": "IB256101/00004",
            "VAT_REF": "IB256101/00004",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "91900"
        },
        {
            "DI_KEY": "1375",
            "DI_DATE": "201810080000",
            "DI_REF": "IB256110/00001",
            "VAT_REF": "IB256110/00001",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "50062.63"
        },
        {
            "DI_KEY": "1383",
            "DI_DATE": "201807150000",
            "DI_REF": "IB256107/00001",
            "VAT_REF": "IB256107/00001",
            "AP_CODE": "AP010",
            "AP_NAME": "บริษัท อินจัน จำกัด",
            "APD_B_AMT": "42234.77"
        },
        {
            "DI_KEY": "1387",
            "DI_DATE": "201801050000",
            "DI_REF": "IB256101/00005",
            "VAT_REF": "IB256101/00005",
            "AP_CODE": "AP003",
            "AP_NAME": "บริษัท กรีนเพท จำกัด",
            "APD_B_AMT": "51520.5"
        },
        {
            "DI_KEY": "1391",
            "DI_DATE": "201807160000",
            "DI_REF": "IB256107/00002",
            "VAT_REF": "IB256107/00002",
            "AP_CODE": "AP012",
            "AP_NAME": "บริษัท คราม จำกัด",
            "APD_B_AMT": "49723"
        },
        {
            "DI_KEY": "1392",
            "DI_DATE": "201801220000",
            "DI_REF": "IB256101/00006",
            "VAT_REF": "IB256101/00006",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "6671551.68"
        },
        {
            "DI_KEY": "1395",
            "DI_DATE": "201808180000",
            "DI_REF": "IB256108/00001",
            "VAT_REF": "IB256108/00001",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "43640"
        },
        {
            "DI_KEY": "1411",
            "DI_DATE": "201809030000",
            "DI_REF": "IB256109/00001",
            "VAT_REF": "IB256109/00001",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "64842"
        },
        {
            "DI_KEY": "1412",
            "DI_DATE": "201801020000",
            "DI_REF": "IB256101/00001",
            "VAT_REF": "IB256101/00001",
            "AP_CODE": "AP013",
            "AP_NAME": "บริษัท รุ้ง จำกัด",
            "APD_B_AMT": "87943.3"
        },
        {
            "DI_KEY": "1417",
            "DI_DATE": "201809170000",
            "DI_REF": "IB256109/00002",
            "VAT_REF": "IB256109/00002",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "57742.64"
        },
        {
            "DI_KEY": "1435",
            "DI_DATE": "201803010000",
            "DI_REF": "IB256103/00001",
            "VAT_REF": "IB256103/00001",
            "AP_CODE": "AP008",
            "AP_NAME": "บริษัท ไลค์แชร์ จำกัด",
            "APD_B_AMT": "28000"
        },
        {
            "DI_KEY": "1436",
            "DI_DATE": "201810150000",
            "DI_REF": "IB256110/00002",
            "VAT_REF": "IB256110/00002",
            "AP_CODE": "AP012",
            "AP_NAME": "บริษัท คราม จำกัด",
            "APD_B_AMT": "52697.5"
        },
        {
            "DI_KEY": "1440",
            "DI_DATE": "201803050000",
            "DI_REF": "IB256103/00002",
            "VAT_REF": "IB256103/00002",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "29500"
        },
        {
            "DI_KEY": "1441",
            "DI_DATE": "201810180000",
            "DI_REF": "IB256110/00003",
            "VAT_REF": "IB256110/00003",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "64512.44"
        },
        {
            "DI_KEY": "1455",
            "DI_DATE": "201810250000",
            "DI_REF": "IB256110/00004",
            "VAT_REF": "IB256110/00004",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "102399"
        },
        {
            "DI_KEY": "1459",
            "DI_DATE": "201805090000",
            "DI_REF": "IB256105/00001",
            "VAT_REF": "IB256105/00001",
            "AP_CODE": "AP011",
            "AP_NAME": "บริษัท ควิ้กรี่ จำกัด",
            "APD_B_AMT": "10165"
        },
        {
            "DI_KEY": "1471",
            "DI_DATE": "201811250000",
            "DI_REF": "IB256111/00001",
            "VAT_REF": "IB256111/00001",
            "AP_CODE": "AP004",
            "AP_NAME": "บริษัท เอ.เอ็ม.แอล จำกัด",
            "APD_B_AMT": "174492"
        },
        {
            "DI_KEY": "1474",
            "DI_DATE": "201804040000",
            "DI_REF": "IB256104/00001",
            "VAT_REF": "IB256104/00001",
            "AP_CODE": "AP013",
            "AP_NAME": "บริษัท รุ้ง จำกัด",
            "APD_B_AMT": "7190"
        },
        {
            "DI_KEY": "1482",
            "DI_DATE": "201804150000",
            "DI_REF": "IB256104/00002",
            "VAT_REF": "IB256104/00002",
            "AP_CODE": "AP002",
            "AP_NAME": "บริษัท เอ.บี.ฟาส จำกัด",
            "APD_B_AMT": "155043"
        },
        {
            "DI_KEY": "1489",
            "DI_DATE": "201811300000",
            "DI_REF": "IB256111/00002",
            "VAT_REF": "IB256111/00002",
            "AP_CODE": "FAP003",
            "AP_NAME": "Zhejiang Xiang you Automation Technology Co.,Ltd.",
            "APD_B_AMT": "21280"
        },
        {
            "DI_KEY": "1499",
            "DI_DATE": "201802110000",
            "DI_REF": "IB256102/00001",
            "VAT_REF": "IB256102/00001",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "52590.5"
        },
        {
            "DI_KEY": "1501",
            "DI_DATE": "201806110000",
            "DI_REF": "IB256106/00001",
            "VAT_REF": "IB256106/00001",
            "AP_CODE": "AP017",
            "AP_NAME": "บริษัท อานันตาพุฒ จำกัด",
            "APD_B_AMT": "14091.9"
        },
        {
            "DI_KEY": "1505",
            "DI_DATE": "201812150000",
            "DI_REF": "IB256112/00001",
            "VAT_REF": "IB256112/00001",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "6790"
        },
        {
            "DI_KEY": "1509",
            "DI_DATE": "201812200000",
            "DI_REF": "IB256112/00002",
            "VAT_REF": "IB256112/00002",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "21375"
        },
        {
            "DI_KEY": "1513",
            "DI_DATE": "201802130000",
            "DI_REF": "IB256102/00002",
            "VAT_REF": "IB256102/00002",
            "AP_CODE": "AP019",
            "AP_NAME": "บริษัท ปัทมี จำกัด",
            "APD_B_AMT": "96709.81"
        },
        {
            "DI_KEY": "1523",
            "DI_DATE": "201805120000",
            "DI_REF": "IB256105/00002",
            "VAT_REF": "IB256105/00002",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "47741.96"
        },
        {
            "DI_KEY": "1530",
            "DI_DATE": "201805150000",
            "DI_REF": "IB256105/00003",
            "VAT_REF": "IB256105/00003",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "152151.68"
        },
        {
            "DI_KEY": "1532",
            "DI_DATE": "201805200000",
            "DI_REF": "IB256105/00004",
            "VAT_REF": "IB256105/00004",
            "AP_CODE": "AP014",
            "AP_NAME": "บริษัท สยามไท จำกัด",
            "APD_B_AMT": "83444.49"
        },
        {
            "DI_KEY": "1535",
            "DI_DATE": "201801130000",
            "DI_REF": "DBC256101/0001",
            "VAT_REF": "DBC256101/0001",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "19910"
        },
        {
            "DI_KEY": "1539",
            "DI_DATE": "201801170000",
            "DI_REF": "DBC256101/0002",
            "VAT_REF": "DBC256101/0002",
            "AP_CODE": "FAP002",
            "AP_NAME": "SHIJIAZHUANG GOLDEN IMPORT AND   EXPORT TRADE CO., LTD.",
            "APD_B_AMT": "12922"
        },
        {
            "DI_KEY": "1553",
            "DI_DATE": "201809130000",
            "DI_REF": "DBC256109/0001",
            "VAT_REF": "DBC256109/0001",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "77040"
        },
        {
            "DI_KEY": "1560",
            "DI_DATE": "201809150000",
            "DI_REF": "DBC256109/0002",
            "VAT_REF": "DBC256109/0002",
            "AP_CODE": "AP010",
            "AP_NAME": "บริษัท อินจัน จำกัด",
            "APD_B_AMT": "12744.68"
        },
        {
            "DI_KEY": "1871",
            "DI_DATE": "201802030000",
            "DI_REF": "DBC256102/0001",
            "VAT_REF": "DBC256102/0001",
            "AP_CODE": "AP002",
            "AP_NAME": "บริษัท เอ.บี.ฟาส จำกัด",
            "APD_B_AMT": "466.52"
        },
        {
            "DI_KEY": "1884",
            "DI_DATE": "201809190000",
            "DI_REF": "CB256109/00005",
            "VAT_REF": "CB256109/00005",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "34533"
        },
        {
            "DI_KEY": "1895",
            "DI_DATE": "201807150000",
            "DI_REF": "DBC256107/0001",
            "VAT_REF": "DBC256107/0001",
            "AP_CODE": "AP011",
            "AP_NAME": "บริษัท ควิ้กรี่ จำกัด",
            "APD_B_AMT": "10229.2"
        },
        {
            "DI_KEY": "1903",
            "DI_DATE": "201803050000",
            "DI_REF": "IB256103/00003",
            "VAT_REF": "IB256103/00003",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "29500"
        },
        {
            "DI_KEY": "1904",
            "DI_DATE": "201809220000",
            "DI_REF": "IB256109/00003",
            "VAT_REF": "IB256109/00003",
            "AP_CODE": "AP008",
            "AP_NAME": "บริษัท ไลค์แชร์ จำกัด",
            "APD_B_AMT": "25199.17"
        },
        {
            "DI_KEY": "1909",
            "DI_DATE": "201808220000",
            "DI_REF": "IB256108/00002",
            "VAT_REF": "IB256108/00002",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "205012"
        },
        {
            "DI_KEY": "1910",
            "DI_DATE": "201802160000",
            "DI_REF": "IB256102/00003",
            "VAT_REF": "IB256102/00003",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "297144.4"
        },
        {
            "DI_KEY": "1919",
            "DI_DATE": "201804160000",
            "DI_REF": "IB256104/00003",
            "VAT_REF": "IB256104/00003",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "11026.35"
        },
        {
            "DI_KEY": "1926",
            "DI_DATE": "201806130000",
            "DI_REF": "IB256106/00002",
            "VAT_REF": "IB256106/00002",
            "AP_CODE": "AP011",
            "AP_NAME": "บริษัท ควิ้กรี่ จำกัด",
            "APD_B_AMT": "15735.42"
        },
        {
            "DI_KEY": "1932",
            "DI_DATE": "201806150000",
            "DI_REF": "IB256106/00003",
            "VAT_REF": "IB256106/00003",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "98932.2"
        },
        {
            "DI_KEY": "1944",
            "DI_DATE": "201808230000",
            "DI_REF": "IB256108/00003",
            "VAT_REF": "IB256108/00003",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "3852"
        },
        {
            "DI_KEY": "1945",
            "DI_DATE": "201809220000",
            "DI_REF": "IB256109/00004",
            "VAT_REF": "IB256109/00004",
            "AP_CODE": "AP008",
            "AP_NAME": "บริษัท ไลค์แชร์ จำกัด",
            "APD_B_AMT": "75896.52"
        },
        {
            "DI_KEY": "1962",
            "DI_DATE": "201803110000",
            "DI_REF": "IB256103/00004",
            "VAT_REF": "IB256103/00004",
            "AP_CODE": "AP012",
            "AP_NAME": "บริษัท คราม จำกัด",
            "APD_B_AMT": "43752.3"
        },
        {
            "DI_KEY": "1983",
            "DI_DATE": "201812210000",
            "DI_REF": "IB256112/00003",
            "VAT_REF": "IB256112/00003",
            "AP_CODE": "AP002",
            "AP_NAME": "บริษัท เอ.บี.ฟาส จำกัด",
            "APD_B_AMT": "94135"
        },
        {
            "DI_KEY": "2024",
            "DI_DATE": "201808130000",
            "DI_REF": "DBC256108/0001",
            "VAT_REF": "DBC256108/0001",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "5219.46"
        },
        {
            "DI_KEY": "2034",
            "DI_DATE": "201803050000",
            "DI_REF": "DBC256103/0001",
            "VAT_REF": "DBC256103/0001",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "533"
        },
        {
            "DI_KEY": "2118",
            "DI_DATE": "201801040000",
            "DI_REF": "DB256101/00001",
            "VAT_REF": "DB256101/00001",
            "AP_CODE": "AP013",
            "AP_NAME": "บริษัท รุ้ง จำกัด",
            "APD_B_AMT": "3555.61"
        },
        {
            "DI_KEY": "2124",
            "DI_DATE": "201801060000",
            "DI_REF": "DB256101/00002",
            "VAT_REF": "DB256101/00002",
            "AP_CODE": "AP003",
            "AP_NAME": "บริษัท กรีนเพท จำกัด",
            "APD_B_AMT": "10432.5"
        },
        {
            "DI_KEY": "2138",
            "DI_DATE": "201801080000",
            "DI_REF": "DB256101/00003",
            "VAT_REF": "DB256101/00003",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "8490"
        },
        {
            "DI_KEY": "2144",
            "DI_DATE": "201802130000",
            "DI_REF": "DB256102/00001",
            "VAT_REF": "DB256102/00001",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "10432.5"
        },
        {
            "DI_KEY": "2147",
            "DI_DATE": "201802180000",
            "DI_REF": "DB256102/00002",
            "VAT_REF": "DB256102/00002",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "155608.32"
        },
        {
            "DI_KEY": "2149",
            "DI_DATE": "201803040000",
            "DI_REF": "DB256103/00001",
            "VAT_REF": "DB256103/00001",
            "AP_CODE": "AP008",
            "AP_NAME": "บริษัท ไลค์แชร์ จำกัด",
            "APD_B_AMT": "28000"
        },
        {
            "DI_KEY": "2151",
            "DI_DATE": "201803090000",
            "DI_REF": "DB256103/00002",
            "VAT_REF": "DB256103/00002",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "1475"
        },
        {
            "DI_KEY": "2153",
            "DI_DATE": "201804060000",
            "DI_REF": "DB256104/00001",
            "VAT_REF": "DB256104/00001",
            "AP_CODE": "AP013",
            "AP_NAME": "บริษัท รุ้ง จำกัด",
            "APD_B_AMT": "310"
        },
        {
            "DI_KEY": "2154",
            "DI_DATE": "201804160000",
            "DI_REF": "DB256104/00002",
            "VAT_REF": "DB256104/00002",
            "AP_CODE": "AP002",
            "AP_NAME": "บริษัท เอ.บี.ฟาส จำกัด",
            "APD_B_AMT": "12305"
        },
        {
            "DI_KEY": "2163",
            "DI_DATE": "201805140000",
            "DI_REF": "DB256105/00001",
            "VAT_REF": "DB256105/00001",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "2341"
        },
        {
            "DI_KEY": "2164",
            "DI_DATE": "201805180000",
            "DI_REF": "DB256105/00002",
            "VAT_REF": "DB256105/00002",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "20148"
        },
        {
            "DI_KEY": "2176",
            "DI_DATE": "201806120000",
            "DI_REF": "DB256106/00001",
            "VAT_REF": "DB256106/00001",
            "AP_CODE": "AP017",
            "AP_NAME": "บริษัท อานันตาพุฒ จำกัด",
            "APD_B_AMT": "1155.6"
        },
        {
            "DI_KEY": "2177",
            "DI_DATE": "201806140000",
            "DI_REF": "DB256106/00002",
            "VAT_REF": "DB256106/00002",
            "AP_CODE": "AP011",
            "AP_NAME": "บริษัท ควิ้กรี่ จำกัด",
            "APD_B_AMT": "15246.43"
        },
        {
            "DI_KEY": "2205",
            "DI_DATE": "201807160000",
            "DI_REF": "DB256107/00001",
            "VAT_REF": "DB256107/00001",
            "AP_CODE": "AP010",
            "AP_NAME": "บริษัท อินจัน จำกัด",
            "APD_B_AMT": "1472.78"
        },
        {
            "DI_KEY": "2206",
            "DI_DATE": "201807180000",
            "DI_REF": "DB256107/00002",
            "VAT_REF": "DB256107/00002",
            "AP_CODE": "AP012",
            "AP_NAME": "บริษัท คราม จำกัด",
            "APD_B_AMT": "25897.25"
        },
        {
            "DI_KEY": "2209",
            "DI_DATE": "201808190000",
            "DI_REF": "DB256108/00001",
            "VAT_REF": "DB256108/00001",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "2945"
        },
        {
            "DI_KEY": "2211",
            "DI_DATE": "201808230000",
            "DI_REF": "DB256108/00002",
            "VAT_REF": "DB256108/00002",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "8132"
        },
        {
            "DI_KEY": "2225",
            "DI_DATE": "201809050000",
            "DI_REF": "DB256109/00001",
            "VAT_REF": "DB256109/00001",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "11222.16"
        },
        {
            "DI_KEY": "2228",
            "DI_DATE": "201809190000",
            "DI_REF": "DB256109/00002",
            "VAT_REF": "DB256109/00002",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "15749.05"
        },
        {
            "DI_KEY": "2232",
            "DI_DATE": "201810010000",
            "DI_REF": "DB256110/00005",
            "VAT_REF": "DB256110/00005",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "56335.5"
        },
        {
            "DI_KEY": "2239",
            "DI_DATE": "201810060000",
            "DI_REF": "DB256110/00006",
            "VAT_REF": "DB256110/00006",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "5716.05"
        },
        {
            "DI_KEY": "2260",
            "DI_DATE": "201812220000",
            "DI_REF": "IB256112/00004",
            "VAT_REF": "IB256112/00004",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "18000"
        },
        {
            "DI_KEY": "2263",
            "DI_DATE": "201812120000",
            "DI_REF": "DB256112/00001",
            "VAT_REF": "DB256112/00001",
            "AP_CODE": "AP004",
            "AP_NAME": "บริษัท เอ.เอ็ม.แอล จำกัด",
            "APD_B_AMT": "9759.28"
        },
        {
            "DI_KEY": "2267",
            "DI_DATE": "201812130000",
            "DI_REF": "DB256112/00002",
            "VAT_REF": "DB256112/00002",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "8140.56"
        },
        {
            "DI_KEY": "2278",
            "DI_DATE": "201811020000",
            "DI_REF": "DB256111/00001",
            "VAT_REF": "DB256111/00001",
            "AP_CODE": "AP017",
            "AP_NAME": "บริษัท อานันตาพุฒ จำกัด",
            "APD_B_AMT": "609.9"
        },
        {
            "DI_KEY": "2284",
            "DI_DATE": "201811080000",
            "DI_REF": "DB256111/00002",
            "VAT_REF": "DB256111/00002",
            "AP_CODE": "AP014",
            "AP_NAME": "บริษัท สยามไท จำกัด",
            "APD_B_AMT": "4483.78"
        },
        {
            "DI_KEY": "2304",
            "DI_DATE": "201804130000",
            "DI_REF": "DBC256104/0001",
            "VAT_REF": "DBC256104/0001",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "5066.64"
        },
        {
            "DI_KEY": "2317",
            "DI_DATE": "201805090000",
            "DI_REF": "DBC256105/0001",
            "VAT_REF": "DBC256105/0001",
            "AP_CODE": "AP014",
            "AP_NAME": "บริษัท สยามไท จำกัด",
            "APD_B_AMT": "11347.92"
        },
        {
            "DI_KEY": "2319",
            "DI_DATE": "201810150000",
            "DI_REF": "DBC256110/0002",
            "VAT_REF": "DBC256110/0002",
            "AP_CODE": "AP017",
            "AP_NAME": "บริษัท อานันตาพุฒ จำกัด",
            "APD_B_AMT": "930.9"
        },
        {
            "DI_KEY": "2324",
            "DI_DATE": "201810160000",
            "DI_REF": "DBC256110/0003",
            "VAT_REF": "DBC256110/0003",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "1861.8"
        },
        {
            "DI_KEY": "2341",
            "DI_DATE": "201810180000",
            "DI_REF": "DBC256110/0004",
            "VAT_REF": "DBC256110/0004",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "48.15"
        },
        {
            "DI_KEY": "2377",
            "DI_DATE": "201809220000",
            "DI_REF": "CB256109/00006",
            "VAT_REF": "CB256109/00006",
            "AP_CODE": "AP007",
            "AP_NAME": "บริษัท จันทกัณ จำกัด",
            "APD_B_AMT": "2281625.2"
        },
        {
            "DI_KEY": "2390",
            "DI_DATE": "201806160000",
            "DI_REF": "DBC256106/0001",
            "VAT_REF": "DBC256106/0001",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "5810"
        },
        {
            "DI_KEY": "2393",
            "DI_DATE": "201811060000",
            "DI_REF": "DBC256111/0001",
            "VAT_REF": "DBC256111/0001",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "6888.66"
        },
        {
            "DI_KEY": "2472",
            "DI_DATE": "201809240000",
            "DI_REF": "IB256109/00006",
            "VAT_REF": "IB256109/00006",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "29371.5"
        },
        {
            "DI_KEY": "2479",
            "DI_DATE": "201809250000",
            "DI_REF": "IB256109/00007",
            "VAT_REF": "IB256109/00007",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "1284"
        },
        {
            "DI_KEY": "2503",
            "DI_DATE": "201809260000",
            "DI_REF": "IB256109/00008",
            "VAT_REF": "IB256109/00008",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "17200"
        },
        {
            "DI_KEY": "2510",
            "DI_DATE": "201807220000",
            "DI_REF": "IB256107/00003",
            "VAT_REF": "IB256107/00003",
            "AP_CODE": "AP014",
            "AP_NAME": "บริษัท สยามไท จำกัด",
            "APD_B_AMT": "86250"
        },
        {
            "DI_KEY": "2569",
            "DI_DATE": "201812050000",
            "DI_REF": "DBC256112/0001",
            "VAT_REF": "DBC256112/0001",
            "AP_CODE": "AP020",
            "AP_NAME": "บริษัท ปรารถนี จำกัด",
            "APD_B_AMT": "1102.1"
        },
        {
            "DI_KEY": "2572",
            "DI_DATE": "201812130000",
            "DI_REF": "DBC256112/0002",
            "VAT_REF": "DBC256112/0002",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "1893.9"
        },
        {
            "DI_KEY": "2624",
            "DI_DATE": "201809090000",
            "DI_REF": "IB256109/00009",
            "VAT_REF": "IB256109/00009",
            "AP_CODE": "AP016",
            "AP_NAME": "บริษัท บุญเจริญ จำกัด",
            "APD_B_AMT": "60151.5"
        },
        {
            "DI_KEY": "2629",
            "DI_DATE": "201809250000",
            "DI_REF": "CT256109/00001",
            "VAT_REF": "CT256109/00001",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "73081.2"
        },
        {
            "DI_KEY": "2636",
            "DI_DATE": "201807060000",
            "DI_REF": "CT256107/00001",
            "VAT_REF": "CT256107/00001",
            "AP_CODE": "FAP002",
            "AP_NAME": "SHIJIAZHUANG GOLDEN IMPORT AND   EXPORT TRADE CO., LTD.",
            "APD_B_AMT": "155075.21"
        },
        {
            "DI_KEY": "2640",
            "DI_DATE": "201808180000",
            "DI_REF": "CB256108/00002",
            "VAT_REF": "CB256108/00002",
            "AP_CODE": "AP008",
            "AP_NAME": "บริษัท ไลค์แชร์ จำกัด",
            "APD_B_AMT": "280020"
        },
        {
            "DI_KEY": "2646",
            "DI_DATE": "201808040000",
            "DI_REF": "CT256108/00001",
            "VAT_REF": "CT256108/00001",
            "AP_CODE": "FAP002",
            "AP_NAME": "SHIJIAZHUANG GOLDEN IMPORT AND   EXPORT TRADE CO., LTD.",
            "APD_B_AMT": "73535.35"
        },
        {
            "DI_KEY": "2675",
            "DI_DATE": "201810050000",
            "DI_REF": "CT256110/00001",
            "VAT_REF": "CT256110/00001",
            "AP_CODE": "FAP003",
            "AP_NAME": "Zhejiang Xiang you Automation Technology Co.,Ltd.",
            "APD_B_AMT": "215883.2"
        },
        {
            "DI_KEY": "2688",
            "DI_DATE": "201811080000",
            "DI_REF": "CT256111/00001",
            "VAT_REF": "CT256111/00001",
            "AP_CODE": "FAP004",
            "AP_NAME": "ZIBO JINGXIN E&M MANUFACTURING CO.,LTD.",
            "APD_B_AMT": "104753.89"
        },
        {
            "DI_KEY": "2704",
            "DI_DATE": "201810060000",
            "DI_REF": "IB256110/00006",
            "VAT_REF": "IB256110/00006",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "184040"
        },
        {
            "DI_KEY": "2745",
            "DI_DATE": "201810060000",
            "DI_REF": "IB256110/00007",
            "VAT_REF": "IB256110/00007",
            "AP_CODE": "FAP002",
            "AP_NAME": "SHIJIAZHUANG GOLDEN IMPORT AND   EXPORT TRADE CO., LTD.",
            "APD_B_AMT": "1387200"
        },
        {
            "DI_KEY": "2755",
            "DI_DATE": "201801230000",
            "DI_REF": "IB256101/00002",
            "VAT_REF": "IB256101/00002",
            "AP_CODE": "AP012",
            "AP_NAME": "บริษัท คราม จำกัด",
            "APD_B_AMT": "430568"
        },
        {
            "DI_KEY": "2766",
            "DI_DATE": "201801050000",
            "DI_REF": "IB.1256101/001",
            "VAT_REF": "IB.1256101/001",
            "AP_CODE": "FAP002",
            "AP_NAME": "SHIJIAZHUANG GOLDEN IMPORT AND   EXPORT TRADE CO., LTD.",
            "APD_B_AMT": "1303900"
        },
        {
            "DI_KEY": "2768",
            "DI_DATE": "201809250000",
            "DI_REF": "DBC256109/0003",
            "VAT_REF": "DBC256109/0003",
            "AP_CODE": "AP009",
            "AP_NAME": "บริษัท ซับสไคร์ป จำกัด",
            "APD_B_AMT": "1262.6"
        },
        {
            "DI_KEY": "2831",
            "DI_DATE": "201801140000",
            "DI_REF": "IB.1256101/002",
            "VAT_REF": "IB.1256101/002",
            "AP_CODE": "FAP003",
            "AP_NAME": "Zhejiang Xiang you Automation Technology Co.,Ltd.",
            "APD_B_AMT": "80000"
        },
        {
            "DI_KEY": "2863",
            "DI_DATE": "201803030000",
            "DI_REF": "IB.1256103/001",
            "VAT_REF": "IB.1256103/001",
            "AP_CODE": "FAP004",
            "AP_NAME": "ZIBO JINGXIN E&M MANUFACTURING CO.,LTD.",
            "APD_B_AMT": "257600"
        },
        {
            "DI_KEY": "2873",
            "DI_DATE": "201805060000",
            "DI_REF": "IB.1256105/001",
            "VAT_REF": "IB.1256105/001",
            "AP_CODE": "FAP003",
            "AP_NAME": "Zhejiang Xiang you Automation Technology Co.,Ltd.",
            "APD_B_AMT": "772310"
        },
        {
            "DI_KEY": "2928",
            "DI_DATE": "201808060000",
            "DI_REF": "IB.1256108/001",
            "VAT_REF": "IB.1256108/001",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "305368"
        },
        {
            "DI_KEY": "2931",
            "DI_DATE": "201809300000",
            "DI_REF": "IB.1256109/001",
            "VAT_REF": "IB.1256109/001",
            "AP_CODE": "FAP004",
            "AP_NAME": "ZIBO JINGXIN E&M MANUFACTURING CO.,LTD.",
            "APD_B_AMT": "23234.8"
        },
        {
            "DI_KEY": "3253",
            "DI_DATE": "201801230000",
            "DI_REF": "CB256101/00004",
            "VAT_REF": "CB256101/00004",
            "AP_CODE": "AP014",
            "AP_NAME": "บริษัท สยามไท จำกัด",
            "APD_B_AMT": "3418.65"
        },
        {
            "DI_KEY": "3254",
            "DI_DATE": "201904110000",
            "DI_REF": "IB256204/00001",
            "VAT_REF": "IB256204/00001",
            "AP_CODE": "AP015",
            "AP_NAME": "บริษัท ที.ดี.แซท จำกัด",
            "APD_B_AMT": "162105"
        },
        {
            "DI_KEY": "3255",
            "DI_DATE": "201904230000",
            "DI_REF": "IB256204/00002",
            "VAT_REF": "IB256204/00002",
            "AP_CODE": "FAP001",
            "AP_NAME": "Nanjing T-Bota Scietech Instruments & Equipment Co.,Ltd.",
            "APD_B_AMT": "28200"
        },
        {
            "DI_KEY": "3281",
            "DI_DATE": "201905280000",
            "DI_REF": "IB256205/00001",
            "VAT_REF": "IB256205/00001",
            "AP_CODE": "AP003",
            "AP_NAME": "บริษัท กรีนเพท จำกัด",
            "APD_B_AMT": "0"
        },
        {
            "DI_KEY": "3288",
            "DI_DATE": "201905280000",
            "DI_REF": "IB256205/00002",
            "VAT_REF": "IB256205/00002",
            "AP_CODE": "AP003",
            "AP_NAME": "บริษัท กรีนเพท จำกัด",
            "APD_B_AMT": "3531"
        },
        {
            "DI_KEY": "6336",
            "DI_DATE": "201906280000",
            "DI_REF": "IB256206/00001",
            "VAT_REF": "IB256206/00001",
            "AP_CODE": "AP003",
            "AP_NAME": "บริษัท กรีนเพท จำกัด",
            "APD_B_AMT": "963"
        },
        {
            "DI_KEY": "6340",
            "DI_DATE": "201907020000",
            "DI_REF": "IB256207/00001",
            "VAT_REF": "IB256207/00001",
            "AP_CODE": "AP001",
            "AP_NAME": "บริษัท ฟ้ารุ่ง จำกัด",
            "APD_B_AMT": "107"
        }
]

export default data
