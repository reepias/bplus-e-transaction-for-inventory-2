import { StyleSheet } from 'react-native'
import { heightWindow } from '../../constants/resolutions'
import {
  colors,
  startGradient,
  middleGradient,
  greenStartGardient,
} from '../../constants/styles'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen'

import { textSmall, textMedium } from '../../constants/fontSize'

export default styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  topBar: {
    backgroundColor: startGradient,
  },
  content: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  bottomPanel: {
    flex: 0.01,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: greenStartGardient,
    width: '100%',
  },
  buttonAdd: {
    backgroundColor: greenStartGardient,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    height: hp('6%'),
  },
  textButtonAdd: {
    color: colors.white,
    fontSize: textMedium,
    textAlign: 'center',
  },
  table: {
    flex: 1,
  },
  headTable: {
    backgroundColor: middleGradient,
    flexDirection: 'row',
  },
  footer: {
    backgroundColor: 'transparent',
    height: hp('6%'),
  },
})
