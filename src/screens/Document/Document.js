import React, { PureComponent, Fragment } from 'react'
import { View, SafeAreaView } from 'react-native'
import {
  TextComponent,
  HeaderComponent,
  ModalSearchComponent,
  ButtonComponent,
} from '../../components'
import { preventFunction, customWithLanguage } from '../../helpers'
import { pop, push } from '../../helpers/navigations'
import styles from './styles'
import { Button, Footer } from 'native-base'
import Sheet from './Sheet'
// import Sheet from './Sheet1'
import {
  BILLING_SCREEN,
  DOCUMENT_SEARCH_SCREEN,
} from '../../navigations/Screens'
import { connect } from 'react-redux'
import { getDocument } from '../../redux/actions/index'
import { i18n } from 'react-native-i18n-localize'

import _ from 'lodash'

class Document extends PureComponent {
  constructor(props) {
    super(props)
    this.onNavigating = false
  }

  componentDidMount() {
    this._getDocument()
  }

  _getDocument = () => {
    const { sqlTableKey } = this.props.chooseDocument
    this.props.getDocument(sqlTableKey)
  }

  _goSearch = () => {
    const object = {
      componentName: DOCUMENT_SEARCH_SCREEN,
    }
    push(this.props.componentId, object)
  }

  onPressed = () => {
    const object = {
      componentName: BILLING_SCREEN,
      passProps: {
        title: this.props.chooseDocument.title,
        mode: this.props.chooseDocument.customer,
      },
    }
    push(this.props.componentId, object)
  }

  render() {
    const { documents } = this.props
    return (
      <Fragment>
        <SafeAreaView style={styles.container}>
          <View
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'space-between',
            }}
          >
            <HeaderComponent
              onPressedBack={() => pop(this.props.componentId)}
              onPressedSearch={preventFunction.exec(this._goSearch)}
              haveBack={true}
              haveSearch={true}
              title={`ค้นหาเอกสาร - ${this.props.chooseDocument.title}`}
              style={styles.topBar}
            />
            <View style={styles.content}>
              {documents && documents.data && (
                <Sheet
                  title={this.props.chooseDocument.title}
                  data={documents.data}
                  componentId={this.props.componentId}
                  onPress={preventFunction.exec(this.onPressed)}
                />
              )}
            </View>
          </View>
        </SafeAreaView>
        <Footer style={[styles.footer]}>
          <Button
            transparent
            onPress={preventFunction.exec(this.onPressed)}
            style={styles.buttonAdd}
          >
            <TextComponent style={styles.textButtonAdd}>
              {`สร้างใหม่`}
            </TextComponent>
          </Button>
        </Footer>
      </Fragment>
    )
  }
}

function mapStateToProps({ document }) {
  const { documents, isFetching, chooseDocument } = document
  return {
    documents,
    chooseDocument,
    isFetching,
  }
}

export default connect(
  mapStateToProps,
  {
    getDocument,
  }
)(customWithLanguage(Document))
